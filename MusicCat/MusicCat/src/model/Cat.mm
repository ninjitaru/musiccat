//
//  Cat.m
//  MusicCatInvasion
//
//  Created by Jason Chang on 8/28/12.
//
//

#import "Cat.h"

#define kHEADANGLE 4

@implementation Cat

- (id) initWithParent: (CCNode*) parent
{
    self = [super init];
    if(self)
    {
        _parent = parent;
        self.node = [CCSpriteBatchNode batchNodeWithFile: @"cat.png"];
        [_parent addChild: self.node];
        
        CCSprite* s = [CCSprite spriteWithSpriteFrameName: @"body.png"];
        self.mainColor = ccc3(CCRANDOM_0_1()*255, CCRANDOM_0_1()*255, CCRANDOM_0_1()*255);
        s.color = self.mainColor;
        self.body = s;
        [self.node addChild: s];
        
        s = [CCSprite spriteWithSpriteFrameName: @"head.png"];
        [self.node addChild: s];
        self.head = s;
        self.head.color = self.mainColor;
        
        s = [CCSprite spriteWithSpriteFrameName: @"nose.png"];
        [self.head addChild: s];
        s.anchorPoint = ccp(0,0);
        
        s = [CCSprite spriteWithSpriteFrameName: @"ear.png"];
        [self.head addChild: s];
        s.anchorPoint = ccp(0,0);
        self.ear = s;
        self.earColor = ccc3(CCRANDOM_0_1()*255, CCRANDOM_0_1()*255, CCRANDOM_0_1()*255);
        s.color = self.earColor;
        
        s = [CCSprite spriteWithSpriteFrameName: @"eyes_heightcolor.png"];
        [self.head addChild: s];
        s.anchorPoint = ccp(0,0);
        
        s = [CCSprite spriteWithSpriteFrameName: @"eyes.png"];
        [self.head addChild: s];
        self.eye = s;
        s.anchorPoint = ccp(0,0);
        self.eyeColor = ccc3(CCRANDOM_0_1()*255, CCRANDOM_0_1()*255, CCRANDOM_0_1()*255);
        s.color = self.eyeColor;
        
        s = [CCSprite spriteWithSpriteFrameName: @"foot1.png"];
        [self.node addChild: s];
        self.foot = s;
        
        s = [CCSprite spriteWithSpriteFrameName: @"hand1.png"];
        [self.node addChild: s];
        self.hand = s;
        
        CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
        NSArray* temp = @[[cache spriteFrameByName: @"hand1.png"],[cache spriteFrameByName: @"hand2.png"],[cache spriteFrameByName: @"hand3.png"],[cache spriteFrameByName: @"hand4.png"], [cache spriteFrameByName: @"hand_jump.png"], [cache spriteFrameByName: @"hand_jump2.png"]];

        CCAnimation* a = [CCAnimation animationWithSpriteFrames: temp];
        a.delayPerUnit = 0.05f;
        //a.restoreOriginalFrame = YES;
        //id b = [CCSequence actionOne: [CCAnimate actionWithAnimation: a] two: [CCCallFunc actionWithTarget: self selector: @selector(testtest)]];
        _handAnimation = [[CCAnimate actionWithAnimation: a] retain];
        
    }
    return self;
}

- (void) setPosition:(CGPoint)position
{
    _initPos = position;
    self.node.position = position;
}

- (CGPoint) position
{
    return self.node.position;
}

- (BOOL) shakeHead: (BOOL) start
{
    if(start)
    {
        if(self.head.numberOfRunningActions == 0)
        {
            id test = [CCRepeatForever actionWithAction: [CCSequence actionOne:[CCRotateTo actionWithDuration:0.5f angle:kHEADANGLE] two: [CCRotateTo actionWithDuration: 0.5f angle: -kHEADANGLE]]];
            [self.head runAction: test];
            return YES;
        }
        return NO;
    }
    else
    {
        [self.head stopAllActions];
        [self.head runAction: [CCRotateTo actionWithDuration: 0.1f angle: 0]];
    }
    return YES;
}

- (void) jump
{
    [self.node runAction: [CCJumpTo actionWithDuration: 0.5f position: _initPos height:50 jumps: 1]];
    if(!_isJumping && self.hand.numberOfRunningActions == 0)
        [self.hand runAction: _handAnimation];
    
    _isJumping = YES;
}

- (void) update: (ccTime) dt
{
    if(_isJumping && self.hand.numberOfRunningActions == 0 && self.node.position.y == _initPos.y)
    {
        _isJumping = NO;
        [self testtest];
    }
}

- (void) testtest
{
    [self.hand setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"hand1.png"]];
}

- (void) playSound
{
    
}

- (void) dealloc
{
    [_handAnimation release];
    self.soundName = nil;
    [super dealloc];
}

- (BOOL) containsPoint: (CGPoint) pos
{
    CGRect rect = self.body.boundingBox;
    rect.origin = ccpAdd(rect.origin, self.node.position);
    return CGRectContainsPoint(rect, pos);
}

@end
