//
//  SoundSystem.h
//  MusicCat
//
//  Created by Jason Chang on 11/19/12.
//
//

#import <Foundation/Foundation.h>

@interface SoundSystem : NSObject

+ (SoundSystem*) system;

@property (assign, nonatomic) BOOL muteSound;
@property (assign, nonatomic) BOOL muteMusic;
@property (assign) float musicVolume;
@property (assign) float soundVolume;


@end
