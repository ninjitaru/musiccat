//
//  GridManager.m
//  MusicCat
//
//  Created by Jason Chang on 12/18/12.
//
//

#import "GridManager.h"
#import "MusicBall.h"

@implementation GridManager

- (void) dealloc
{
    [self freeGrid];
    [super dealloc];
}

- (id) initWithOffset: (CGPoint) offset
{
    self = [super init];
    if(self)
    {
        _gridOffset = offset;
        _winSize = [CCDirector sharedDirector].winSize;
        [self createGrid];
    }
    return self;
}

- (CGPoint) fitGridWithCellSize: (float) cellSize InScreenSize: (CGSize) screenSize
{
    return CGPointMake(0, 0);
}

- (CGPoint) range
{
    return ccp(_gridWidth, _gridHeight);
}

- (float) gridSize
{
    return _gridSize;
}

- (int) gridXCount
{
    return _gridXCount;
}

- (int) gridYCount
{
    return _gridYCount;
}

- (void) setIndex: (int) index withValue: (BOOL) value
{
    _gridMap[index] = value;
}

- (void) reset
{
    [self resetGrid];
}

- (int) randomAvaliableGridIndex
{
    int index = ((int)CCRANDOM_0_1()*1000)%_gridCount;
    int counter = 0;
    while(_gridMap[index] == YES)
    {
        index = (index+1)%_gridCount;
        counter++;
        if(counter == _gridCount)
            return -1;
    }
    return index;
}

- (BOOL) avaliableGridIndex: (int) index
{
    return (_gridMap[index] == NO);
}

- (CGPoint) gridIndexToPoint: (int) index
{
    int x = index % _gridXCount;
    int y = index / _gridXCount;
    float xx = x * _gridSize + _gridOffset.x + _gridSize/2;
    float yy = y * _gridSize + _gridOffset.y + _gridSize/2;
    return ccp(xx,yy);
}

#define SEARCHDEPTHLIMIT 3

- (int) findEmptyIndexFrom: (int) index searchDepth: (int) depth
{
    if(depth > SEARCHDEPTHLIMIT)
        return -1;

    int ni = -1;
    // try right
    if(index%_gridXCount != 8)
    {
        ni = index+1;
        if(_gridMap[ni] == NO)
            return ni;
    }
    //try up
    if(index+_gridXCount < _gridCount)
    {
        ni = index+_gridXCount;
        if(_gridMap[ni] == NO)
            return ni;
    }
    // try left
    if(index%_gridXCount != 0)
    {
        ni = index-1;
        if(_gridMap[ni] == NO)
            return ni;
    }
    // try bottom
    if(index-_gridXCount >= 0)
    {
        ni = index-_gridXCount;
        if(_gridMap[ni] == NO)
            return ni;
    }
    return [self findEmptyIndexFrom: ni searchDepth: depth+1];
}

- (void) createGrid
{
    CCSprite* temp = [CCSprite spriteWithSpriteFrameName: @"note_sound1.png"];
    _gridWidth = _winSize.width - _gridOffset.x * 2;
    _gridSize = temp.contentSize.width;

    _gridHeight = _winSize.height - _gridOffset.y - _gridSize * 0.5f;
    _gridXCount = _gridWidth/_gridSize;
    _gridYCount = _gridHeight/_gridSize;
    _gridCount = _gridXCount*_gridYCount;
    
    _gridMap = (BOOL*)calloc(_gridCount, sizeof(BOOL));
}

- (void) resetGrid
{
    for(int i = 0; i < _gridCount; i++)
    {
        _gridMap[i] = NO;
    }
}

- (void) freeGrid
{
    free(_gridMap);
}

- (int) pointToGridIndex: (CGPoint) pos
{
    float x = pos.x - _gridOffset.x;
    if(x < 0 || x >= _gridWidth)
        return -1;
    float y = pos.y - _gridOffset.y;
    if(y < 0 || y >= _gridHeight)
        return -1;
    
    int xi = x / _gridSize;
    int yi = y / _gridSize;
    int index = yi * _gridXCount + xi;
    return index;
}

@end
