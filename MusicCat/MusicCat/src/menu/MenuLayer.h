//
//  MenuLayer.h
//  MusicCat
//
//  Created by Jason Chang on 10/29/12.
//
//

#import "cocos2d.h"
#import "SceneLayer.h"
#import "IntroMenuManager.h"
#import "MainMenuManager.h"
#import "SingleStageMenuManager.h"

#import "StageInfo.h"

@interface MenuLayer : SceneLayer
{
    int _state;
    IntroMenuManager *_introManager;
    MainMenuManager* _mainManager;
    SingleStageMenuManager* _stageManager;
    NSArray* _stages;
    int _stageIndex;
}

+ (CCScene*) sceneToMenu;
+ (CCScene*) sceneToStage: (StageInfo*) stage;

- (void) readyMenu;
- (void) readyStage: (StageInfo*) stage;

@end
