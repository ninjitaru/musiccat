//
//  Song.m
//  MusicCat
//
//  Created by Jason Chang on 12/11/12.
//
//

#import "Song.h"

@implementation Song

- (float) timePerSection
{
    return self.secondPerBeat * 4;
}

- (int) trackCounts
{
    return self.trackTimings.count;
}

- (void) dealloc
{
    self.soundFileName = nil;
    self.trackTimings = nil;
    [super dealloc];
}

@end
