//
//  GameImageFileManager.m
//  MusicCat
//
//  Created by Jason Chang on 11/26/12.
//
//

#import "GameImageFileManager.h"
#import "UIImage+Scale.h"
//#import "CCFileUtils+DocumentSupport.h"

#define SCALENUMBER(a,s) (floorf(a*s))

@implementation GameImageFileManager

- (id) init
{
    self = [super init];
    if(self)
    {
        _fileUtil = [CCFileUtils sharedFileUtils];
        _deviceSize = [DeviceInfoHelper getDeviceType];
    }
    return self;
}

- (void) setupImages: (NSArray*) images
{
    for(NSString* imageName in images)
    {
        if([self deviceFileExist: imageName])
            continue;
        [self createImage: imageName forcePOT: NO];
        [self.delegate imagesReady: imageName];
    }
    [self.delegate completeProcessImages];
}

- (void) setupPlists: (NSArray*) lists
{
    for(NSString* file in lists)
    {
        if([self deviceFileExist: file])
            continue;
        [self adjustPList: file];
        [self.delegate imagesReady: file];
    }
    [self.delegate completeProcessPlist];
}

- (void) adjustPList: (NSString*) fileName
{
    NSString* source = [self fileName: fileName withSuffix: _fileUtil.iPadRetinaDisplaySuffix];
    NSString* dest = [self fileName: fileName withSuffix: [self getDeviceSuffix]];
    [self createPlist: dest fromFile: source withOriginalName: fileName];
}

- (float) getDeviceRatioFromIpadHD
{
    float scale = 1.0f;
    switch (_deviceSize)
    {
        case kiphone3gs:
            scale = 5/24.0f;
            break;
        case kiphone4:
            scale = 5/12.0f;
            break;
        case kiphone5:
            scale = 5/12.0f;
            break;
        case kipad:
            scale = 0.5f;
            break;
        case kipad3:
            break;
    }
    return scale;
}

- (void) createImage: (NSString*) imageName forcePOT: (BOOL) enablePOT
{
    NSString* source = [self fileName: imageName withSuffix: _fileUtil.iPadRetinaDisplaySuffix];
    NSString* dest = [self fileName: imageName withSuffix: [self getDeviceSuffix]];
    UIImage* temp = [UIImage imageFromFile: source];
    
    CGSize size = temp.size;
    float scale = [self getDeviceRatioFromIpadHD];
    
    size.width = floorf(size.width*scale);
    size.height = floorf(size.height*scale);
    
    temp = [temp scaleToSize: size forcePOT: enablePOT];
    [temp savetoDiskWithName: dest];
}

- (BOOL) deviceFileExist: (NSString*) imageName
{
    switch(_deviceSize)
    {
        case kiphone3gs:
            if([_fileUtil fileExistsAtPath: imageName withSuffix: nil])
            {
                return YES;
            }
            break;
        case kiphone4:
        case kiphone5:
            return [_fileUtil iPhoneRetinaDisplayFileExistsAtPath: imageName];
            break;
        case kipad:
            return [_fileUtil iPadFileExistsAtPath: imageName];
            break;
        case kipad3:
            return [_fileUtil iPadRetinaDisplayFileExistsAtPath: imageName];
            break;
    }
    
    return NO;
}

- (NSString*) getDeviceSuffix
{
    switch(_deviceSize)
    {
        case kiphone3gs:
            return nil;
        case kiphone4:
        case kiphone5:
            return  _fileUtil.iPhoneRetinaDisplaySuffix;
        case kipad:
            return _fileUtil.iPadSuffix;
        case kipad3:
            return _fileUtil.iPadRetinaDisplaySuffix;
    }
    return nil;
}

- (NSString*) fileName: (NSString*) imageName withSuffix: (NSString*) suffix
{
    NSString *newName = imageName;
    NSString *pathWithoutExtension = [imageName stringByDeletingPathExtension];
    NSString *name = [pathWithoutExtension lastPathComponent];
    
    if(suffix && suffix.length > 0)
    {
        // check if path already has the suffix.
        if( [name rangeOfString:suffix].location == NSNotFound )
        {
            NSString *extension = [imageName pathExtension];
            
            if( [extension isEqualToString:@"ccz"] || [extension isEqualToString:@"gz"] )
            {
                // All ccz / gz files should be in the format filename.xxx.ccz
                // so we need to pull off the .xxx part of the extension as well
                extension = [NSString stringWithFormat:@"%@.%@", [pathWithoutExtension pathExtension], extension];
                pathWithoutExtension = [pathWithoutExtension stringByDeletingPathExtension];
            }
            
            newName = [pathWithoutExtension stringByAppendingString:suffix];
            newName = [newName stringByAppendingPathExtension:extension];
        }
    }
    
    return newName;
}

- (void) createPlist: (NSString*) output fromFile: (NSString*) source withOriginalName: (NSString*) fileName
{
    NSString *path = [[CCFileUtils sharedFileUtils] fullPathFromRelativePath:source];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];

    NSString* textureName = [fileName stringByDeletingPathExtension];
    textureName = [textureName stringByAppendingPathExtension: @"png"];
    [self createImage: textureName forcePOT: YES];
    [self createPlistWithData: dict andName: output withTextureName: [self fileName: textureName withSuffix: [self getDeviceSuffix]]];
}

- (void) scaleDict: (NSMutableDictionary*) d key: (NSString*) k withScale: (float) s
{
    d[k] = @(SCALENUMBER([d[k] floatValue],s));
}

-(void) createPlistWithData: (NSDictionary*)dict andName: (NSString*) output withTextureName: (NSString*) textureName
{
    NSMutableDictionary* dictionary = [NSMutableDictionary dictionaryWithDictionary: dict];
    /*
     Supported Zwoptex Formats:
     ZWTCoordinatesFormatOptionXMLLegacy = 0, // Flash Version
     ZWTCoordinatesFormatOptionXML1_0 = 1, // Desktop Version 0.0 - 0.4b
     ZWTCoordinatesFormatOptionXML1_1 = 2, // Desktop Version 1.0.0 - 1.0.1
     ZWTCoordinatesFormatOptionXML1_2 = 3, // Desktop Version 1.0.2+
     */
    int format = 0;
    
    // check the format
    
    if([dictionary objectForKey:@"metadata"])
    {
        NSMutableDictionary *metadataDict = [NSMutableDictionary dictionaryWithDictionary: [dictionary objectForKey:@"metadata"]];
        format = [[metadataDict objectForKey:@"format"] intValue];
        metadataDict[@"textureFileName"] = textureName;
        dictionary[@"metadata"] = metadataDict;
    }
    NSDictionary *framesDict = [dictionary objectForKey:@"frames"];
    
    NSAssert( format >= 0 && format <= 3, @"format is not supported for CCSpriteFrameCache addSpriteFramesWithDictionary:textureFilename:");
//    NSDictionary *metadataDict = [dict objectForKey:@"metadata"];
//    if( metadataDict )
//        // try to read  texture file name from meta data
//        

    
    
    
    float scale = [self getDeviceRatioFromIpadHD];
    
    NSMutableDictionary* tempFrames = [NSMutableDictionary dictionaryWithCapacity: framesDict.count];
    
    // apply scale to all numbers
    for(NSString *frameDictKey in framesDict)
    {
        NSMutableDictionary *frameDict =  [NSMutableDictionary dictionaryWithDictionary: [framesDict objectForKey: frameDictKey]];
        tempFrames[frameDictKey] = frameDict;
        if(format == 0) {
            
            float x = [[frameDict objectForKey:@"x"] floatValue];
            float y = [[frameDict objectForKey:@"y"] floatValue];
            float w = [[frameDict objectForKey:@"width"] floatValue];
            float h = [[frameDict objectForKey:@"height"] floatValue];
            float ox = [[frameDict objectForKey:@"offsetX"] floatValue];
            float oy = [[frameDict objectForKey:@"offsetY"] floatValue];
            int ow = [[frameDict objectForKey:@"originalWidth"] intValue];
            int oh = [[frameDict objectForKey:@"originalHeight"] intValue];
            
            [self scaleDict: frameDict key: @"x" withScale: scale];
            [self scaleDict: frameDict key: @"y" withScale: scale];
            [self scaleDict: frameDict key: @"width" withScale: scale];
            [self scaleDict: frameDict key: @"height" withScale: scale];
            [self scaleDict: frameDict key: @"offsetX" withScale: scale];
            [self scaleDict: frameDict key: @"offsetY" withScale: scale];
            [self scaleDict: frameDict key: @"originalWidth" withScale: scale];
            [self scaleDict: frameDict key: @"originalHeight" withScale: scale];

            x = [[frameDict objectForKey:@"x"] floatValue];
            y = [[frameDict objectForKey:@"y"] floatValue];
            w = [[frameDict objectForKey:@"width"] floatValue];
            h = [[frameDict objectForKey:@"height"] floatValue];
            ox = [[frameDict objectForKey:@"offsetX"] floatValue];
            oy = [[frameDict objectForKey:@"offsetY"] floatValue];
            ow = [[frameDict objectForKey:@"originalWidth"] intValue];
            oh = [[frameDict objectForKey:@"originalHeight"] intValue];
            
        } else if(format == 1 || format == 2)
        {
            CGRect frame = CCRectFromString([frameDict objectForKey:@"frame"]);
            CGPoint offset = CCPointFromString([frameDict objectForKey:@"offset"]);
            CGSize sourceSize = CCSizeFromString([frameDict objectForKey:@"sourceSize"]);
            
            frame.origin.x = SCALENUMBER(frame.origin.x, scale);
            frame.origin.y = SCALENUMBER(frame.origin.y, scale);
            frame.size.width = SCALENUMBER(frame.size.width, scale);
            frame.size.height = SCALENUMBER(frame.size.height, scale);
            offset.x = SCALENUMBER(offset.x, scale);
            offset.y = SCALENUMBER(offset.y, scale);
            sourceSize.width = SCALENUMBER(sourceSize.width, scale);
            sourceSize.height = SCALENUMBER(sourceSize.height, scale);
            
            frameDict[@"frame"] = NSStringFromCGRect(frame);
            frameDict[@"offset"] = NSStringFromCGPoint(offset);
            frameDict[@"sourceSize"] = NSStringFromCGSize(sourceSize);
            
        } else if(format == 3)
        {
            // get values
            CGSize spriteSize = CCSizeFromString([frameDict objectForKey:@"spriteSize"]);
            CGPoint spriteOffset = CCPointFromString([frameDict objectForKey:@"spriteOffset"]);
            CGSize spriteSourceSize = CCSizeFromString([frameDict objectForKey:@"spriteSourceSize"]);
            CGRect textureRect = CCRectFromString([frameDict objectForKey:@"textureRect"]);
            
            textureRect.origin.x = SCALENUMBER(textureRect.origin.x, scale);
            textureRect.origin.y = SCALENUMBER(textureRect.origin.y, scale);
            textureRect.size.width = SCALENUMBER(textureRect.size.width, scale);
            textureRect.size.height = SCALENUMBER(textureRect.size.height, scale);
            spriteSize.width = SCALENUMBER(spriteSize.width, scale);
            spriteSize.height = SCALENUMBER(spriteSize.height, scale);
            spriteOffset.x = SCALENUMBER(spriteOffset.x, scale);
            spriteOffset.y = SCALENUMBER(spriteOffset.y, scale);
            spriteSourceSize.width = SCALENUMBER(spriteSourceSize.width, scale);
            spriteSourceSize.height = SCALENUMBER(spriteSourceSize.height, scale);
            
            frameDict[@"textureRect"] = NSStringFromCGRect(textureRect);
            frameDict[@"spriteSize"] = NSStringFromCGSize(spriteSize);
            frameDict[@"spriteOffset"] = NSStringFromCGPoint(spriteOffset);
            frameDict[@"spriteSourceSize"] = NSStringFromCGSize(spriteSourceSize);
        }
        
    }
    
    dictionary[@"frames"] = tempFrames;
    output = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent: output];
    if(![dictionary writeToFile: output atomically: YES])
    {
        NSLog(@"create file %@ failed", output);
    }
}



@end
