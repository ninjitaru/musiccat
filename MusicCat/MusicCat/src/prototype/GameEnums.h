//
//  GameEnums.h
//  MusicCat
//
//  Created by Jason Chang on 10/9/12.
//
//

#ifndef MusicCat_GameEnums_h
#define MusicCat_GameEnums_h

typedef enum
{
    sMusicSetup,
    sMusicComputer,
    sMusicPlayer,
} eMusicState;

typedef enum
{
    sDepthBG,
    sDepthCat,
    sDepthEffect,
    sDepthUI,
} eObjectDepth;

#endif
