//
//  MusicBar.m
//  MusicCat
//
//  Created by Jason Chang on 11/9/12.
//
//

#import "MusicBar.h"
#import "SpriteCreator.h"
#import "DeviceInfoHelper.h"
#import "GameConstants.h"

#define SHEETCOUNT 4
#define EFFECTCOUNT 6
#define SHEETSTARTOFFSET 2

@interface MusicBar ()

@property (assign) CCSpriteBatchNode* batch;
@property (assign) CCSprite* frame;
@property (assign) CCSprite* beginsymbol;
@property (assign) CCSprite* sheet1;
@property (assign) CCSprite* sheet2;
@property (assign) CCSprite* sheet3;
@property (assign) CCSprite* sheet4;
@property (assign) CCSprite* e1;
@property (assign) CCSprite* e2;
@property (assign) CCSprite* e3;
@property (assign) CCSprite* e4;
@property (assign) CCSprite* e5;
@property (assign) CCSprite* e6;


@end

@implementation MusicBar

- (void) dealloc
{
    [_nextNoteIndexArray release];
    [_notePreviews release];
    if(_sheets)
        free(_sheets);
    if(_effects)
        free(_effects);
    if(_effectSpeed)
        free(_effectSpeed);
    [super dealloc];
}

- (id) initWithParent: (CCNode*) parent
{
    self = [super init];
    if(self)
    {
        _colorMode = [GameConstants constants].colorMode;
        
        CGSize size = [CCDirector sharedDirector].winSize;
        
        NSArray* settings = @[
        @{@"type":@(typeBatch),
        @"texture":@"game_item.png",
        @"name":@"batch"},
        
        @{@"type":@(typeSprite),
        @"texture":@"sheetframe.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,0)],
        @"parentname":@"batch",
        @"depth":@5,
        @"name":@"frame"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"symbol_begin.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(3,7)],
        @"scale":@(0.9),
        @"parentname":@"batch",
        @"depth":@4,
        @"name":@"beginsymbol"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheet.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2*3,0)],
        @"parentname":@"batch",
        @"depth":@0,
        @"name":@"sheet4"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheet.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(size.width,0)],
        @"parentname":@"batch",
        @"depth":@1,
        @"name":@"sheet3"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheet.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,0)],
        @"parentname":@"batch",
        @"depth":@2,
        @"name":@"sheet2"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheet.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"parentname":@"batch",
        @"depth":@3,
        @"name":@"sheet1"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheeteffect_super1.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(0,70)],
        @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(0,100)],
        @"parentname":@"batch",
        @"depth":@1,
        @"name":@"e1"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheeteffect_super1.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(size.width,70)],
        @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width,100)],
        @"parentname":@"batch",
        @"depth":@1,
        @"name":@"e2"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheeteffect_super2.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(-10,68)],
        @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(-10,98)],
        @"parentname":@"batch",
        @"depth":@1,
        @"name":@"e3"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheeteffect_super2.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(size.width-10,68)],
        @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width-10,98)],
        @"parentname":@"batch",
        @"depth":@1,
        @"name":@"e4"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheeteffect_super2.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(-25,70)],
        @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(-25,100)],
        @"parentname":@"batch",
        @"depth":@1,
        @"name":@"e5"
        },
        
        @{@"type":@(typeSprite),
        @"texture":@"sheeteffect_super2.png",
        @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
        @"position": [NSValue valueWithCGPoint: CGPointMake(size.width-25,70)],
        @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width-25,100)],
        @"parentname":@"batch",
        @"depth":@1,
        @"name":@"e6"
        },
        
        ];

        [[SpriteCreator sharedCreator] addSprites: settings toNode: parent inObject: self];
                
        _effects = malloc(sizeof(CCSprite*)*EFFECTCOUNT);
        for(int i = 0; i < EFFECTCOUNT; i++)
        {
            _effects[i] = [self valueForKey: [NSString stringWithFormat:@"e%i",i+1]];
            _effects[i].visible = NO;
        }
        
        _sheets = malloc(sizeof(CCSprite*)*SHEETCOUNT);
        for(int i = 0; i < SHEETCOUNT; i++)
        {
            _sheets[i] = [self valueForKey: [NSString stringWithFormat:@"sheet%i",i+1]];
            CCSprite* bar = [CCSprite spriteWithSpriteFrameName: @"symbol_section.png"];
            bar.anchorPoint = ccp(0.5,0);
            bar.position = ccp(_sheets[i].contentSize.width,0);
            [_sheets[i] addChild: bar];
            _sheets[i].tag = i+1;
        }
        
        if(_colorMode)
        {
            [self setRandomColor: _sheets[2]];
            [self setRandomColor: _sheets[3]];
        }
        
    }
    return self;
}

- (float) height
{
    return self.frame.position.y + self.frame.contentSize.height;
}

#define PADDINGWIDTH 10
#define PADDINGHEIGHT 6

- (void) createNotePreview
{
    if(_colorMode)
        return;
    
    int beats = self.song.beatPerSection;
    // TODO fix hard code values
    float width = (_sheet1.contentSize.width - PADDINGWIDTH*2) / beats;
    float height = (_sheet1.contentSize.height - PADDINGHEIGHT*2)/self.song.trackCounts;
    float x = width - width/2 + PADDINGWIDTH;
    float y = height - height/2 + PADDINGHEIGHT*2;
    
    NSMutableArray* array = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < SHEETCOUNT; i++)
    {
        for(int trackindex = 0; trackindex < self.song.trackCounts; trackindex++)
        {
            NSString* noteImage = @"note_sound1.png";
            if(trackindex == 1)
                noteImage = @"note_sound2.png";
            else if(trackindex == 2)
                noteImage = @"note_sound3.png";
            for(int ni = 0; ni < beats; ni++)
            {
                CCSprite* sprite = [CCSprite spriteWithSpriteFrameName: noteImage];
                sprite.position = ccp(x+width*ni,y+height*trackindex);
                sprite.scale = height/sprite.contentSize.width;
                sprite.visible = NO;
                sprite.color = ccc3(60,60,60);
                [_sheets[i] addChild: sprite];
                [array addObject: sprite];
            }
        }
    }
    
    _notePreviews = [array retain];
    [array release];
    [self resetPreview];
}

- (CCNode*) node
{
    return self.batch;
}

- (void) resetPreview
{
    if(!_nextNoteIndexArray)
    {
        _nextNoteIndexArray = [[NSMutableArray alloc] init];
        
        for(int trackindex = 0; trackindex < self.song.trackCounts; trackindex++)
        {
            [_nextNoteIndexArray addObject: @0];
        }
    }
    
    int beats = self.song.beatPerSection;
    int startSheet = SHEETSTARTOFFSET;
    for(int i = startSheet; i < SHEETCOUNT; i++)
    {
        int offset = i * self.song.trackCounts*beats;
        for(int trackindex = 0; trackindex < self.song.trackCounts; trackindex++)
        {
            int trackoffset = trackindex * beats;
            NSArray* timing = self.song.trackTimings[trackindex];
            
            // reset note index tracking
            _nextNoteIndexArray[trackindex] = @0;
            
            int startIndex = (i-startSheet) * beats + 1;
            int endIndex = (i-startSheet+1) * beats;
            for(int j = 0; j < timing.count; j++)
            {
                int value = [timing[j] intValue];
                if(value >= startIndex && value <= endIndex)
                {
                    int indexvalue = offset+trackoffset+(value-(startIndex-1))-1;
                    CCSprite* sprite = _notePreviews[indexvalue];
                    sprite.visible = YES;
                }
                if(value > endIndex)
                {
                    break;
                }
            }
        }
    }
    _sheetLocation = 4 - SHEETSTARTOFFSET;
}

#define CHECKLINEPOSITIONX 40

- (void) update: (ccTime) dt
{
    CGPoint speed = ccp(_speed*dt,0);
    for(int i = 0; i < SHEETCOUNT; i++)
    {
        CGPoint pos = _sheets[i].position;
        _sheets[i].position = ccpAdd(pos, speed);
    }

    [self swapSheet];
    [self moveEffect: dt];
    
}

- (void) reset
{
    if(_colorMode)
    {
        
    }
    else
    {
        for(CCSprite* sprite in _notePreviews)
        {
            sprite.visible = NO;
            sprite.color = ccc3(60,60,60);
        }
        [self resetPreview];
    }
    [self resetSheet];
}
                      
- (void) resetSheet
{
    CGSize size = [CCDirector sharedDirector].winSize;
    _currentSection = 0;

    for(int i = 0; i < SHEETCOUNT; i++)
    {
        _sheets[i] = [self valueForKey: [NSString stringWithFormat:@"sheet%i",i+1]];
        _sheets[i].color = ccWHITE;
    }

    _sheets[0].position = ccp(0,0);
    _sheets[1].position = ccp(size.width/2,0);
    _sheets[2].position = ccp(size.width,0);
    _sheets[3].position = ccp(size.width*3/2,0);
    [self.batch reorderChild: _sheets[3] z: 0];
    [self.batch reorderChild: _sheets[2] z: 1];
    [self.batch reorderChild: _sheets[1] z: 2];
    [self.batch reorderChild: _sheets[0] z: 3];
    
    if(_colorMode)
    {
        [self setRandomColor: _sheets[2]];
        [self setRandomColor: _sheets[3]];
    }
}

- (void) moveEffect: (ccTime) dt
{
    if(self.showSuperMode)
    {
        CGPoint speed = ccp(dt,0);
        for(int i = 0; i < EFFECTCOUNT; i++)
        {
            CGPoint pos = _effects[i].position;
            _effects[i].position = ccpAdd(pos, ccpMult(speed, _effectSpeed[i]));
            if(i%2 == 1 && _effects[i].position.x <= 0)
            {
                CCSprite* temp = _effects[i-1];
                _effects[i-1] = _effects[i];
                _effects[i] = temp;
                _effects[i].position = ccpAdd(_effects[i-1].position, ccp([CCDirector sharedDirector].winSize.width,0));
            }
        }
    }
}

- (BOOL) addNote: (MusicBall*) ball ignoreRule: (BOOL) ignorerule
{
    BOOL correct = NO;
    if(_colorMode)
    {
        for(int i = 0; i < 3; i++)
        {
            CCSprite* sheet = _sheets[i];
            if(CGRectContainsPoint(sheet.boundingBox,ball.node.position))
            {
                NSLog(@"%i %i %i", sheet.color.r, sheet.color.g, sheet.color.b);
                switch (ball.trackIndex)
                {
                    case 0:
                        if(sheet.color.g == 255 && sheet.color.r == 0)
                            correct = YES;
                        break;
                    case 1:
                        if(sheet.color.r == 255 && sheet.color.g == 0)
                            correct = YES;
                        break;
                    case 2:
                        if(sheet.color.g == 255 && sheet.color.r == 255)
                            correct = YES;
                        break;
                    default:
                        break;
                }
            }
        }
    }
    else
    {
    
        int cIndex = [_nextNoteIndexArray[ball.trackIndex] intValue];
        int value = [_song.trackTimings[ball.trackIndex][cIndex] intValue];
        
        CCSprite* sprite = nil;
        correct = value == ball.noteIndex;
        if(ignorerule)
        {
            sprite = (CCSprite*)_notePreviews[[self previewIndexFromNote: value track: ball.trackIndex]];
            correct = YES;
        }
        else
        {
            sprite = (CCSprite*)_notePreviews[[self previewIndexFromNote: ball.noteIndex track: ball.trackIndex]];
        }
        // TODO assert that sprite is ALREADY VISIBLE
        _skippedNoteCount = 0;
        
        if(correct)
        {
            sprite.color = ccWHITE;
        }
        else
        {
            sprite.color = ccRED;
//            int newindex = [self settingIndexFromNote: ball.noteIndex track: ball.trackIndex];
//            
//            // handle skipped note
//            for(int trackIndex = 0; trackIndex < _nextNoteIndexArray.count; trackIndex++)
//            {
//                cIndex = [_nextNoteIndexArray[trackIndex] intValue];
//                for(int i = cIndex; i < newindex; i++)
//                {
//                    if(i >= ((NSArray*)_song.trackTimings[trackIndex]).count)
//                        break;
//                    int skippedValue = [_song.trackTimings[trackIndex][i] intValue];
//                    sprite = (CCSprite*)_notePreviews[[self previewIndexFromNote: skippedValue track: trackIndex]];
//                    sprite.color = ccYELLOW;
//                    _nextNoteIndexArray[trackIndex] = @(i+1);
//                    _skippedNoteCount++;
//                }
//            }
//            cIndex = newindex+1;
        }
        
        cIndex++;
        
        _nextNoteIndexArray[ball.trackIndex] = @(cIndex);
        if(ignorerule)
        {
            _latestNoteValueSeem = value;
        }
        else
            _latestNoteValueSeem = ball.noteIndex;
    }
    return correct;
}

- (void) skipNote:(MusicBall *)ball
{
    if(_colorMode)
        return;
    CCSprite* sprite = (CCSprite*)_notePreviews[[self previewIndexFromNote: ball.noteIndex track: ball.trackIndex]];
    sprite.color = ccYELLOW;
    
    _nextNoteIndexArray[ball.trackIndex] = @([_nextNoteIndexArray[ball.trackIndex] intValue]+1);
}

- (void) missNote: (MusicBall*) ball
{
    if(_colorMode)
        return;
    CCSprite* sprite = (CCSprite*)_notePreviews[[self previewIndexFromNote: ball.noteIndex track: ball.trackIndex]];
    sprite.color = ccBLUE;
    
    _nextNoteIndexArray[ball.trackIndex] = @([_nextNoteIndexArray[ball.trackIndex] intValue]+1);
}

- (int) skippedNoteCount
{
    return _skippedNoteCount;
}

- (int) settingIndexFromNote: (int) index track: (int) track
{
    // TODO this is stupid
    NSArray* timing = _song.trackTimings[track];
    for(int i = 0; i < timing.count; i++)
    {
        if(index == [timing[i] intValue])
        {
            return i;
        }
    }
    return -1;
}

- (int) previewIndexFromNote: (int) index track: (int) track
{
    // music note setting index start with 1!!! OMFG ... need to correct these mess
    int sheetIndex = ((index-1)/_song.beatPerSection + SHEETSTARTOFFSET)%4;
    int localNoteIndex = (index-1) % 4;
    return (sheetIndex * self.song.trackCounts + track)*self.song.beatPerSection + localNoteIndex;
}

- (int) latestNoteValueSeem
{
    return _latestNoteValueSeem;
}

#pragma mark - properties

- (void) setTimePerSection:(float)timePerSection
{
    _timePerSection = timePerSection;
    _speed = -(_sheet1.contentSize.width)/_timePerSection;
}

- (void) setShowSuperMode:(BOOL)showSuperMode
{
    if(_showSuperMode == showSuperMode)
        return;
    _showSuperMode = showSuperMode;
    if(_showSuperMode)
    {
        for(int i = 0; i < EFFECTCOUNT; i++)
            _effects[i].visible = YES;
        if(!_effectSpeed)
        {
            _effectSpeed = malloc(sizeof(float)*6);
        }
        _effectSpeed[0] = CCRANDOM_0_1()*-65;
        _effectSpeed[1] = _effectSpeed[0];
        _effectSpeed[2] = CCRANDOM_0_1()*-70;
        _effectSpeed[3] = _effectSpeed[2];
        _effectSpeed[4] = CCRANDOM_0_1()*-75;
        _effectSpeed[5] = _effectSpeed[4];
    }
    else
    {
        for(int i = 0; i < EFFECTCOUNT; i++)
        {
            _effects[i].visible = NO;
        }
        CGSize size = [CCDirector sharedDirector].winSize;
        if([DeviceInfoHelper getScreenSizeType] == kscreenSizeIpad)
        {
            _effects[0].position = ccp(0,100);
            _effects[1].position = ccp(size.width,100);
            _effects[2].position = ccp(-5,98);
            _effects[3].position = ccp(size.width-5,98);
            _effects[4].position = ccp(-25,100);
            _effects[5].position = ccp(size.width-25,100);
        }
        else
        {
            _effects[0].position = ccp(0,70);
            _effects[1].position = ccp(size.width,70);
            _effects[2].position = ccp(-5,68);
            _effects[3].position = ccp(size.width-5,68);
            _effects[4].position = ccp(-25,70);
            _effects[5].position = ccp(size.width-25,70);
        }
    }
}

#pragma mark - helpers

- (void) setRandomColor: (CCSprite*) sprite
{
    int value = ((int)(CCRANDOM_0_1() * 100)) % 3;
    switch (value)
    {
        case 0:
            sprite.color = ccGREEN;
            break;
        case 1:
            sprite.color = ccRED;
            break;
        case 2:
            sprite.color = ccYELLOW;
            break;
        default:
            break;
    }
}

- (void) swapSheet
{
    if(_sheets[0].position.x < -(_sheet1.contentSize.width+9))
    {
        _currentSection++;
        CCSprite* temp = _sheets[0];
        _sheets[0] = _sheets[1];
        _sheets[1] = _sheets[2];
        _sheets[2] = _sheets[3];
        _sheets[3] = temp;
        
        //[self removeNoteFrom: temp];
        self.deadSheetTag = temp.tag;
        self.needCleanSheet = YES;
        
        temp.color = ccWHITE;
        temp.position = ccp(_sheets[2].position.x+_sheet1.contentSize.width, 0);
        
        [self.batch reorderChild: _sheets[3] z: 0];
        [self.batch reorderChild: _sheets[2] z: 1];
        [self.batch reorderChild: _sheets[1] z: 2];
        [self.batch reorderChild: _sheets[0] z: 3];
        
        if(_colorMode)
        {
            [self setRandomColor: temp];
        }
        else
        {
            [self updatePreviewNote: temp.tag-1];
        }
    }
}

- (void) updatePreviewNote: (int) sheetIndex
{
    // TODO slow need optimize
    int beats = self.song.beatPerSection;
    int offset = sheetIndex * self.song.trackCounts*beats;
    int max = offset + self.song.trackCounts*beats;
    
    for(int index = offset; index < max; index++)
    {
        ((CCSprite*)_notePreviews[index]).visible = NO;
    }
    
    for(int trackindex = 0; trackindex < self.song.trackCounts; trackindex++)
    {
        int trackoffset = trackindex * beats;
        NSArray* timing = self.song.trackTimings[trackindex];
        int startIndex = _sheetLocation * beats + 1;
        int endIndex = (_sheetLocation+1) * beats;
        //NSLog(@"%i %i", startIndex, endIndex);
        for(int j = 0; j < timing.count; j++)
        {
            int value = [timing[j] intValue];
            if(value >= startIndex && value <= endIndex)
            {
                int indexvalue = offset+trackoffset+(value-(startIndex-1))-1;
                CCSprite* sprite = _notePreviews[indexvalue];
                sprite.color = ccc3(60,60,60);
                sprite.visible = YES;
            }
            if(value > endIndex)
            {
                break;
            }
        }
    }
    
    _sheetLocation++;
}

@end
