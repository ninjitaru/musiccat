//
//  MusicManager.m
//  MusicCat
//
//  Created by Jason Chang on 11/12/12.
//
//

#import "MusicManager.h"
#import "Song.h"

#define kTimerInterval 0.017f
#define kDEFAULTTrackCount 3

@implementation MusicManager

@synthesize song = _song;

- (void) setSong:(Song *)song
{
    if(_activeBGSound)
    {
        [self stopBGSound];
    }
    
    if(_song == song)
        return;
    [_song release];
    _song = [song retain];
    [self setupTrackTiming];
    
    _beatInterval = _song.timePerSection/_song.beatPerSection;
    _halfBeatInterval = _beatInterval/2;
}

- (void) dealloc
{
    [_noteSoundTiming release];
    [_noteSound release];
    [_song release];
    [_activeBGSound stop];
    [_activeBGSound release];
    [_pollingTimer invalidate];
    [_events release];
    [_firingTime release];
    [_firingIndex release];
    
    [super dealloc];
}

- (id) initWithSong:(Song *)song
{
    self = [super init];
    if(self)
    {
        _firingTime = [[NSMutableArray arrayWithCapacity: kDEFAULTTrackCount] retain];
        _firingIndex = [[NSMutableArray arrayWithCapacity: kDEFAULTTrackCount] retain];
        _events = [[NSMutableArray array] retain];
        
        _noteSoundTiming = [[NSMutableArray alloc] init];
        _noteSound = [[NSMutableArray alloc] init];
        
        self.song = song;
    }
    return self;
}

- (void) setupTrackTiming
{
    [_firingTime removeAllObjects];
    [_firingIndex removeAllObjects];
    for(NSArray* timing in _song.trackTimings)
    {
        [_firingTime addObject: timing];
        [_firingIndex addObject: @0];
    }
}

- (void) resetTrackTiming
{
    for(int i = 0; i < _firingIndex.count; i++)
    {
        _firingIndex[i] = @0;
    }
}

#pragma mark - properties

- (float) soundPosition
{
    return _activeBGSound.position;
}

- (float) soundPositionPercentage
{
    return _activeBGSound.position/_activeBGSound.durationInSeconds;
}

- (float) soundDuration
{
    return _activeBGSound.durationInSeconds;
}

#pragma mark - methods

- (void) playBGSound
{
    [self resetTrackTiming];
    
    if(!_activeBGSound)
    {
        _activeBGSound = [[[SimpleAudioEngine sharedEngine] soundSourceForFile: _song.soundFileName] retain];
        _activeBGSound.looping = NO;
    }
    
    [_activeBGSound setGain: 0.5];
    [_activeBGSound play];
    _pollingTimer = [NSTimer scheduledTimerWithTimeInterval: kTimerInterval target: self selector: @selector(countLoop) userInfo: nil repeats: YES];
}

- (void) preloadBGSound
{
    if(!_activeBGSound)
    {
        _activeBGSound = [[[SimpleAudioEngine sharedEngine] soundSourceForFile: _song.soundFileName] retain];
        _activeBGSound.looping = NO;
    }
}

- (void) pauseBGSound
{
    if(!_activeBGSound)
        return;
    
    [_pollingTimer invalidate];
    _pollingTimer = nil;
    [_activeBGSound pause];
}

- (void) resumeBGSound
{
    if(!_activeBGSound)
        return;
    
    [_activeBGSound play];
    [self countLoop];
    _pollingTimer = [NSTimer scheduledTimerWithTimeInterval: kTimerInterval target: self selector: @selector(countLoop) userInfo: nil repeats: YES];
}

- (void) stopBGSound
{
    [_noteSound removeAllObjects];
    [_noteSoundTiming removeAllObjects];
    [_pollingTimer invalidate];
    _pollingTimer = nil;
    [_activeBGSound stop];
    [_activeBGSound release];
    _activeBGSound = nil;
}

- (void) countLoop
{
    if(!_activeBGSound || !_activeBGSound.isPlaying)
    {
        if(!_activeBGSound.isPlaying)
        {
            for(id<MusicManagerDelegate> delegate in _events)
            {
                [delegate musicEnded: _activeBGSound.position];
            }
        }
        [self stopBGSound];
        return;
    }
    
    float soundpos = _activeBGSound.position;

    // handle play music sound
    while(_noteSoundTiming.lastObject)
    {
        if([_noteSoundTiming[0] floatValue] <= soundpos)
        {
            [[SimpleAudioEngine sharedEngine] playEffect: _noteSound[0]];
            [_noteSound removeObjectAtIndex: 0];
            [_noteSoundTiming removeObjectAtIndex: 0];
        }
        else
        {
            break;
        }
    }
    
    for(int i = 0; i < _firingTime.count; i++)
    {
        NSArray* array = _firingTime[i];
        int index = [_firingIndex[i] intValue];
        if(index >= array.count)
            continue;
        int beatIndex = [array[index] intValue];
        float fireTime = beatIndex * _beatInterval - _halfBeatInterval;
        if(soundpos >= fireTime)
        {
            for(id<MusicManagerDelegate> delegate in _events)
            {
                //NSLog(@"firing for %i", _currentIndex);
                [delegate eventFiredAt: soundpos forIndex: beatIndex forTrack: i];
            }
            _firingIndex[i] = @(index+1);
        }
    }
}

- (void) addDelegate: (id<MusicManagerDelegate>) delegate
{
    [_events addObject: delegate];
}

- (void) removeDelegate: (id<MusicManagerDelegate>) delegate
{
    [_events removeObject: delegate];
}

- (void) removeAllDelegate
{
    [_events removeAllObjects];
}


@end
