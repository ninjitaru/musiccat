//
//  HelloWorldLayer.m
//  backgroundhelper
//
//  Created by Jason Chang on 12/7/12.
//  Copyright Greamer 2012. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "StageInfo.h"
#import "SettingDataFactory.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation HelloWorldLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		
		[[SettingDataFactory sharedFactory] loadData];
        _stages = [SettingDataFactory sharedFactory].stages;
        
        StageInfo* thestage = _stages[0];
        bkManager = [[BackgroundManager alloc] initWithParentNode: self andStage: thestage];
        
        musicManager = [[MusicManager alloc] initWithSong: thestage.song];
        [musicManager playBGSound];
        NSLog(@"_stages is %@",_stages);
        [self scheduleUpdate];
        
	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [bkManager release];
    [_stages release];
    [musicManager release];
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
    
	// don't forget to call "super dealloc"
	[super dealloc];
}

- (void) update:(ccTime)delta
{
    [bkManager update: delta];
    [bkManager updateMusic: musicManager.soundPosition];
}

#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}
@end
