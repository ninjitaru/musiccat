//
//  CatBandBar.h
//  MusicCat
//
//  Created by Jason Chang on 12/24/12.
//
//

#import <Foundation/Foundation.h>
#import "BandCat.h"

@class MusicBall;

@interface CatBandBar : NSObject
{
    CCNode* _parentNode;
    NSMutableArray* _catsPool;
    NSMutableArray* _activeCats;
    float _initSpeed;
    float _currentSpeed;
    float _timeAccum;
    float _totalTime;
    float _speedIncrementCount;
    BandCat* _lastCat;
    BandCat* _firstCat;
    float _bandHeight;
}

@property (assign) BOOL start;

@property (readonly) float height;

- (id) initWithParentNode: (CCNode*) parent;
- (void) update: (ccTime) dt;
- (void) reset;

- (BOOL) addNote: (MusicBall*) ball;

@end
