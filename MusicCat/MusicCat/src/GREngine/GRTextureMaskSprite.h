//
//  GRTextureMaskSprite.h
//  MusicCat
//
//  Created by Jason Chang on 12/4/12.
//
//

#import "cocos2d.h"

@interface GRTextureMaskSprite : CCSprite
{
    CCRenderTexture* _renderSurface;
}

@property (nonatomic, retain) CCSprite* original;
@property (nonatomic, retain) CCSprite* mask;

+ (id) spriteWithSpriteFrameName:(NSString *)spriteFrameName maskSpriteFrameName:(NSString*) maskSpriteFrameName;
+ (id) spriteWithFile:(NSString *)filename maskFile: (NSString*) maskname;

- (id) initWithSpriteFrameName:(NSString *)spriteFrameName maskSpriteFrameName:(NSString*) maskSpriteFrameName;
- (id) initWithFile:(NSString *)filename maskFile: (NSString*) maskname;

@end
