//
//  GameConstants.m
//  MusicCat
//
//  Created by Jason Chang on 12/21/12.
//
//

#import "GameConstants.h"
#import "DeviceInfoHelper.h"

@implementation GameConstants

+ (GameConstants*) constants
{
    static GameConstants *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] init];
    });
    return _sharedClient;
}

- (id) init
{
    self = [super init];
    if(self)
    {
        self.catBandMode = YES;
        self.fullCatBellyCount = 10;
        self.comboTriggerValue = 25;
        self.randomMusicNotePositionChance = 0.2f;
        self.gridXOffset = 15;
        self.gridYDelta = 0.01f;
        if([DeviceInfoHelper getScreenSizeType] == kscreenSizeIpad)
        {
            self.perfectDragDelay = 0.9f;
        }
        else
        {
            self.perfectDragDelay = 0.6f;
        }
        self.colorMode = YES;
    }
    return self;
}

@end
