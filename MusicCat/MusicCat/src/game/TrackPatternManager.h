//
//  TrackPatternManager.h
//  MusicCat
//
//  Created by Jason Chang on 11/12/12.
//
//

#import <Foundation/Foundation.h>

typedef enum
{
    kTrackTypeArchLeft,
    kTrackTypeArchRight,
    kTrackTypeVerticalLine,
    kTrackTypeHorizontalLine,
    kTrackTypeZipVertical,
    kTrackTypeZipHorizontal,
    kTrackTypeHalfCircle,
    kTrackTypeReverseHalfCircle,
    kTrackTypeRandom,
} TrackPatternType;

typedef enum
{
    kMoveLine,
    kMoveArchBottom,
    kMoveArchTop,
    kMoveRandomBezier,
} TrackMovementType;

@interface TrackPatternManager : NSObject
{
    float _gridCellSize;
    CGSize _gridSize;
    NSDictionary* _methods;
}

- (CGPoint) generatePath: (TrackPatternType) type withIndex: (int) index outOf: (int) number range: (CGPoint) range cellSize: (float) cellsize;

@end
