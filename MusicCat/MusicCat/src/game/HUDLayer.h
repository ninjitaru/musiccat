//
//  HUDLayer.h
//  MusicCat
//
//  Created by Jason Chang on 11/18/12.
//
//

#import <UIKit/UIKit.h>
//#import "ScoreLabel.h"
#import "SystemFlowDelegate.h"
#import "MusicNoteStatusDelegate.h"
#import "ItemTriggerDelegate.h"

@interface HUDLayer : CCLayerColor <MusicNoteActionDelegate, MusicNoteStatusDelegate, ItemTriggerDelegate>
{
    CCSprite* _scoreUnderLine;
    
    NSArray* _labels; 
    
    //ScoreLabel* _scoreLabel;
    CCLabelBMFont *_pointLabel;
    float _pointLabelScale;
    CCLabelBMFont* _coinLabel;
    
    id _showMenuAction;
    id _hideMenuAction;
    
    BOOL _doubleInSection;
    int _coin;
    int _point;
    BOOL _comboStarted;
    int _comboCount;
    float _comboPointBonus;
    float _comboPointBonusMod;
    
    CCLabelBMFont *_finalPointLabel;
    CCLabelBMFont *_comboLabel;

}

@property (assign) BOOL pausedMode;
@property (assign, nonatomic) BOOL scoreMode;

@property (assign) id<SystemFlowDelegate> flowDelegate;

- (void) displayScore;

@end
