//
//  LoadingOperation.m
//  MusicCat
//
//  Created by Jason Chang on 11/27/12.
//
//

#import "LoadingOperation.h"

@implementation LoadingOperation

- (void)main {
    // a lengthy operation
    @autoreleasepool
    {
        if(self.isCancelled)
            return;
    }
}

@end
