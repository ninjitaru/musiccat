//
//  GamePersistantData.h
//  MusicCat
//
//  Created by Jason Chang on 12/20/12.
//
//

#import <Foundation/Foundation.h>

@interface GamePersistantData : NSObject

+ (GamePersistantData*) sharedData;

@property (assign) int playerCoins;

- (void) save;

@end
