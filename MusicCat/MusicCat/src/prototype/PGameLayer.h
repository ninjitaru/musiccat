//
//  GameLayer.h
//  MusicCatInvasion
//
//  Created by Jason Chang on 8/24/12.
//
//

#import "Cat.h"
#import "TouchTracker.h"
#import "SimpleAudioEngine.h"
#import "MusicManager2.h"
#import "GameEnums.h"
/*
 Note - individual music note
 group - every 4 note = 1 group
 */

@interface PGameLayer : CCLayer
{
    MusicManager2* _musicManager;
    TouchTracker* _tracker;
    
    NSArray* _cats;
    int* _trackNumberArray;
    
    CCLabelBMFont* _label;
    CCLabelBMFont* _loopLabel;
    float _gameTimeAccum;
    
    CCSprite* _hint1;
    CCSprite* _hint1Cat;
    CCSprite* _hint2;
    CCSprite* _hint2Cat;

    // song rule
    NSArray* _scoreTimingArray;
    NSArray* _scorePitchArray;
    
    // song state tracking
    BOOL _initedCurrentGroup;
    int _groupIndex;
    int _noteIndex;
    NSArray* _timings;  // noretain
    NSArray* _pitchs; // noretain
    
    // game music logic
    eMusicState _musicMode;
    int _activeModeGroupValue;
    BOOL _allowInput;
    int _playerInputNoteIndex;
    
    // in game menu , prototype stuff
    CCLabelBMFont* _pauseButton;
    CCLabelBMFont* _restartButton;
    NSArray* _pianoButtons;
    CGRect* _pianoButtonRect;
    CCSprite* _energyBar;
    CCSprite* _groupProgressBar;
    CCLabelBMFont* _comboLabel;
    CCLabelBMFont* _gameScoreLabel;
    int _comboCount;
    float _energyWidth;
    float _progressWidth;
    
}

+(CCScene *) scene;

@end
