//
//  SceneLayer.m
//  MusicCat
//
//  Created by Jason Chang on 10/29/12.
//
//

#import "SceneLayer.h"

@implementation SceneLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	CCLayer *layer = [self node];
	[scene addChild: layer];
	return scene;
}

- (id) init
{
    self = [super init];
    if(self)
    {
        [self initLayer];
    }
    return self;
}

- (void) initLayer
{
    NSLog(@"ran SceneLayer initLayer");
}

@end
