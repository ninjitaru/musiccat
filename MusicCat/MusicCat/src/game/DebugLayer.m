//
//  DebugLayer.m
//  MusicCat
//
//  Created by Jason Chang on 11/12/12.
//
//

#import "DebugLayer.h"

@implementation DebugLayer

- (void) draw
{
    CGPoint pos0 = ccp(80,100);
    CGPoint pos1 = ccp(90,300);
    CGPoint pos2 = ccp(390,300);
    CGPoint pos3 = ccp(400,100);
    //glEnable(GL_LINE_SMOOTH);
	//glColor4ub(255, 0, 255, 255);
	glLineWidth( 5.0f );
	ccDrawColor4B(255,0,0,255);
    ccDrawCubicBezier(pos0, pos1, pos2, pos3,24);
}

@end
