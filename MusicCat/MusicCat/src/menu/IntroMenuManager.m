//
//  IntroMenuManager.m
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "IntroMenuManager.h"
#import "cocos2d.h"
#import "MenuDepthEnum.h"
#import "SpriteCreator.h"

@interface IntroMenuManager ()

@property (assign) CCSprite* bg;
@property (assign) CCSpriteBatchNode* batch;

@end

@implementation IntroMenuManager

- (void) prepareLayer
{
    CGSize size = [CCDirector sharedDirector].winSize;
    NSArray* setup = @[
    
    @{@"type":@(typeSprite),
    @"purepng":@YES,
    @"texture":@"bg.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"depth":@(kDepthBackground),
    @"name":@"bg"
    },

    @{@"type":@(typeSprite),
    @"texture":@"item_stagefloor.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,0)],
    @"depth":@(kDepthForeground),
    @"tag":@0
    },

    @{@"type":@(typeSprite),
    @"purepng":@YES,
    @"texture":@"item_leftcurtain.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(1,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(110,size.height)],
    @"iphoneposition": [NSValue valueWithCGPoint: CGPointMake(110,size.height)],
    @"iphone5position": [NSValue valueWithCGPoint: CGPointMake(110,size.height)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(220,size.height)],
    @"depth":@(kDepthCurtain),
    @"tag":@1
    },

    @{@"type":@(typeSprite),
    @"purepng":@YES,
    @"texture":@"item_rightcurtain.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width-110,size.height)],
    @"iphoneposition": [NSValue valueWithCGPoint: CGPointMake(size.width-110,size.height)],
    @"iphone5position": [NSValue valueWithCGPoint: CGPointMake(size.width-110,size.height)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width-220,size.height)],
    @"depth":@(kDepthCurtain),
    @"tag":@2
    },

    @{@"type":@(typeSprite),
    @"texture":@"menu_title.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,size.height)],
    @"depth":@(kDepthBackGameButton),
    @"tag":@3
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_cats.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(110,-20)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(200,-20)],
    @"depth":@(kDepthFrontItem),
    @"tag":@4
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_meow.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(110,70)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(200,110)],
    @"depth":@(kDepthFrontItem),
    @"tag":@5
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"cat_shadow.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.75,size.height*0.1)],
    @"depth":@(kDepthBackItemShadow),
    @"tag":@6
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"menu_cat.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.73,size.height*0.1)],
//    @"position": [NSValue valueWithCGPoint: CGPointMake(350,30)],
    @"depth":@(kDepthBackItem),
    @"tag":@7
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_spotlight.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
//    @"position": [NSValue valueWithCGPoint: CGPointMake(350,size.height+20)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.73,size.height+20)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width*0.73,size.height+60)],
    @"depth":@(kDepthSpotLight),
    @"tag":@8
    }

    ];
    
    [[SpriteCreator sharedCreator] addSprites: setup toNode: _node inObject: self];
    
    _smallcatposition = [_node getChildByTag: 4].position;
    _smallmeowposition = [_node getChildByTag: 5].position;
    
    _allowAnimation = YES;
    _isRunningEndLayer = NO;
    [self startCatAnimation];
    [self startLightFlicking];
    
}

- (void) reAddMenu
{
    CGSize size = [CCDirector sharedDirector].winSize;
    NSArray* setup = @[

    @{@"type":@(typeSprite),
    @"texture":@"item_cats.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(110,-20)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(200,-20)],
    @"depth":@(kDepthFrontItem),
    @"tag":@4
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_meow.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(110,70)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(200,110)],
    @"depth":@(kDepthFrontItem),
    @"tag":@5
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"cat_shadow.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.75,size.height*0.1)],
    @"depth":@(kDepthFrontItemShadow),
    @"tag":@6
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"menu_cat.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.73,size.height*0.1)],
    //    @"position": [NSValue valueWithCGPoint: CGPointMake(350,30)],
    @"depth":@(kDepthFrontItem),
    @"tag":@7
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_spotlight.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    //    @"position": [NSValue valueWithCGPoint: CGPointMake(350,size.height+20)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.73,size.height+20)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width*0.73,size.height+60)],
    @"depth":@(kDepthSpotLight),
    @"tag":@8
    }

    ];
    
    _allowAnimation = YES;
    _isRunningEndLayer = NO;
    [[SpriteCreator sharedCreator] addSprites: setup toNode: _node inObject: self];
    
    [self startCatAnimation];
    [self startLightFlicking];
}

- (void) endLayer:(void (^)(void))block;
{
    if(_isRunningEndLayer)
        return;
    _isRunningEndLayer = YES;
    CCSprite* title = (CCSprite*)[_node getChildByTag: 3];
    [title stopAllActions];
    [title runAction: [CCSequence actions:
     [CCMoveTo actionWithDuration: 0.5f position: ccp(title.position.x, title.position.y+title.contentSize.height)],
                       [CCCallBlock actionWithBlock:
                        ^{
                            [_node removeChild: title cleanup: YES];
                            _isRunningEndLayer = NO;
                            block();
                        }],
                       nil]];
}

- (void) stopAnimations
{
    _allowAnimation = NO;
    CCSprite* meow = (CCSprite*)[_node getChildByTag: 5];
    CCSprite* cats = (CCSprite*)[_node getChildByTag: 4];
    
    CCSprite* light = (CCSprite*)[_node getChildByTag: 8];
    CCSprite* shadow = (CCSprite*)[_node getChildByTag: 6];
    [light stopAllActions];
    [shadow stopAllActions];
    [cats stopAllActions];
    [meow stopAllActions];
    meow.visible = NO;
}

- (void) startCatAnimation
{
    CCSprite* meow = (CCSprite*)[_node getChildByTag: 5];
    meow.visible = NO;
    [NSTimer scheduledTimerWithTimeInterval: CCRANDOM_0_1()*4.0f target: self selector: @selector(performCatAnimation) userInfo: nil repeats: NO];
}

- (void) performCatAnimation
{
    if(!_allowAnimation)
        return;
    
    CCSprite* cats = (CCSprite*)[_node getChildByTag: 4];
    CCSprite* meow = (CCSprite*)[_node getChildByTag: 5];
    [cats stopAllActions];
    [meow stopAllActions];
    
    [cats runAction: [CCSequence actions:
                      [CCMoveTo actionWithDuration: 0.3f+CCRANDOM_0_1()*0.2f position: ccp(_smallcatposition.x,0)],
                      [CCDelayTime actionWithDuration: 0.2f],
                      [CCMoveTo actionWithDuration: 0.2f+CCRANDOM_0_1()*0.2f position: ccp(_smallcatposition.x,CCRANDOM_0_1()*_smallcatposition.y)],
                      nil]];
    [meow runAction: [CCSequence actions:
                      [CCDelayTime actionWithDuration: 0.3f],
                      [CCShow action],
                      [CCDelayTime actionWithDuration: 0.2f],
                      [CCHide action],
                      nil]];
    
    [NSTimer scheduledTimerWithTimeInterval: CCRANDOM_0_1()*4.0f target: self selector: @selector(performCatAnimation) userInfo: nil repeats: NO];
}

- (void) startLightFlicking
{
    if(!_allowAnimation)
        return;
    
    [self flickLight];
    [NSTimer scheduledTimerWithTimeInterval: 4+CCRANDOM_0_1()*5.0f target: self selector: @selector(startLightFlicking) userInfo: nil repeats: NO];
}

- (void) openSpotLight
{
    CCSprite* light = (CCSprite*)[_node getChildByTag: 8];
    CCSprite* shadow = (CCSprite*)[_node getChildByTag: 6];
    [light stopAllActions];
    [shadow stopAllActions];
    
    [light runAction: [CCFadeIn actionWithDuration: 0.5f]];
    [shadow runAction: [CCFadeTo actionWithDuration: 0.5f opacity: 255]];
}

- (void) flickLight
{
    CCSprite* light = (CCSprite*)[_node getChildByTag: 8];
    CCSprite* shadow = (CCSprite*)[_node getChildByTag: 6];
    [light stopAllActions];
    [shadow stopAllActions];
    
    [light runAction: [CCSequence actions:
                       [CCFadeTo actionWithDuration: 0.2f opacity: 50],
                       [CCFadeTo actionWithDuration: 0.1f opacity: 150],
                       [CCFadeTo actionWithDuration: 0.3f opacity: 75],
                       [CCFadeTo actionWithDuration: 0.3f opacity: 255],
                       nil]];
    [shadow runAction: [CCSequence actions:
                        [CCFadeTo actionWithDuration: 0.2f opacity: 50],
                        [CCFadeTo actionWithDuration: 0.1f opacity: 150],
                        [CCFadeTo actionWithDuration: 0.3f opacity: 75],
                        [CCFadeTo actionWithDuration: 0.3f opacity: 255],
                        nil]];
}

- (void) closeSpotLight
{
    CCSprite* light = (CCSprite*)[_node getChildByTag: 8];
    CCSprite* shadow = (CCSprite*)[_node getChildByTag: 6];
    [light stopAllActions];
    [shadow stopAllActions];
    
    [light runAction: [CCFadeOut actionWithDuration: 0.2f]];
    [shadow runAction: [CCFadeTo actionWithDuration: 0.2f opacity: 200]];
}

@end
