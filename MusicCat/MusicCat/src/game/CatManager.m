//
//  CatManager.m
//  MusicCat
//
//  Created by Jason Chang on 12/11/12.
//
//

#import "CatManager.h"
#import "GameDepthEnum.h"

@implementation CatManager

- (void) initLayerManager
{
    CGSize size = [CCDirector sharedDirector].winSize;
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"seacat_motion.plist"];
    _cat = [CCSprite spriteWithSpriteFrameName: @"sea_cat_general_1.png"];
    NSArray *array =@[
    [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"sea_cat_general_1.png"],
    [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"sea_cat_general_2.png"],
    [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"sea_cat_general_2.png"],
    [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"sea_cat_general_1.png"],
    [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"sea_cat_general_3.png"],
    [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"sea_cat_general_3.png"],
    ];
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:array delay:0.3f];
    CCAnimate *action =[CCAnimate actionWithAnimation:animation];
    [_cat runAction: [CCRepeatForever actionWithAction: action]];
    _cat.anchorPoint = ccp(0.5f, 0);
    //NSLog(@"%f", size.height * (75.0f/320));
    _cat.position = ccp(size.width/2, size.height* (75.0f/320));
    [_parentNode addChild: _cat z: kDepthCat];}

- (void) restart
{
    
}

- (void) update: (ccTime) dt
{
    
}

- (void) placeNote: (BOOL) right onTime: (BOOL) perfect skippedNotes: (int) count atLocation:(CGPoint)pos
{
    //NSLog(@"note played %i withskippedCount: %i", correctness, skippedCount);
}

- (void) noteMissed
{
    //NSLog(@"note missed");
}

- (void) comboStarted
{
    //NSLog(@"combo start!!");
}

- (void) comboEnded
{
    //NSLog(@"combo end!!");
}

@end
