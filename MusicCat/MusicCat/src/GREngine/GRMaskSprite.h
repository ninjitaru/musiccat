//
//  GRMaskSprite.h
//  MusicCat
//
//  Created by Jason Chang on 12/3/12.
//
//

#import "cocos2d.h"

@interface GRMaskSprite : CCSprite
{
    CCTexture2D * _maskTexture;
    GLuint _textureLocation;
    GLuint _maskLocation;
}

@end
