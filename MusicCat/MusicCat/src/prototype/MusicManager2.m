//
//  MusicManager.m
//  MusicCat
//
//  Created by Jason Chang on 9/17/12.
//
//

#import "MusicManager2.h"

#define kTimerErrorAllowance 0.01f
#define kTimerInterval 0.017f

/*
 TODO handle phone interrupt
 TODO pause the game when enter background
 TODO save the time when enter background, replay from there
 */

@implementation MusicManager2

- (void) dealloc
{
    [_activeBGSound stop];
    [_activeBGSound release];
    [_pollingTimer invalidate];
    [super dealloc];
}

- (void) playBGSound: (NSString*) sound
{
    if(_activeBGSound)
    {
        [self stopBGSound];
    }
    
    _previousBGPosition = 0;
    _groupPosition = 0;
    _incremented = NO;
    _stopping = NO;
    
    _activeBGSound = [[[SimpleAudioEngine sharedEngine] soundSourceForFile: sound] retain];
    _activeBGSound.looping = YES;
    [_activeBGSound play];
    _pollingTimer = [NSTimer scheduledTimerWithTimeInterval: kTimerInterval target: self selector: @selector(countLoop) userInfo: nil repeats: YES];
}

- (void) pauseBGSound
{
    if(!_activeBGSound)
        return;
    
    [_pollingTimer invalidate];
    _pollingTimer = nil;
    [_activeBGSound pause];
}

- (void) resumeBGSound
{
    if(!_activeBGSound)
        return;
    
    [_activeBGSound play];
    [self countLoop];
    _pollingTimer = [NSTimer scheduledTimerWithTimeInterval: kTimerInterval target: self selector: @selector(countLoop) userInfo: nil repeats: YES];
}

- (void) stopBGSound
{
    [_pollingTimer invalidate];
    _pollingTimer = nil;
    [_activeBGSound stop];
    [_activeBGSound release];
    _activeBGSound = nil;
}

- (void) smoothStopBGSound
{
    if(_activeBGSound)
    {
        _activeBGSound.looping = NO;
        _stopping = YES;
    }
}

- (void) countLoop
{
    if(!_activeBGSound || !_activeBGSound.isPlaying)
    {
        [self stopBGSound];
        return;
    }
    
    if(_stopping)
        return;
    
    float pos = _activeBGSound.position;
    if(pos < _previousBGPosition)
    {
        _groupPosition++;
        [self.delegate groupStarted];
    }

    _previousBGPosition = pos;
}

- (float) soundPosition
{
    return _activeBGSound.position;
}

- (float) soundPositionPercentage
{
    return _activeBGSound.position/_activeBGSound.durationInSeconds;
}

@end
