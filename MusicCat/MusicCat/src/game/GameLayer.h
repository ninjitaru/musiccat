//
//  GameLayer3.h
//  MusicCat
//
//  Created by Jason Chang on 10/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "TouchTracker.h"

#import "MusicManager.h"
#import "BackgroundManager.h"
#import "MusicNoteManager.h"
#import "StageInfo.h"
#import "CatManager.h"
#import "TrackPatternManager.h"
#import "ItemManager.h"

#import "SystemFlowDelegate.h"

@class HUDLayer;

@interface GameLayer : CCLayer <MusicManagerDelegate, SystemFlowDelegate>
{
    StageInfo* _stageInfo;
    
    CCLabelBMFont* _readyLabel;
    CCLabelBMFont* _goLabel;
    
    ItemManager* _itemManager;
    MusicNoteManager* _noteManager;
    CatManager* _catManager;
    BackgroundManager* _bgManager;
    MusicManager* _musicManager;
    TrackPatternManager* _trackManager;
    
    TouchTracker* _tracker;
    
    BOOL _paused;
    BOOL _gameStart;

    float _previousTime;
    float _gameTime;
}

+ (CCScene *) sceneWithStage: (StageInfo*) stage;

@property (nonatomic, assign) BOOL begin;
@property (assign) HUDLayer* hud;

- (id) initWithStage: (StageInfo*) stage;


@end
