//
//  IntroMenuManager.h
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "MenuManager.h"

@interface IntroMenuManager : MenuManager
{
    BOOL _allowAnimation;
    CGPoint _smallcatposition;
    CGPoint _smallmeowposition;
}

- (void) stopAnimations;
- (void) closeSpotLight;

- (void) reAddMenu;
- (void) startCatAnimation;
- (void) startLightFlicking;

@end
