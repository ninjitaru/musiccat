//
//  DebugController.m
//  MusicCat
//
//  Created by Jason Chang on 12/21/12.
//
//

#import "DebugController.h"

@implementation DebugController

- (void) dealloc
{
    [_delegates release];
    [super dealloc];
}

+ (DebugController*) debugger
{
    static DebugController *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] init];
    });
    return _sharedClient;
}

- (void) setDebugOn:(BOOL)debugOn
{
    _debugOn = debugOn;
    for(id<DebugDelegate> delegate in _delegates)
    {
        [delegate setDebug: _debugOn];
    }
}

- (void) registerDebugDelegate: (id<DebugDelegate>) delegate
{
    if(!_delegates)
    {
        _delegates = [[NSMutableArray alloc] init];
    }
    [_delegates addObject: delegate];
}

- (void) removeDebugDelegate: (id<DebugDelegate>) delegate
{
    [_delegates removeObject: delegate];
}

- (void) removeAllDebugDelegate
{
    [_delegates removeAllObjects];
}


@end
