//
//  MenuManager.h
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "cocos2d.h"

@interface MenuManager : NSObject
{
    CCNode* _node;
    BOOL _isRunningEndLayer;
}

- (id) initWithLayer: (CCNode*) node;
- (void) prepareLayer;
- (void) endLayer:(void (^)(void))block;

@end
