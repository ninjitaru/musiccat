//
//  SpriteCreator.h
//  MusicCat
//
//  Created by Jason Chang on 11/19/12.
//
//

#import <Foundation/Foundation.h>
#import "DeviceInfoHelper.h"

typedef enum
{
    typeSprite = 0,
    typeTouchableSprite,
    typeMaskSprite,
    typeBatch,
} kSpriteType;

@interface SpriteCreator : NSObject
{
    NSDictionary* spriteProcessorMap;
    CCNode* _currentNode;
    id _currentObject;
    enumScreenSizeType _screenSizeType;
    NSString* _screenTypePositionString;
}

+ (SpriteCreator*) sharedCreator;

- (void) addSprites: (NSArray*) settings toNode: (CCNode*) node inObject: (id) object;
- (void) addAnimations: (NSArray*) datas inObject: (id) object;

@end
