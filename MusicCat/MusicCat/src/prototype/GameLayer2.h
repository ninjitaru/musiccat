#import "Cat.h"
#import "TouchTracker.h"
#import "SimpleAudioEngine.h"
#import "MusicManager2.h"
#import "GameEnums.h"

/*
 Note - individual music note
 group - every 4 note = 1 group
 */

@interface GameLayer2 : CCLayer
{
    MusicManager2* _musicManager;
    TouchTracker* _tracker;
    
    NSArray* _cats;
    int* _trackNumberArray;
    
    CCLabelBMFont* _label;
    CCLabelBMFont* _loopLabel;
    float _gameTimeAccum;
    
    CCSprite* _hint1;
    CCSprite* _hint1Cat;
    CCSprite* _hint2;
    CCSprite* _hint2Cat;
    
    // song rule
    NSArray* _scoreTimingArray;
    NSArray* _scorePitchArray;
    
    // song state tracking
    BOOL _initedCurrentGroup;
    int _groupIndex;
    int _noteIndex;
    NSArray* _timings;  // noretain
    NSArray* _pitchs; // noretain
    
    // game music logic
    eMusicState _musicMode;
    int _activeModeGroupValue;
    BOOL _allowInput;
    int _playerInputNoteIndex;
    
    // in game menu , prototype stuff
    CCLabelBMFont* _pauseButton;
    CCLabelBMFont* _restartButton;
    NSArray* _pianoButtons;
    CGRect* _pianoButtonRect;
    CCSprite* _energyBar;
    CCSprite* _groupProgressBar;
    CCLabelBMFont* _comboLabel;
    CCLabelBMFont* _gameScoreLabel;
    int _comboCount;
    float _energyWidth;
    float _progressWidth;
    
    NSArray* _soundNames;
    NSMutableArray* _activeBallArray;
    NSMutableArray* _trashCan;
    int _bubbleCounter;
    int _trackCounter;
    
    float _testScale;
}

//+(CCScene *) scene;

@end
