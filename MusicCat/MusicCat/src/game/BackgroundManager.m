//
//  BackgroundManager.m
//  MusicCat
//
//  Created by Jason Chang on 11/5/12.
//
//

#import "BackgroundManager.h"
#import "GameDepthEnum.h"
#import "SpriteCreator.h"

@interface BackgroundManager ()

@end

@implementation BackgroundManager

- (void) dealloc
{
    [_spriteMap release];
    [_animationMap release];
    [super dealloc];
}

- (id) initWithParentNode:(CCNode *)node andStage:(StageInfo *)stage
{
    self = [super initWithParentNode: node];
    if(self)
    {
        _stage = stage;
        [self initWithStageInfo];
    }
    return self;
}

- (void) initWithStageInfo
{
    _spriteMap = [[NSMutableDictionary alloc] init];
    _animationMap = [[NSMutableDictionary alloc] init];
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    [cache addSpriteFramesWithFile: _stage.plistFile];
    
    [[SpriteCreator sharedCreator] addSprites: _stage.spriteData toNode: _parentNode inObject: _spriteMap];
    [[SpriteCreator sharedCreator] addAnimations: _stage.animationData inObject: _animationMap];
    
    [self startAnimation: _stage.fixedAnimationPair];
    [self playMusicAnimation: _stage.musicAnimationPair];
}

- (void) startAnimation: (NSArray*) animationInfo
{
    // NSLog(@"animationInfo is %@, _spriteMap is %@, _animationMap is %@",animationInfo, _spriteMap[@"bat1"], _animationMap[@"bat1"]);
    
    // [_spriteMap[@"bat1"] runAction:[CCRepeatForever actionWithAction:_animationMap[@"bat1"]]];
    // [_spriteMap[@"bat2"] runAction:[CCRepeatForever actionWithAction:_animationMap[@"bat2"]]];
    // [_spriteMap[@"bat3"] runAction:[CCRepeatForever actionWithAction:_animationMap[@"bat3"]]];
    // [_spriteMap[@"ghost1"] runAction:[CCRepeatForever actionWithAction:_animationMap[@"ghost1"]]];
    // [_spriteMap[@"tree1"] runAction:[CCRepeatForever actionWithAction:_animationMap[@"tree1"]]];

    //    [self.fish1 runAction: _animationMap[@"fish1"]];
    //    [self.fish2 runAction: _animationMap[@"fish2"]];
    //    [self.fish3 runAction: _animationMap[@"fish3"]];
    //
    //    [self.seaweed1 runAction: _animationMap[@"seaweed1"]];
    //    [self.seaweed2 runAction: _animationMap[@"seaweed2"]];
    //    [self.seaweed3 runAction: _animationMap[@"seaweed3"]];
    //    [self.seaweed4 runAction: _animationMap[@"seaweed4"]];
}

- (void) playMusicAnimation: (NSArray*) animationInfo
{
    
}

- (void) updateMusic: (float) musicDelta
{
    
}

- (void) update:(ccTime)dt
{
    if(_stage.updateBlock)
    {
        _stage.updateBlock(_spriteMap, _animationMap, dt);
    }
}

@end
