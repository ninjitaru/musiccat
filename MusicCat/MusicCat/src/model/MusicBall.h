//
//  MusicBall.h
//  MusicCat
//
//  Created by Jason Chang on 11/9/12.
//
//

#import <Foundation/Foundation.h>

@class TouchTracker;
@class CCBlade;

typedef enum
{
    kMusicBallHeart,
    kMusicBallStar,
    kMusicBallShape,
} kMusicBallType;

@class GridManager;

@interface MusicBall : NSObject
{
    CCSprite* _sprite;
    
    CCSprite* _spirit;
    id _spiritAction;
    
    CCSprite* _leftWing;
    CCSprite* _rightWing;
    id _leftWingAction;
    id _rightWingAction;
    
    CCSprite* _magnetRing;
    id _magnetRingAction;
    
    float _internalTime;
}

@property (assign) BOOL autoPilot;
@property (assign) CGPoint start;
@property (assign) CGPoint end;

@property (assign) MusicBall* previous;
@property (assign) MusicBall* next;
@property (readonly) NSMutableSet* siblings;
@property (assign) TouchTracker* touchTracker;
@property (assign) int touchTrackNumber;
@property (assign) CCBlade* blade;

@property (assign) GridManager* gridManager;
@property (assign) int gridIndex;

@property (assign) int noteIndex;
@property (assign) int trackIndex;

@property (assign) float birthTime;
@property (assign, nonatomic) float timeToDeath;

@property (assign) BOOL shouldFloat;
@property (assign, nonatomic) BOOL isDead;
@property (assign) BOOL onSheet;
@property (assign) int sheetTag;
@property (readonly) BOOL shouldDelete;
@property (nonatomic, assign) BOOL showSpirit;
@property (nonatomic, assign) BOOL showWing;
@property (nonatomic, assign) BOOL showMagnet;

@property (assign, nonatomic) float internalTime;

@property (nonatomic, assign) kMusicBallType type;

@property (readonly) CCSprite* node;

- (id) initWithParent: (CCNode*) parent;
- (void) randomType;
- (void) typeByTrack: (int) trackNum;
- (void) update: (ccTime) dt;
- (void) reset;

@end
