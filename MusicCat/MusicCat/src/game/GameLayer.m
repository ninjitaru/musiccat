//
//  GameLayer.m
//  MusicCat
//
//  Created by Jason Chang on 10/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameLayer.h"
#import "GameDepthEnum.h"
#import "HUDLayer.h"
#import "MenuLayer.h"

@implementation GameLayer

- (void) dealloc
{
    [_itemManager release];
    [_musicManager release];
    [_noteManager release];
    [_tracker release];
    [_stageInfo release];
    [_bgManager release];
    [super dealloc];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}

+ (CCScene *) sceneWithStage: (StageInfo*) stage
{
    CCScene *scene = [CCScene node];
	GameLayer *layer = [[[self alloc] initWithStage: stage] autorelease];
    HUDLayer* uilayer = [[HUDLayer alloc] initWithColor: ccc4(0, 0, 0, 0)];
    uilayer.flowDelegate = layer;
    layer.hud = uilayer;
	[scene addChild: layer];
    [scene addChild: uilayer];
	return scene;
}

- (id) initWithStage: (StageInfo*) stage
{
    self = [super init];
    if(self)
    {
        _stageInfo = [stage retain];
        [self initLayer];
    }
    return self;
}

- (void) loadResources
{
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"game_item.plist"];
}

- (void) initLayer
{
    [self loadResources];
    
    CGSize size = [CCDirector sharedDirector].winSize;
    //[CCTexture2D setDefaultAlphaPixelFormat: kCCTexture2DPixelFormat_RGBA4444];
    _tracker = [[TouchTracker alloc] init];
    
    //_catManager = [[CatManager alloc] initWithParentNode: self];
    _musicManager = [[MusicManager alloc] initWithSong: _stageInfo.song];
    _bgManager = [[BackgroundManager alloc] initWithParentNode: self andStage: _stageInfo];

    _noteManager = [[MusicNoteManager alloc] initWithParentNode: self andSong: _stageInfo.song andTouchTracker: _tracker];
    _noteManager.cat = _catManager.cat;
    _noteManager.musicManager = _musicManager;
    
    _itemManager = [[ItemManager alloc] initWithParentNode: self];
    _itemManager.difficulty = _stageInfo.difficulty;
    
    _readyLabel = [CCLabelBMFont labelWithString: @"Ready!" fntFile: @"font.fnt"];
    _readyLabel.position = ccp(size.width/2,size.height/2);
    _readyLabel.visible = NO;
    [self addChild: _readyLabel z: kDepthFront];
    
    _goLabel = [CCLabelBMFont labelWithString: @"GO" fntFile: @"font.fnt"];
    _goLabel.position = ccp(size.width/2, size.height/2);
    _goLabel.visible = NO;
    [self addChild: _goLabel z: kDepthFront];

    _gameStart = NO;
    [self scheduleUpdate];
    self.isTouchEnabled = YES;
}

- (void) onEnterTransitionDidFinish
{
    // setup communication
    
    //[_noteManager registerActionDelegatation: _catManager];
    [_noteManager registerActionDelegatation: self.hud];
    [_noteManager registerStatusDelegatation: _itemManager];
    [_noteManager registerStatusDelegatation: self.hud];
    [_itemManager registerDelegatation: _noteManager];
    [_itemManager registerDelegatation: self.hud];
    
    // end
    
    [super onEnterTransitionDidFinish];
    // TODO move preload to loading
    [self setupMusic];
    [self playStartAnimation];
}

- (void) setupMusic
{
    [_musicManager stopBGSound];
    [_musicManager preloadBGSound];
    [_musicManager removeAllDelegate];
    [_musicManager addDelegate: self];
}

- (void) onExitTransitionDidStart
{
    [super onExitTransitionDidStart];
    [_musicManager stopBGSound];
}

- (void) restartGame
{
    _previousTime = 0;
    _gameTime = 0;
    _gameStart = NO;
    [_tracker clear];
    [_noteManager restart];
    
    [self setupMusic];
    [_itemManager restart];
    [_catManager restart];
    
    [self playStartAnimation];
}

- (void) playStartAnimation
{
    _readyLabel.scale = 1;
    _readyLabel.opacity = 255;
    _readyLabel.visible = YES;
    _goLabel.visible = YES;
    
    [_readyLabel runAction: [CCSpawn actionOne: [CCFadeOut actionWithDuration: 0.5f] two: [CCScaleTo actionWithDuration: 0.5f scaleX: 2.0f scaleY: 2.5f]]];
    _goLabel.opacity = 0;
    _goLabel.scale = 2;
    [_goLabel runAction: [CCSequence actions:
                          [CCDelayTime actionWithDuration: 0.6f],
                          [CCSpawn actions:
                           [CCFadeIn actionWithDuration: 0.5f],
                           [CCScaleTo actionWithDuration: 0.5f scale: 1],
                           nil],
                          [CCDelayTime actionWithDuration: 0.4f],
                          [CCCallBlock actionWithBlock:
                           ^{
                               [self startGame];
                           }],
                          nil]];
}

- (void) startGame
{
    _goLabel.visible = NO;
    _readyLabel.visible = NO;
    _gameStart = YES;
    
    [_musicManager playBGSound];
    
}

- (void) update: (ccTime) dt
{
    if(_paused)
        return;
    
    if(_gameStart)
    {
        _gameTime += dt;
        
        float diff = _musicManager.soundPosition - _previousTime;
        [_catManager update: dt];
        [_bgManager update: diff];
        [_noteManager updateMusic: diff];
        _previousTime = _musicManager.soundPosition;
        
        [_noteManager update: dt];
        [_itemManager update: dt];
    }
}

- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_paused || !_gameStart)
        return;
    for(UITouch* touch in touches)
    {
        CGPoint pos = [self convertTouchToNodeSpace: touch];
        
        int num = [_tracker trackTouch: touch];
        if(num == -1)
            continue;
        
        [_noteManager touchBegin: pos withTrackNum: num];
    }
}

- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_paused || !_gameStart)
        return;
    for(UITouch* touch in touches)
    {
        int num = [_tracker getTouchID: touch];
        if(num == -1)
            continue;
        
        [_noteManager touchMoved: [self convertTouchToNodeSpace: touch] withTrackNum: num];
    }
}

- (void) ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self ccTouchesEnded: touches withEvent: event];
}

- (void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch* touch in touches)
    {
        CGPoint pos = [self convertTouchToNodeSpace: touch];
        int num = [_tracker getTouchID: touch];
        if(num == -1)
        {
            [_itemManager touchEnded: pos];
            continue;
        }
        //NSLog(@" end time %f", _gameTime);
        [_noteManager touchEnded: pos withTrackNum: num time: _gameTime];
    }
}

- (void) eventFiredAt:(float)time forIndex:(int)index forTrack:(int)trackNum
{
    if(!_gameStart)
        return;
    //NSLog(@" fire time %f", _gameTime);
    [_noteManager createBubble: index atTime: _gameTime forTrack: trackNum];
}

- (void) musicEnded:(float)time
{
    _gameStart = NO;
    [_noteManager stop];
    [self.hud displayScore];
}

- (void) resume
{
    [self resumeSchedulerAndActions];
    [_itemManager resume];
    [_noteManager resume];
    [_musicManager resumeBGSound];
    _paused = NO;
}

- (BOOL) pause
{
    if(!_gameStart)
        return NO;
    
    [_itemManager pause];
    [_noteManager pause];
    [self pauseSchedulerAndActions];
    [_musicManager pauseBGSound];
    _paused = YES;
    return YES;
}

- (void) restart
{
    [self resumeSchedulerAndActions];
    _paused = NO;
    [self restartGame];
}

- (void) toMenu
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[MenuLayer sceneToMenu] withColor:ccWHITE]];
}

- (void) toStageSelection
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[MenuLayer sceneToStage: _stageInfo] withColor:ccWHITE]];
}

@end
