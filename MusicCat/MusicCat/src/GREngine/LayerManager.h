//
//  LayerManager.h
//  MusicCat
//
//  Created by Jason Chang on 11/5/12.
//
//

#import "cocos2d.h"

@interface LayerManager : NSObject
{
    CCNode* _parentNode;
}

- (id) initWithParentNode: (CCNode*) node;
- (void) initLayerManager;

@end
