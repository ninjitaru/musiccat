//
//  StageInfo.h
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "Song.h"

@interface StageInfo : NSObject

@property (nonatomic, retain) NSString* previewImage;
@property (nonatomic, retain) NSString* labelImage;
@property (nonatomic, retain) NSString* plistFile;
@property (nonatomic, retain) NSString* imageFile;

@property (retain, nonatomic) NSArray* spriteData;
@property (retain, nonatomic) NSArray* animationData;
@property (retain, nonatomic) NSArray* fixedAnimationPair;
@property (retain, nonatomic) NSArray* musicAnimationPair;
@property (retain, nonatomic) NSArray* boostFixedAnimationPair;
@property (retain, nonatomic) NSArray* boostMusicAnimationPair;

@property (retain, nonatomic) Song* song;

@property (nonatomic, copy) void (^updateBlock)(NSDictionary* sprites, NSDictionary* animations, float deltaTime);

// for difficulty setting
@property (assign) int difficulty;

@end
