//
//  IntroLayer.m
//  MusicCat
//
//  Created by Jason Chang on 9/12/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "IntroLayer.h"
#import "GameLayer.h"
#import "MenuLayer.h"
#import "UIImage+Scale.h"
#import "SettingDataFactory.h"

#import "GameImageFileManager.h"

#pragma mark - IntroLayer

// HelloWorldLayer implementation
@implementation IntroLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	IntroLayer *layer = [IntroLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// 
-(void) onEnter
{
	[super onEnter];

	// ask director for the window size
	CGSize size = [[CCDirector sharedDirector] winSize];

	CCSprite *background;
    
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
    {
		background = [CCSprite spriteWithFile:@"Default.png"];
		//background.rotation = 90;
	} else
    {
		background = [CCSprite spriteWithFile:@"Default-Landscape~ipad.png"];
	}
	background.position = ccp(size.width/2, size.height/2);

	// add the label as a child to this Layer
	[self addChild: background];
    
    _resourcesCreator = [[GameImageFileManager alloc] init];
    _resourcesCreator.delegate = self;
    //[[SettingDataFactory sharedFactory] loadData];
    
    [self scheduleOnce: @selector(createResources) delay: 0.2];
    //[self scheduleOnce: @selector(makeTransition) delay: 0.2];
    
}

- (void) createResources
{
    [self removeAllChildrenWithCleanup: YES];
    // show loading
    CCSprite* loading = [CCSprite spriteWithFile: @"Icon.png"];
    CGSize size = [[CCDirector sharedDirector] winSize];
    loading.tag = 1;
    loading.position = ccp(size.width/2, size.height/2);
    [loading runAction: [CCRepeatForever actionWithAction: [CCBlink actionWithDuration: 1.0f blinks: 1]]];
    [self addChild: loading z: 1];
	
    [self performSelectorInBackground: @selector(createImages) withObject: nil];
}

- (void) createImages
{
    if([NSThread currentThread].isCancelled)
    {
        [self performSelectorOnMainThread: @selector(loadingFailed) withObject: nil waitUntilDone: NO];
        return;
    }
    [_resourcesCreator setupImages: @[]];
    
    if([NSThread currentThread].isCancelled)
    {
        [self performSelectorOnMainThread: @selector(loadingFailed) withObject: nil waitUntilDone: NO];
        return;
    }
    
    [[SettingDataFactory sharedFactory] loadData];
    
    if([NSThread currentThread].isCancelled)
    {
        [self performSelectorOnMainThread: @selector(loadingFailed) withObject: nil waitUntilDone: NO];
        return;
    }
    
    [_resourcesCreator setupPlists: @[]];
    
    
    
}

-(void) makeTransition
{
    srand(arc4random());
    srandom(arc4random());
	//[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.5 scene: [GameLayer sceneWithStage: [SettingDataFactory sharedFactory].stages[1]] withColor:ccWHITE]];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.5 scene: [MenuLayer scene] withColor:ccWHITE]];
}

- (void) imagesReady: (NSString*) source
{
    NSLog(@"loading %@ done", source);
}

- (void) loadingProgressAt: (float) percentage
{

}

- (void) completeProcessImages
{
    
}

- (void) completeProcessPlist
{
    if([NSThread currentThread].isCancelled)
    {
        [self performSelectorOnMainThread: @selector(loadingFailed) withObject: nil waitUntilDone: NO];
        return;
    }
    [self performSelectorOnMainThread: @selector(makeTransition) withObject: nil waitUntilDone: NO];
}

- (void) loadingFailed
{
    
}

@end
