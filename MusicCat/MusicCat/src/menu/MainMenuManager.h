//
//  MainMenuManager.h
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "MenuManager.h"

@interface MainMenuManager : MenuManager
{
    CCSprite* _playButton;
    CCSprite* _profileButton;
    CCSprite* _shopButton;
}

- (BOOL) inPlayButton: (CGPoint) pos;
- (BOOL) inProfileButton: (CGPoint) pos;
- (BOOL) inShopButton: (CGPoint) pos;

- (void) reAddMenu;

- (void) cleanup;

@end
