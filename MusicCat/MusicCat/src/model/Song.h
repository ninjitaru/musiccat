//
//  Song.h
//  MusicCat
//
//  Created by Jason Chang on 12/11/12.
//
//

#import <Foundation/Foundation.h>

@interface Song : NSObject

@property (nonatomic, retain) NSString* soundFileName;
@property (nonatomic, retain) NSArray* trackTimings;

@property (nonatomic, readonly) int trackCounts;
@property (assign) float length;
@property (readonly, nonatomic) float timePerSection;
@property (assign) int beatPerSection;
@property (assign) float secondPerBeat;

@end
