//
//  MusicManager.h
//  MusicCat
//
//  Created by Jason Chang on 9/17/12.
//
//

#import "SimpleAudioEngine.h"

@protocol MusicManager2Delegate <NSObject>

@optional
- (void) groupStarted;

@end

@interface MusicManager2 : NSObject
{
    CDSoundSource* _activeBGSound;
    float _previousBGPosition;
    BOOL _incremented;
    NSTimer* _pollingTimer;
    BOOL _stopping;
}

@property (readonly) int groupPosition;
@property (readonly) float soundPosition;
@property (readonly) float soundPositionPercentage;
@property (nonatomic, assign) id<MusicManager2Delegate> delegate;

- (void) playBGSound: (NSString*) sound;
- (void) pauseBGSound;
- (void) resumeBGSound;
- (void) stopBGSound;
- (void) smoothStopBGSound;

@end
