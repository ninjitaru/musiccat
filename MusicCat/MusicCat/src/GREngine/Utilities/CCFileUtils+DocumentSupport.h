//
//  CCFileUtils+DocumentSupport.h
//  MusicCat
//
//  Created by Jason Chang on 12/18/12.
//
//

//#import <Foundation/Foundation.h>
//
//
//
//@interface CCFileUtils (custom)
//
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"
//
//
//-(NSString*) pathForResource:(NSString*)resource ofType:(NSString *)ext inDirectory:(NSString *)subpath;
//-(BOOL) fileExistsAtPath:(NSString*)relPath withSuffix:(NSString*)suffix;
//
//
//// do your override
//
//#pragma clang diagnostic pop
//@end
//
