//
//  ItemManager.h
//  MusicCat
//
//  Created by Jason Chang on 12/18/12.
//
//

#import "LayerManager.h"
#import "ItemTriggerDelegate.h"
#import "SpecialItem.h"
#import "MusicNoteStatusDelegate.h"
#import "DebugController.h"

@interface ItemManager : LayerManager <MusicNoteStatusDelegate, DebugDelegate>
{
    SpecialItem* _item;
    NSArray* _coins;
    
    BOOL _comboActivated;
    int _comboCount;
    float _correctPercentage;
    
    SpecialItemType _effectType;
    BOOL _effectActive;
    float _effectTimeAccum;
    float _effectTimeLimit;
    
    float _itemRespawnDelay;  // setting
    float _itemTimeAccum;
    float _itemChance;    // setting

    int _itemSpawnLimit; // setting base on difficulty
    int _itemSpawnedCount;
    
    float _coinRespawnDelay;  // setting
    float _coinTimeAccum;
    float _coinChanceMod;  // setting
    float _coinChance;   // setting
    float _coinChanceModLimit; // setting
    int _coinSpawnLimit;    // setting base on difficulty
    int _coinSpawnedCount; 
    NSMutableArray* _delegations;
}

@property (nonatomic, assign) int difficulty;

- (void) update: (ccTime) dt;
- (void) restart;
- (void) pause;
- (void) resume;

- (BOOL) touchBegin: (CGPoint) pos;
- (void) touchEnded: (CGPoint) pos;

- (void) registerDelegatation: (id<ItemTriggerDelegate>) delegate;
- (void) removeDelegation: (id<ItemTriggerDelegate>) delegate;

@end
