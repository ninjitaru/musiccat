//
//  MainMenuManager.m
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "MainMenuManager.h"
#import "MenuDepthEnum.h"
#import "SpriteCreator.h"
#import "DeviceInfoHelper.h"

@implementation MainMenuManager

- (void) prepareLayer
{
    CGSize size = [CCDirector sharedDirector].winSize;
    NSArray* setup = @[
    
    @{@"type":@(typeSprite),
    @"texture":@"item_bar.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(150,size.height+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(320,size.height+378)],
    @"depth":@(kDepthForeground),
    @"tag":@10
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_bar.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(270,size.height+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(600,size.height+378)],
    @"depth":@(kDepthForeground),
    @"tag":@11
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_play.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(150,size.height-20+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(320,size.height-20+378)],
    @"depth":@(kDepthBackGameButton),
    @"tag":@12
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_profile.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(270,size.height-20+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(600,size.height-20+378)],
    @"depth":@(kDepthBackGameButton),
    @"tag":@13
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_shop.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(270,size.height-97+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(600,size.height-200+378)],
    @"depth":@(kDepthBackGameButton),
    @"tag":@14
    },

    ];

    _isRunningEndLayer = NO;
    [[SpriteCreator sharedCreator] addSprites: setup toNode: _node inObject: self];
    [self startButtonAnimation];
}

- (void) cleanup
{
    for(int i = 4; i < 15; i++)
    {
        [_node removeChildByTag: i cleanup: YES];
    }
}

- (void) endLayer:(void (^)(void))block
{
    if(_isRunningEndLayer)
        return;
    _isRunningEndLayer = YES;
    
    CGSize size = [CCDirector sharedDirector].winSize;
    CCSprite* lcurtain = (CCSprite*)[_node getChildByTag: 1];
    CCSprite* rcurtain = (CCSprite*)[_node getChildByTag: 2];
    CCSprite* cats = (CCSprite*)[_node getChildByTag: 4];
    [cats runAction: [CCMoveTo actionWithDuration: 0.3f position: ccp(110,-cats.contentSize.height)]];
    
    float widthDiff = 30;
    if([DeviceInfoHelper getScreenSizeType] == kscreenSizeIpad)
    {
        widthDiff = 60;
    }
    
    [lcurtain runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2+widthDiff, size.height)]];
    [rcurtain runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2-(widthDiff*2), size.height)]];
    // move cat
    // move shadow
    [_node runAction: [CCSequence actions:
                       [CCDelayTime actionWithDuration: 0.6f],
                       [CCCallBlock actionWithBlock: ^ { _isRunningEndLayer = NO; [self cleanup]; block(); }],
                       nil]];
}

- (void) reAddMenu
{
    CGSize size = [CCDirector sharedDirector].winSize;
    NSArray* setup = @[
    
    @{@"type":@(typeSprite),
    @"texture":@"item_bar.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(150,size.height+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(320,size.height+378)],
    @"depth":@(kDepthForeground),
    @"tag":@10
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_bar.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(270,size.height+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(600,size.height+378)],
    @"depth":@(kDepthForeground),
    @"tag":@11
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_play.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(150,size.height-20+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(320,size.height-20+378)],
    @"depth":@(kDepthBackGameButton),
    @"tag":@12
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_profile.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(270,size.height-20+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(600,size.height-20+378)],
    @"depth":@(kDepthBackGameButton),
    @"tag":@13
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_shop.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(270,size.height-97+178)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(600,size.height-200+378)],
    @"depth":@(kDepthBackGameButton),
    @"tag":@14
    },
    
    ];
    
    _isRunningEndLayer = NO;
    [[SpriteCreator sharedCreator] addSprites: setup toNode: _node inObject: self];
    [self startButtonAnimation];
}

- (void) startButtonAnimation
{
    CGSize size = [CCDirector sharedDirector].winSize;
    
    CCSprite* leftbar = (CCSprite*)[_node getChildByTag: 10];
    CCSprite* rightbar = (CCSprite*)[_node getChildByTag: 11];
    _playButton = (CCSprite*)[_node getChildByTag: 12];
    _profileButton = (CCSprite*)[_node getChildByTag: 13];
    _shopButton = (CCSprite*)[_node getChildByTag: 14];
    
    if([DeviceInfoHelper getScreenSizeType] == kscreenSizeIpad)
    {
        [leftbar runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.0f position:ccp(320, size.height)]]
         ];
        [rightbar runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.5f position:ccp(600, size.height)]]
         ];
        [_playButton runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.0f position:ccp(320, size.height-20)]]
         ];
        [_profileButton runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.5f position:ccp(600, size.height-20)]]
         ];
        [_shopButton runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.5f position:ccp(600, size.height-200)]]
         ];
    }
    else
    {
        [leftbar runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.0f position:ccp(150, size.height)]]
         ];
        [rightbar runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.5f position:ccp(270, size.height)]]
         ];
        [_playButton runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.0f position:ccp(150, size.height-20)]]
         ];
        [_profileButton runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.5f position:ccp(270, size.height-20)]]
         ];
        [_shopButton runAction: [CCEaseBounceOut actionWithAction: [CCMoveTo actionWithDuration: 1.5f position:ccp(270, size.height-97)]]
         ];
    }
}

- (BOOL) inPlayButton: (CGPoint) pos
{
    return CGRectContainsPoint(_playButton.boundingBox, pos);
}

- (BOOL) inProfileButton: (CGPoint) pos
{
    return CGRectContainsPoint(_profileButton.boundingBox, pos);
}

- (BOOL) inShopButton: (CGPoint) pos
{
    return CGRectContainsPoint(_shopButton.boundingBox, pos);
}

@end
