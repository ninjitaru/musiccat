//
//  BandCat.h
//  MusicCat
//
//  Created by Jason Chang on 12/24/12.
//
//

#import <Foundation/Foundation.h>

typedef enum
{
    kCatGreen,
    kCatPink,
    kCatOrange,
} CatType;

@class MusicBall;

@interface BandCat : NSObject
{
    CCSprite* _sprite;
    BOOL _full;
    CCSprite* _colorMask;
    float _fillPercentage;
    int _bellyCount;
    int _fullCount;
}

@property (assign) BandCat* next;
@property (assign, nonatomic) BOOL active;
@property (assign) float speed;
@property (assign, nonatomic) CatType catType;
@property (readonly) CCSprite* node;
@property (readonly) BOOL full;

- (id) initWithParentNode: (CCNode*) parent;

- (void) setRandomCat;

- (void) update: (ccTime) dt;
- (BOOL) addNote: (MusicBall*) ball;
- (void) colorTo: (float) percentage;

@end
