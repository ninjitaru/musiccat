//
//  MusicBall.m
//  MusicCat
//
//  Created by Jason Chang on 11/9/12.
//
//

#import "MusicBall.h"
#import "TouchTracker.h"
#import "CCBlade.h"
#import "GridManager.h"

@implementation MusicBall

- (void) dealloc
{
    [_spiritAction release];
    [_leftWingAction release];
    [_rightWingAction release];
    [_magnetRingAction release];
    [super dealloc];
}

- (id) initWithParent:(CCNode *)parent
{
    self = [super init];
    if(self)
    {
        CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
        _sprite = [CCSprite spriteWithSpriteFrameName: @"note_sound2.png"];
        _type = kMusicBallHeart;
        self.touchTrackNumber = -1;
        self.gridIndex = -1;
        [parent addChild: _sprite];
        
        _spirit = [CCSprite spriteWithSpriteFrameName: @"effect_fairy_1.png"];
        [_sprite addChild: _spirit];
        _spirit.position = ccp(10,10);
        _spirit.visible = NO;
        _spiritAction = [[CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames: @[[cache spriteFrameByName: @"effect_fairy_1.png"],[cache spriteFrameByName: @"effect_fairy_2.png"]] delay: 0.3f]]] retain];
        
        _leftWing = [CCSprite spriteWithSpriteFrameName: @"effect_wing_1.png"];
        [_sprite addChild: _leftWing];
        _leftWing.position = ccp(-5,50);
        _leftWing.visible = NO;
        _leftWing.flipX = YES;
        
        _rightWing = [CCSprite spriteWithSpriteFrameName: @"effect_wing_1.png"];
        [_sprite addChild: _rightWing];
        _rightWing.position = ccp(60,50);
        //_rightWing.flipX = YES;
        _rightWing.visible = NO;
        
        _leftWingAction = [[CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames: @[[cache spriteFrameByName: @"effect_wing_1.png"],[cache spriteFrameByName: @"effect_wing_2.png"]] delay: 0.2f]]] retain];
        
        _rightWingAction = [[CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames: @[[cache spriteFrameByName: @"effect_wing_1.png"],[cache spriteFrameByName: @"effect_wing_2.png"]] delay: 0.2f]]] retain];
        
        _magnetRing = [CCSprite spriteWithSpriteFrameName: @"effect_radio_1.png"];
        [_sprite addChild: _magnetRing];
        _magnetRing.position = ccp(_sprite.contentSize.width/2,_sprite.contentSize.height/2);
        _magnetRing.visible = NO;
        _magnetRingAction = [[CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames: @[[cache spriteFrameByName: @"effect_radio_1.png"],[cache spriteFrameByName: @"effect_radio_2.png"]] delay: 0.2f]]] retain];
    }
    return self;
}

#pragma mark - properties

- (void) setShowSpirit:(BOOL)showSpirit
{
    if(showSpirit)
    {
        _spirit.visible = YES;
        [_spirit stopAllActions];
        [_spirit runAction: _spiritAction];
    }
    else
    {
        _spirit.visible = NO;
        [_spirit stopAllActions];
    }
}

- (void) setShowWing:(BOOL)showWing
{
    if(showWing)
    {
        _leftWing.visible = YES;
        _rightWing.visible = YES;
        [_leftWing stopAllActions];
        [_rightWing stopAllActions];
        [_leftWing runAction: _leftWingAction];
        [_rightWing runAction: _rightWingAction];
    }
    else
    {
        _leftWing.visible = NO;
        _rightWing.visible = NO;
        [_leftWing stopAllActions];
        [_rightWing stopAllActions];
    }
}

- (void) setShowMagnet:(BOOL)showMagnet
{
    if(showMagnet)
    {
        _magnetRing.visible = YES;
        [_magnetRing stopAllActions];
        [_magnetRing runAction: _magnetRingAction];
    }
    else
    {
        _magnetRing.visible = NO;
        [_magnetRing stopAllActions];
    }
}

- (void) setType:(kMusicBallType)type
{
    if(_type == type)
        return;
    _type = type;
    NSString* name = @"note_sound2.png";
    switch (_type)
    {
        case kMusicBallHeart:
            name = @"note_sound2.png";
            break;
        case kMusicBallShape:
            name = @"note_sound3.png";
            break;
        case kMusicBallStar:
            name = @"note_sound1.png";
            break;
        default:
            break;
    }
    [_sprite setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: name]];
}

- (CCNode*) node
{
    return _sprite;
}

- (void) setTimeToDeath:(float)timeToDeath
{
    _timeToDeath = timeToDeath;
    _internalTime = _timeToDeath;
}

#pragma mark - methods

- (void) typeByTrack: (int) trackNum
{
    if(trackNum == 0)
    {
        self.type = kMusicBallStar;
    }
    else if(trackNum == 1)
    {
        self.type = kMusicBallHeart;
    }
    else
    {
        self.type = kMusicBallShape;
    }
}

- (void) randomType
{
    int type = lroundf(CCRANDOM_0_1() * (kMusicBallShape+1))%(kMusicBallShape+1);
    self.type = type;
}

- (float) internalTime
{
    return _internalTime;
}

- (void) setInternalTime:(float)internalTime
{
    _internalTime = internalTime;
}

- (void) update: (ccTime) dt
{
    if(!self.node.visible || self.isDead || self.onSheet)
        return;
    _internalTime -= dt;
    if(!self.isDead && _internalTime <= 0)
    {
        self.isDead = YES;
        return;
    }
    
    if(self.shouldFloat)
    {
        if(self.node.numberOfRunningActions == 0 && self.touchTrackNumber == -1)
        {
            [self.node runAction: [CCMoveBy actionWithDuration: 1 position: ccp(CCRANDOM_0_1()*4-2, CCRANDOM_0_1()*10-5)]];
        }
    }
}

- (void) setIsDead:(BOOL)isDead
{
    _isDead = isDead;
    if(_isDead)
    {
        float y = [CCDirector sharedDirector].winSize.height;
        [self.node stopAllActions];
        [self.node runAction: [CCEaseBackIn actionWithAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(self.node.position.x, y+100)]]];
        self.shouldFloat = NO;
        self.onSheet = NO;
        self.showSpirit = NO;
        self.showWing = NO;
        self.showMagnet = NO;
        self.autoPilot = NO;
        
        [self releaseInfo];
        //self.node.visible = NO;
    }
}

- (BOOL) shouldDelete
{
    return self.node.numberOfRunningActions == 0;
}

- (void) releaseInfo
{
    if(self.blade)
    {
        self.blade.visible = NO;
        self.blade = nil;
    }
    
    if(self.touchTrackNumber != -1)
    {
        [self.touchTracker releaseTouchByID: self.touchTrackNumber];
        self.touchTrackNumber = -1;
    }
    
    if(self.gridIndex != -1)
    {
        [self.gridManager setIndex: self.gridIndex withValue: NO];
        self.gridIndex = -1;
    }
}

- (void) reset
{
    [self releaseInfo];
    
    self.autoPilot = NO;
    self.node.color = ccWHITE;
    self.showSpirit = NO;
    self.showWing = NO;
    self.showMagnet = NO;
    self.shouldFloat = NO;
    self.node.scale = 1.0f;
    self.node.visible = NO;
    self.onSheet = NO;
    self.isDead = NO;
    self.noteIndex = 0;
    self.trackIndex = 0;
}

@end
