//
//  GameTypes.h
//  MusicCat
//
//  Created by Jason Chang on 12/19/12.
//
//

#ifndef MusicCat_GameTypes_h
#define MusicCat_GameTypes_h

typedef enum
{
    kItemAutoWing,
    kItemMagnet,
    kItemSuper,
    kItemSpirit,
    kItemDoubleCoin,
} SpecialItemType;

#endif
