//
//  MenuLayer.m
//  MusicCat
//
//  Created by Jason Chang on 10/29/12.
//
//

#import "MenuLayer.h"
#import "MenuDepthEnum.h"
#import "GameLayer.h"
#import "SettingDataFactory.h"

#import "SimpleAudioEngine.h"

@implementation MenuLayer

- (void) dealloc
{
    [_introManager release];
    [_stageManager release];
    [_mainManager release];
    [super dealloc];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}

+ (CCScene*) sceneToMenu
{
    CCScene *scene = [CCScene node];
	MenuLayer *layer = [self node];
    [layer readyMenu];
    
	[scene addChild: layer];
	return scene;
}

+ (CCScene*) sceneToStage: (StageInfo*) stage
{
    CCScene *scene = [CCScene node];
	MenuLayer *layer = [self node];
    [layer readyStage: stage];
    
	[scene addChild: layer];
	return scene;
}

- (void) readyMenu
{
    if(!_mainManager)
    {
        _mainManager = [[MainMenuManager alloc] initWithLayer: self];
    }
    [_mainManager reAddMenu];
    CCSprite* title = (CCSprite*)[self getChildByTag: 3];
    [self removeChild: title cleanup: YES];
    _state = 1;
}

- (void) readyStage: (StageInfo*) stage
{
    if(!_stageManager)
    {
        _stageManager = [[SingleStageMenuManager alloc] initWithLayer: self];
        [self loadStageData];
    }
    
    [_introManager stopAnimations];
    
    if(!_mainManager)
    {
        _mainManager = [[MainMenuManager alloc] initWithLayer: self];
    }
    [_mainManager cleanup];
    
    CCSprite* title = (CCSprite*)[self getChildByTag: 3];
    [self removeChild: title cleanup: YES];
    
    for(int i = 0; i < _stages.count; i++)
    {
        if([((StageInfo*)_stages[i]).previewImage isEqualToString: stage.previewImage])
        {
            _stageIndex = i;
            break;
        }
    }
    
    _stageManager.currentStage = _stages[_stageIndex];
    [_stageManager prepareLayer];
    _state = 2;
}

- (void) initLayer
{
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    [cache addSpriteFramesWithFile: @"menu_collection.plist"];
    
    [self removeAllChildrenWithCleanup: YES];
    
    [self showIntro];
    [self scheduleUpdate];
    self.isTouchEnabled = YES;
}

- (void) onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
}

- (void) onExitTransitionDidStart
{
    [super onExitTransitionDidStart];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

- (void) loadStageData
{
    _stages = [SettingDataFactory sharedFactory].stages;
    _stageIndex = 0;
}

- (void) update: (ccTime) dt
{
}

// image, anchor, position, depth, id

- (void) showIntro
{
    if(!_introManager)
    {
        _introManager = [[IntroMenuManager alloc] initWithLayer: self];
    }
    
    [_introManager prepareLayer];
    _state = 0;
}

- (void) showMenu
{
    if(!_mainManager)
    {
        _mainManager = [[MainMenuManager alloc] initWithLayer: self];
    }
    
    [_introManager endLayer: ^{
        [_mainManager prepareLayer];
        _state = 1;
    }];
}

- (void) showStage
{
    if(!_stageManager)
    {
        _stageManager = [[SingleStageMenuManager alloc] initWithLayer: self];
        [self loadStageData];
    }
    
    [_introManager stopAnimations];
    [_introManager closeSpotLight];
    [_mainManager endLayer: ^{
        _stageManager.currentStage = _stages[_stageIndex];
        [_stageManager prepareLayer];
        _state = 2;
    }];
}

- (void) showProfile
{
    
}

- (void) showShop
{
    
}

- (void) showDifficulty
{
    [_stageManager showDifficulty];
    _state = 3;
}


- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //UITouch* touch = [touches anyObject];
    //CGPoint pos = [self convertTouchToNodeSpace: touch];
    
    if(_state == 2)
    {
        
    }
}

- (void) backToMenu
{
    [_stageManager backToMenu:
    ^{
        [_introManager reAddMenu];
        [_mainManager reAddMenu];

        _state = 1;
    }];
}

- (void) backToStage
{
    [_stageManager backToStage];
    _state = 2;
}

- (void) startGame
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameLayer sceneWithStage: _stageManager.currentStage] withColor:ccWHITE]];
}

- (void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint pos = [self convertTouchToNodeSpace: touch];
    
    if(_state == 0)
    {
        [self showMenu];
    }
    else if(_state == 1)
    {
        if([_mainManager inPlayButton: pos])
        {
            [self showStage];
        }
        else if([_mainManager inProfileButton: pos])
        {
            [self showProfile];
        }
        else if([_mainManager inShopButton: pos])
        {
            [self showShop];
        }
    }
    else if(_state == 2)
    {
        if([_stageManager isRollingControl: pos])
        {
            _stageIndex = (_stageIndex+1)%5;
            [_stageManager changeStage: _stages[_stageIndex]];
        }
        else if([_stageManager isBackButton: pos])
        {
            [self backToMenu];
        }
        else
        {
            [self showDifficulty];
        }
    }
    else
    {
        if([_stageManager isStartButton: pos])
        {
            [self startGame];
        }
        else if([_stageManager isBackButton: pos])
        {
            [self backToStage];
        }
        else
        {
            [_stageManager alterDifficulty: pos];
        }
    }
}

@end
