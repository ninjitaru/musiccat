//
//  DeviceInfoHelper.h
//  MusicCat
//
//  Created by Jason Chang on 12/13/12.
//
//

#import <Foundation/Foundation.h>

typedef enum
{
    kiphone3gs,
    kiphone4,
    kiphone5,
    kipad,
    kipad3,
} enumiOSDeviceType;

typedef enum
{
    kscreenSizeIphone,
    kscreenSizeIphone5,
    kscreenSizeIpad,
} enumScreenSizeType;

@interface DeviceInfoHelper : NSObject

+ (enumiOSDeviceType) getDeviceType;
+ (enumScreenSizeType) getScreenSizeType;

@end
