//
//  DeviceInfoHelper.m
//  MusicCat
//
//  Created by Jason Chang on 12/13/12.
//
//

#import "DeviceInfoHelper.h"

#define WIDTH_IPHONE5 568

@implementation DeviceInfoHelper

+ (enumiOSDeviceType) getDeviceType
{
    enumiOSDeviceType type = kiphone3gs;
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        type = kipad;
        if( CC_CONTENT_SCALE_FACTOR() == 2 )
            type = kipad3;
    }
    else
    {
        if( CC_CONTENT_SCALE_FACTOR() == 2 )
        {
            type = kiphone4;
            CGSize size = [UIScreen mainScreen].bounds.size;
            if(size.width == WIDTH_IPHONE5 || size.height == WIDTH_IPHONE5)
            {
                type = kiphone5;
            }
        }
    }
    return type;
}

+ (enumScreenSizeType) getScreenSizeType
{
    enumScreenSizeType type = kscreenSizeIpad;
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        type = kscreenSizeIphone;
        CGSize size = [UIScreen mainScreen].bounds.size;
        if(size.width == WIDTH_IPHONE5 || size.height == WIDTH_IPHONE5)
        {
            type = kscreenSizeIphone5;
        }
    }
    return type;
}

@end
