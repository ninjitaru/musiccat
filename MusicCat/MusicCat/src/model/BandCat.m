//
//  BandCat.m
//  MusicCat
//
//  Created by Jason Chang on 12/24/12.
//
//

#import "BandCat.h"
#import "MusicBall.h"
#import "GameDepthEnum.h"
#import "GameConstants.h"

@implementation BandCat

- (void) dealloc
{
    [_sprite.parent removeChild: _sprite cleanup: YES];
    [super dealloc];
}

- (id) initWithParentNode: (CCNode*) parent
{
    self = [super init];
    if(self)
    {
        _sprite = [CCSprite spriteWithFile: @"pink.png"];
        _colorMask = [CCSprite spriteWithFile: @"pink.png"];
        //_colorMask.opacity = 125;
        _sprite.anchorPoint = CGPointZero;
        [parent addChild: _sprite z: kDepthBand];
        _colorMask.anchorPoint = CGPointZero;
        _colorMask.position = CGPointZero;
        [_sprite addChild: _colorMask];
        [self colorTo: _fillPercentage];
        _fullCount = [GameConstants constants].fullCatBellyCount;
    }
    return self;
}

- (void) reset
{
    _full = 0;
    _fillPercentage = 0;
    _bellyCount = 0;
}

- (void) setActive:(BOOL)active
{
    _active = active;
    if(_active)
    {
        [self reset];
        _sprite.visible = YES;
    }
    else
    {
        self.next = nil;
        _sprite.visible = NO;
    }
}

- (void) setCatType:(CatType)catType
{
    _catType = catType;
    NSString* name = @"pink.png";
    switch(_catType)
    {
        case kCatGreen:
            name = @"green.png";
            break;
        case kCatOrange:
            name = @"orange.png";
            break;
        case kCatPink:
            break;
        default:
            break;
    }
    CCSprite* temp = [CCSprite spriteWithFile: name];
    [_sprite setTexture: temp.texture];
    [_sprite setTextureRect: temp.textureRect];
    [_colorMask setTexture: temp.texture];
    [_colorMask setTextureRect: temp.textureRect];
    [self colorTo: _fillPercentage];
}

- (CCSprite*) node
{
    return _sprite;
}

- (BOOL) full
{
    return _full;
}

- (void) setRandomCat
{
    int value = ((int)(CCRANDOM_0_1()*100)) % 3;
    if(value == 0)
        self.catType = kCatGreen;
    else if(value == 1)
        self.catType = kCatPink;
    else
        self.catType = kCatOrange;
}

- (void) update: (ccTime) dt
{
    _sprite.position = ccpAdd(_sprite.position, ccp(-self.speed*dt,0));
}

- (BOOL) addNote: (MusicBall*) ball
{
    BOOL correct = ball.trackIndex == self.catType;
    if(correct)
    {
        _bellyCount++;
        if(_bellyCount >= _fullCount)
        {
            _full = YES;
            _bellyCount = _fullCount;
        }
        [_sprite stopAllActions];
        [_sprite runAction: [CCSequence actions: [CCScaleTo actionWithDuration: 0.1f scale: 1.1f],[CCScaleTo actionWithDuration: 0.1f scale: 1.0f], nil]];
    }
    else
    {
        [_sprite stopAllActions];
        [_sprite runAction: [CCSequence actions: [CCRotateTo actionWithDuration: 0.1f angle: 5],[CCRotateTo actionWithDuration: 0.1f angle: -5],[CCRotateTo actionWithDuration: 0.1f angle: 0], nil]];
        _bellyCount--;
        _full = NO;
        if(_bellyCount < 0)
            _bellyCount = 0;
    }
    
    _fillPercentage = _bellyCount/_fullCount;
    [self colorTo: _fillPercentage];
    
    return correct;
}

- (void) colorTo: (float) percentage
{
    CGRect rect = _sprite.textureRect;
    rect.size.height = percentage * rect.size.height;
    _colorMask.textureRect = rect;
}


@end
