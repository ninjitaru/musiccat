//
//  ItemTriggerDelegate.h
//  MusicCat
//
//  Created by Jason Chang on 12/18/12.
//
//

#ifndef MusicCat_ItemTriggerDelegate_h
#define MusicCat_ItemTriggerDelegate_h

#import "GameTypes.h"

@protocol ItemTriggerDelegate <NSObject>

- (void) activateItem: (SpecialItemType) itemType;
- (void) deactivateItem: (SpecialItemType) itemType;
- (void) obtainCoin;

@end


#endif
