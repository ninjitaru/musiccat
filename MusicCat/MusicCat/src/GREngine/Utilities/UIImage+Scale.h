//
//  UIImage+Scale.h
//  MusicCat
//
//  Created by Jason Chang on 11/26/12.
//
//

@interface UIImage (custom)

+ (UIImage*) imageFromFile: (NSString*) name;
-(UIImage*)scaleToSize:(CGSize)size;
-(UIImage*)scaleToSize:(CGSize)size forcePOT: (BOOL) enablePOT;
- (void) savetoDiskWithName: (NSString*) name;
- (UIImage*) cropToRect: (CGRect) rect;

@end