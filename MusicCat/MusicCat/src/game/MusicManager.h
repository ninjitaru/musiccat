//
//  MusicManager.h
//  MusicCat
//
//  Created by Jason Chang on 11/12/12.
//
//

#import <Foundation/Foundation.h>
#import "SimpleAudioEngine.h"

@class Song;

@protocol MusicManagerDelegate <NSObject>

- (void) eventFiredAt: (float) time forIndex: (int) index forTrack: (int) trackNum;
- (void) musicEnded: (float) time;

@end

@interface MusicManager : NSObject
{
    CDSoundSource* _activeBGSound;
    NSMutableArray* _firingTime;
    NSMutableArray* _firingIndex;
    NSTimer* _pollingTimer;
    NSMutableArray* _events;
    
    NSMutableArray* _noteSoundTiming;
    NSMutableArray* _noteSound;
    
    float _beatInterval;
    float _halfBeatInterval;
}

@property (nonatomic, retain) Song* song;
@property (readonly) float soundPosition;
@property (readonly) float soundPositionPercentage;
@property (readonly) float soundDuration;

@property (readonly) NSMutableArray* noteSoundTiming;
@property (readonly) NSMutableArray* noteSound;

- (id) initWithSong: (Song*) song;

- (void) playBGSound;
- (void) preloadBGSound;
- (void) pauseBGSound;
- (void) resumeBGSound;
- (void) stopBGSound;

//- (void) addFireTiming: (NSArray*) array;
//- (void) clearFireEvents;

- (void) addDelegate: (id<MusicManagerDelegate>) delegate;
- (void) removeDelegate: (id<MusicManagerDelegate>) delegate;
- (void) removeAllDelegate;

@end
