//
//  TrackPatternManager.m
//  MusicCat
//
//  Created by Jason Chang on 11/12/12.
//
//

#import "TrackPatternManager.h"

@implementation TrackPatternManager

- (void) dealloc
{
    [_methods release];
    [super dealloc];
}

- (id) init
{
    self = [super init];
    if(self)
    {
        _methods = [@{
        @(kTrackTypeArchLeft):@"archLeftAt:Outof:",
        @(kTrackTypeArchRight):@"archRightAt:Outof:",
        @(kTrackTypeVerticalLine):@"verticalLineAt:Outof:",
        @(kTrackTypeHorizontalLine):@"horizontalAt:Outof:",
        @(kTrackTypeZipVertical):@"zipVerticalAt:Outof:",
        @(kTrackTypeZipHorizontal):@"zipHorizontalAt:Outof:",
        @(kTrackTypeHalfCircle):@"halfCircleAt:Outof:",
        @(kTrackTypeReverseHalfCircle):@"halfReverseCircleAt:Outof:",
        } retain];
    }
    return self;
}

- (CGPoint) generatePath: (TrackPatternType) type withIndex: (int) index outOf: (int) number range: (CGPoint) range cellSize: (float) cellsize
{
    _gridSize = CGSizeMake(range.x, range.y);
    _gridCellSize = cellsize;
    SEL selector = NSSelectorFromString(_methods[@(type)]);
    return [[self performSelector: selector withObject: @(index) withObject: @(number)] CGPointValue];
}

- (id) archLeftAt: (NSNumber*) index Outof: (NSNumber*) number
{
    CGPoint pos1 = ccp(0,0);
    CGPoint pos2 = ccp(_gridCellSize,_gridSize.height-_gridCellSize);
    CGPoint pos3 = ccp(_gridCellSize*[number intValue],_gridSize.height-_gridCellSize*2);
    float t = 1.0f/([number intValue] - 1) * [index intValue];
    CGPoint pos = ccpAdd(ccpAdd(ccpMult(pos1, powf((1-t), 2)),ccpMult(pos2, 2*(1.0f-t)*t)), ccpMult(pos3,t*t));
    return [NSValue valueWithCGPoint: pos];
}

- (id) archRightAt: (NSNumber*) index Outof: (NSNumber*) number
{
    CGPoint pos1 = ccp(0,0);
    CGPoint pos2 = ccp(-_gridCellSize,_gridSize.height-_gridCellSize);
    CGPoint pos3 = ccp(-_gridCellSize*[number intValue],_gridSize.height-_gridCellSize*2);
    float t = 1.0f/([number intValue] - 1) * [index intValue];
    CGPoint pos = ccpAdd(ccpAdd(ccpMult(pos1, powf((1-t), 2)),ccpMult(pos2, 2*(1.0f-t)*t)), ccpMult(pos3,t*t));
    return [NSValue valueWithCGPoint: pos];
}

- (id) verticalLineAt: (NSNumber*) index Outof: (NSNumber*) number
{
    CGPoint pos1 = ccp(0,0);
    CGPoint pos2 = ccp(0,_gridSize.height);
    float t = 1.0f/([number intValue] - 1) * [index intValue];
    CGPoint pos = ccpAdd(pos1, ccpMult(ccpSub(pos2,pos1), t));
    return [NSValue valueWithCGPoint: pos];
}

- (id) horizontalAt: (NSNumber*) index Outof: (NSNumber*) number
{
    CGPoint pos1 = ccp(0,0);
    CGPoint pos2 = ccp(_gridSize.width,0);
    float t = 1.0f/([number intValue] - 1) * [index intValue];
    CGPoint pos = ccpAdd(pos1, ccpMult(ccpSub(pos2,pos1), t));
    return [NSValue valueWithCGPoint: pos];
}

- (id) zipVerticalAt: (NSNumber*) index Outof: (NSNumber*) number
{
    float y = _gridSize.height * ([index intValue]/[number floatValue]);
    float x = [index intValue]%2*(-2*_gridCellSize)+_gridCellSize;
    return [NSValue valueWithCGPoint: ccp(x, y)];
}

- (id) zipHorizontalAt: (NSNumber*) index Outof: (NSNumber*) number
{
    float y = [index intValue]%2*(-2*_gridCellSize) + _gridCellSize;
    float x = _gridSize.width * ([index intValue]/[number floatValue]);
    return [NSValue valueWithCGPoint: ccp(x, y)];
}

- (id) halfCircleAt: (NSNumber*) index Outof: (NSNumber*) number
{
    float halfCell = _gridCellSize * 0.5f;
    CGPoint pos0 = ccp(0,0);
    CGPoint pos1 = ccp(halfCell,_gridSize.height-_gridCellSize);
    CGPoint pos2 = ccp(_gridCellSize*[number intValue]-halfCell,_gridSize.height-_gridCellSize);
    CGPoint pos3 = ccp(_gridCellSize*[number intValue],0);
    float t = 1.0f/([number intValue] - 1) * [index intValue];
    float nt = 1-t;
    CGPoint pos = ccpAdd(ccpAdd(ccpAdd(ccpMult(pos0, nt*nt*nt),ccpMult(pos1, 3*nt*nt*t)), ccpMult(pos2,3*nt*t*t)), ccpMult(pos3, t*t*t));
    return [NSValue valueWithCGPoint: pos];
}

- (id) halfReverseCircleAt: (NSNumber*) index Outof: (NSNumber*) number
{
    float halfCell = _gridCellSize * 0.5f;
    CGPoint pos0 = ccp(0,0);
    CGPoint pos1 = ccp(-halfCell,_gridSize.height-_gridCellSize);
    CGPoint pos2 = ccp(-_gridCellSize*[number intValue]-halfCell,_gridSize.height-_gridCellSize);
    CGPoint pos3 = ccp(-_gridCellSize*[number intValue],0);
    float t = 1.0f/([number intValue] - 1) * [index intValue];
    float nt = 1-t;
    CGPoint pos = ccpAdd(ccpAdd(ccpAdd(ccpMult(pos0, nt*nt*nt),ccpMult(pos1, 3*nt*nt*t)), ccpMult(pos2,3*nt*t*t)), ccpMult(pos3, t*t*t));
    return [NSValue valueWithCGPoint: pos];    
}

@end
