//
//  GameDepthEnum.h
//  MusicCat
//
//  Created by Jason Chang on 11/5/12.
//
//

#ifndef MusicCat_GameDepthEnum_h
#define MusicCat_GameDepthEnum_h

typedef enum
{
    kDepthBackground,
    kDepthCat,
    kDepthBand,
    kDepthStreak,
    kDepthNote,
    kDepthItem,
    kDepthFront,
    kDepthUI,
    
} GameDepthEnum;

#endif
