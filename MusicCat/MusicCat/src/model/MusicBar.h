//
//  MusicBar.h
//  MusicCat
//
//  Created by Jason Chang on 11/9/12.
//
//

#import <Foundation/Foundation.h>
#import "MusicBall.h"
#import "Song.h"

@interface MusicBar : NSObject
{
    BOOL _colorMode;
    
    CCSpriteBatchNode* _batchParent;
    CCSprite** _sheets;
    CCSprite** _effects;
    
    float* _effectSpeed;
    float _speed;
    int _currentSection;
    int _sheetLocation;
    
    NSArray* _notePreviews;
    NSMutableArray* _nextNoteIndexArray;
    int _latestNoteValueSeem;
    int _skippedNoteCount;
}

@property (readonly) int skippedNoteCount;

@property (readonly) float height;
@property (assign) int deadSheetTag;
@property (assign) BOOL needCleanSheet;
@property (nonatomic, assign) BOOL showSuperMode;
@property (nonatomic, assign) Song* song;

- (id) initWithParent: (CCNode*) parent;

@property (readonly) CCNode* node;
@property (assign, nonatomic) float timePerSection;
@property (readonly) int latestNoteValueSeem;

- (void) createNotePreview;
- (BOOL) addNote: (MusicBall*) ball ignoreRule: (BOOL) ignore;
- (void) missNote: (MusicBall*) ball;
- (void) skipNote: (MusicBall*) ball;

- (void) update: (ccTime) dt;

- (void) reset;

@end
