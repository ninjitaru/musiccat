//
//  GRTouchableCCSprite.m
//  MusicCat
//
//  Created by Jason Chang on 11/19/12.
//
//

#import "GRTouchableCCSprite.h"

@implementation GRTouchableCCSprite

- (void) setSelected:(BOOL)selected
{
    if(_disabled)
        return;
    
    _selected = selected;
    if(_selected && self.selectedSpriteName)
    {
        [self setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: self.selectedSpriteName]];
    }
    else
    {
        [self toNormal];
    }
}

- (void) setHighlighted:(BOOL)highlighted
{
    if(_disabled)
        return;
    
    _highlighted = highlighted;
    if(_highlighted && self.highlightedSpriteName)
    {
        [self setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: self.highlightedSpriteName]];
    }
    else
    {
        [self toNormal];
    }
}

- (void) setDisabled:(BOOL)disabled
{
    _disabled = disabled;
    if(_disabled)
    {
        if(self.disabledSpriteName)
        {
            [self setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: self.disabledSpriteName]];
        }
        else
        {
            self.color = ccc3(50,50,50);
        }
    }
    else
    {
        [self toNormal];
        self.color = ccc3(255,255,255);
    }
}

- (void) toNormal
{
    [self setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: self.normalSpriteName]];
}

- (void) dealloc
{
    self.normalSpriteName = nil;
    self.highlightedSpriteName = nil;
    self.selectedSpriteName = nil;
    self.disabledSpriteName = nil;
    
    [super dealloc];
}

- (id) initWithSpriteFrameName:(NSString *)spriteFrameName highlightedName: (NSString*) hname selectedName: (NSString*) sname disabledName: (NSString*) dname
{
    self = [super initWithSpriteFrameName: spriteFrameName];
    if(self)
    {
        self.normalSpriteName = spriteFrameName;
        if((NSNull*)hname != [NSNull null])
            self.highlightedSpriteName = hname;
        if((NSNull*)sname != [NSNull null])
            self.selectedSpriteName = sname;
        if((NSNull*)dname != [NSNull null])
            self.disabledSpriteName = dname;
    }
    return self;
}

- (void)onEnter
{
    [[CCDirector sharedDirector].touchDispatcher addTargetedDelegate: self priority: 0 swallowsTouches: YES];
    [super onEnter];
}
- (void)onExit
{
    [[CCDirector sharedDirector].touchDispatcher removeDelegate: self];
    [super onExit];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    if(self.disabled)
        return NO;
    
    //CGPoint pos = [self convertToWorldSpace: [self convertTouchToNodeSpace: touch]];
    CGPoint pos = [self convertTouchToNodeSpace: touch];
    CGRect rect = self.boundingBox;
    rect.origin = CGPointZero;
    BOOL touched = CGRectContainsPoint(rect, pos);
    if(touched)
    {
        self.highlighted = YES;
    }
    return touched;
}

- (void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    self.highlighted = NO;
    CGPoint pos = [self convertTouchToNodeSpace: touch];
    CGRect rect = self.boundingBox;
    rect.origin = CGPointZero;
    BOOL touched = CGRectContainsPoint(rect, pos);

    if(touched)
    {
        [_target performSelector: _action];
    }
}

- (void) setTouchUpInsideTarget: (id) target action: (SEL) action
{
    _target = target;
    _action = action;
}

@end
