//
//  SoundSystem.m
//  MusicCat
//
//  Created by Jason Chang on 11/19/12.
//
//

#import "SoundSystem.h"
#import "SimpleAudioEngine.h"

@implementation SoundSystem

+ (SoundSystem*) system
{
    static SoundSystem *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] init];
    });
    return _sharedClient;
}

- (id) init
{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}

- (void) setMuteMusic:(BOOL)muteMusic
{
    _muteMusic = muteMusic;
    if(_muteMusic)
        [[SimpleAudioEngine sharedEngine] setEffectsVolume: 0];
    else
        [[SimpleAudioEngine sharedEngine] setEffectsVolume: 1];
}

- (void) setMuteSound:(BOOL)muteSound
{
    _muteSound = muteSound;
    if(_muteSound)
        [[SimpleAudioEngine sharedEngine] setEffectsVolume: 0];
    else
        [[SimpleAudioEngine sharedEngine] setEffectsVolume: 1];
}

@end
