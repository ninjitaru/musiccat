//
//  GridManager.h
//  MusicCat
//
//  Created by Jason Chang on 12/18/12.
//
//

@interface GridManager : NSObject
{
    BOOL* _gridMap;
    
    CGPoint _gridOffset;
    float _gridWidth;
    float _gridHeight;
    float _gridSize;
    int _gridXCount;
    int _gridYCount;
    int _gridCount;
    
    CGSize _winSize; // use too freq. save a copy myself
}

@property (readonly) CGPoint range;
@property (readonly) float gridSize;
@property (readonly) int gridXCount;
@property (readonly) int gridYCount;

- (CGPoint) fitGridWithCellSize: (float) cellSize InScreenSize: (CGSize) screenSize;

- (void) setIndex: (int) index withValue: (BOOL) value;
- (BOOL) avaliableGridIndex: (int) index;
- (id) initWithOffset: (CGPoint) offset;
- (CGPoint) gridIndexToPoint: (int) index;
- (int) pointToGridIndex: (CGPoint) pos;
- (void) reset;
- (int) findEmptyIndexFrom: (int) index searchDepth: (int) depth;
- (int) randomAvaliableGridIndex;

@end
