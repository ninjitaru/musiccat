//
//  GamePersistantData.m
//  MusicCat
//
//  Created by Jason Chang on 12/20/12.
//
//

#import "GamePersistantData.h"

#define kCOINKEY @"tick"

@implementation GamePersistantData

+ (GamePersistantData*) sharedData
{
    static GamePersistantData *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] init];
    });
    return _sharedClient;
}

- (id) init
{
    self = [super init];
    if(self)
    {
        int value = [[NSUserDefaults standardUserDefaults] integerForKey: kCOINKEY];
        self.playerCoins = value;
    }
    return self;
}

- (void) save
{
    NSUserDefaults* userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setInteger: self.playerCoins forKey: kCOINKEY];
    [userdefault synchronize];
}

- (int) translateInteger: (int) input
{
    return (((input - 1234) * 2) + 5678);
}

@end
