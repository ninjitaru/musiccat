//
//  SpecialItem.h
//  MusicCat
//
//  Created by Jason Chang on 12/19/12.
//
//

#import <Foundation/Foundation.h>
#import "GameTypes.h"

@interface SpecialItem : NSObject
{
    CCSprite* _sprite;
}

- (id) initWithParent: (CCNode*) parent;

@property (readonly) CCNode* node;
@property (assign, nonatomic) SpecialItemType type;

- (void) setRandomTypeExcludeDouble: (BOOL) shouldExclude;

@end
