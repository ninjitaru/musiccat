//
//  LayerManager.m
//  MusicCat
//
//  Created by Jason Chang on 11/5/12.
//
//

#import "LayerManager.h"

@implementation LayerManager

- (id) initWithParentNode:(CCNode *)node
{
    self = [super init];
    if(self)
    {
        _parentNode = node;
        [self initLayerManager];
    }
    return self;
}

- (void) initLayerManager
{
    NSLog(@"ran LayerManager init");
}

@end
