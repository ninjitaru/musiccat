//
//  ItemManager.m
//  MusicCat
//
//  Created by Jason Chang on 12/18/12.
//
//

#import "ItemManager.h"
#import "GameDepthEnum.h"

#define MAXCOINCOUNT 10
#define kSTATUSBARHEIGHT 50

@implementation ItemManager

- (void) dealloc
{
    [[DebugController debugger] removeDebugDelegate: self];
    [_delegations release];
    [_coins release];
    [super dealloc];
}

- (void) initLayerManager
{
    [self createItem];
    [self createCoin];
    [[DebugController debugger] registerDebugDelegate: self];
}

- (void) setDifficulty:(int)difficulty
{
    _coinSpawnLimit = 0;
    _itemSpawnLimit = 0;
    switch(difficulty)
    {
        case 0:
            _coinSpawnLimit = 10;
            _itemSpawnLimit = 2;
            break;
        case 1:
            _coinSpawnLimit = 30;
            _itemSpawnLimit = 4;
            break;
        case 2:
            _coinSpawnLimit = 50;
            _itemSpawnLimit = 8;
            break;
    }
}

- (void) createItem
{
    _itemSpawnedCount = 0;
    _itemRespawnDelay = 2.0f;
    _itemChance = 0.01f;
    
    _item = [[SpecialItem alloc] initWithParent: _parentNode];
    _item.node.visible = NO;
}

- (void) createCoin
{
    _coinSpawnedCount = 0;
    _coinChance = 0.01f;
    _coinRespawnDelay = 1.0f;
    _coinChanceMod = 0.001f;
    _coinChanceModLimit = 0.49f;
    
    NSMutableArray* array = [NSMutableArray array];
    for(int i = 0; i < MAXCOINCOUNT; i++)
    {
        CCSprite* sprite = [CCSprite spriteWithSpriteFrameName: @"coin2.png"];
        sprite.visible = NO;
        [_parentNode addChild: sprite z: kDepthItem];
        [array addObject: sprite];
    }
    _coins = [array retain];
}

- (void) pause
{
    [_item.node pauseSchedulerAndActions];
    for(CCSprite* sprite in _coins)
    {
        [sprite pauseSchedulerAndActions];
    }
}

- (void) resume
{
    [_item.node resumeSchedulerAndActions];
    for(CCSprite* sprite in _coins)
    {
        [sprite resumeSchedulerAndActions];
    }
}

- (void) restart
{
    _comboActivated = NO;
    _comboCount = 0;
    _correctPercentage = 1.0f;
    [self resetItem];
    [self resetCoin];
}

- (void) update: (ccTime) dt
{
    [self updateCoin: dt];
    [self updateItem: dt];
}

- (void) updateItem: (ccTime) dt
{
    if(_effectActive)
    {
        _effectTimeAccum += dt;
        if(_effectTimeAccum >= _effectTimeLimit)
        {
            for(id<ItemTriggerDelegate> delegate in _delegations)
            {
                [delegate deactivateItem: _effectType];
            }
            _effectTimeAccum = 0;
            _effectActive = NO;
        }
        return;
    }
    
    if(_item.node.visible || (_itemSpawnedCount >= _itemSpawnLimit  && _itemSpawnLimit != 0))
        return;
    
    _itemTimeAccum += dt;
    if(_itemTimeAccum > _itemRespawnDelay && !_item.node.visible)
    {
        [self spawnItem];
        _itemTimeAccum = 0;
    }
}

- (void) updateCoin: (ccTime) dt
{
    if(_coinSpawnedCount >= _coinSpawnLimit && _coinSpawnLimit != 0)
        return;
    
    _coinTimeAccum += dt;
    if(_coinRespawnDelay <= _coinTimeAccum)
    {
        _coinTimeAccum = 0;
        [self spawnCoin];
    }
}

- (void) spawnItem
{
    float chance = _itemChance + [self itemChanceMod];
    if(CCRANDOM_0_1() <= chance)
    {
        [_item setRandomTypeExcludeDouble: !_comboActivated];
        [_item.node stopAllActions];
        [self projectItem: _item.node];
        _item.node.visible = YES;
    }
}

- (float) itemChanceMod
{
    if(_correctPercentage > 0.4)
        return 0;
    if(_correctPercentage > 0.3)
        return 0.01f;
    if(_correctPercentage > 0.2)
        return 0.02f;
    if(_correctPercentage > 0.1)
        return 0.05f;
    return 0.09f;
}

- (void) spawnCoin
{
    float chance = _coinChance + (_comboActivated ? (_coinChanceMod*_comboCount < _coinChanceModLimit ? _coinChanceMod*_comboCount : _coinChanceModLimit) : 0);
    if(CCRANDOM_0_1() <= chance)
    {
        for(CCSprite* sprite in _coins)
        {
            if(sprite.visible)
                continue;
            sprite.visible = YES;
            [self projectItem: sprite];
            if(CCRANDOM_0_1() > 0.1f)
                return;
        }
    }
}

- (void) resetCoin
{
    for(CCSprite* sprite in _coins)
    {
        [sprite stopAllActions];
        sprite.visible = NO;
    }
    _coinSpawnedCount = 0;
    _coinTimeAccum = 0;
}

- (void) resetItem
{
    [_item.node stopAllActions];
    _item.node.visible = NO;
    _itemTimeAccum = 0;
    _itemSpawnedCount = 0;
    
    _effectActive = NO;
}

- (void) projectItem: (CCNode*) sprite
{
    CGSize size = [CCDirector sharedDirector].winSize;
    float test = size.height - kSTATUSBARHEIGHT - size.height*0.3f;
    //NSLog(@"%f", test);
    float height = size.height*0.3f + CCRANDOM_0_1() * (test);
    float width1 = size.width*0.1f + CCRANDOM_0_1()*(size.width*0.8);
    float width2 = size.width*0.1f + CCRANDOM_0_1()*(size.width*0.8);
    int mod = width1 > width2 ? 1 : -1;
    [sprite stopAllActions];
    sprite.position = ccp(width1, -60);
    id action = [CCSpawn actions: [CCRotateBy actionWithDuration: 2.0f angle: 360*mod], [CCJumpTo actionWithDuration: 2.0f position: ccp(width2,-60) height: height jumps: 1], nil];
    [sprite runAction: [CCSequence actions: action, [CCCallBlock actionWithBlock: ^{ sprite.visible = NO; }],nil]];
}

- (BOOL) touchBegin: (CGPoint) pos
{
    return YES;
}

- (void) touchEnded: (CGPoint) pos
{
    if(CGRectContainsPoint(_item.node.boundingBox,pos))
    {
        _item.node.visible = NO;
        [_item.node stopAllActions];
        for(id<ItemTriggerDelegate> delegate in _delegations)
        {
            [delegate activateItem: _item.type];
        }
        _itemSpawnedCount++;
        
        _effectType = _item.type;
        _effectActive = YES;
        _effectTimeAccum = 0;
        _effectTimeLimit = 10.0f;
        
        return;
    }
    
    for(CCSprite* coin in _coins)
    {
        if(coin.visible && CGRectContainsPoint(coin.boundingBox,pos))
        {
            [coin stopAllActions];
            [self informObtainCoins];
            _coinSpawnedCount++;
            coin.visible = NO;
            break;
        }
    }
}

- (void) informObtainCoins
{
    for(id<ItemTriggerDelegate> delegate in _delegations)
    {
        [delegate obtainCoin];
    }
}

// delegation registration

- (void) registerDelegatation: (id<ItemTriggerDelegate>) delegate
{
    if(!_delegations)
    {
        _delegations = [[NSMutableArray alloc] init];
    }
    [_delegations addObject: delegate];
}

- (void) removeDelegation: (id<ItemTriggerDelegate>) delegate
{
    [_delegations removeObject: delegate];
}

// delegate

- (void) noteCorrectPercentage: (float) percentage
{
    _correctPercentage = percentage;
}

- (void) comboFillPercentage: (float) percentage
{
    _comboActivated = NO;
    if(percentage >= 1.0f)
        _comboActivated = YES;
}

- (void) comboAt: (int) comboCount
{
    _comboCount = comboCount;
}

// debug delegate

- (void) setDebug:(BOOL)state
{
    if(state)
    {
        _itemChance = 1.0f;
        _coinChance = 1.0f;
        _itemSpawnLimit = 0;
        _coinSpawnLimit = 0;
    }
    else
    {
        _itemChance = 0.01f;
        _coinChance = 0.01f;
        [self setDifficulty: _difficulty];
    }
}

@end
