//
//  HUDLayer.m
//  MusicCat
//
//  Created by Jason Chang on 11/18/12.
//
//

#import "HUDLayer.h"
#import "GameDepthEnum.h"
#import "SpriteCreator.h"
#import "SoundSystem.h"
#import "GRTouchableCCSprite.h"
#import "MenuLayer.h"
#import "GamePersistantData.h"
#import "DebugController.h"

@interface HUDLayer ()

@property (assign, nonatomic) CCSprite* bgOverlay;
@property (assign, nonatomic) GRTouchableCCSprite* pauseBtn;
@property (assign, nonatomic) GRTouchableCCSprite* resumeBtn;
@property (assign, nonatomic) GRTouchableCCSprite* restartBtn;
@property (assign, nonatomic) GRTouchableCCSprite* stageBtn;
@property (assign, nonatomic) GRTouchableCCSprite* menuBtn;
@property (assign, nonatomic) GRTouchableCCSprite* soundBtn;
@property (assign, nonatomic) GRTouchableCCSprite* musicBtn;

@end

@implementation HUDLayer


- (void) setScoreMode:(BOOL)scoreMode
{
    _scoreMode = scoreMode;
}

- (void) dealloc
{
    [_hideMenuAction release];
    [_showMenuAction release];
    [super dealloc];
}

- (id) initWithColor:(ccColor4B)color
{
    self = [super initWithColor: color];
    if(self)
    {
        [self initLayer];
        [self scheduleUpdate];
    }
    return self;
}

- (void) initLayer
{
    _comboPointBonus = 1.1f;
    _comboPointBonusMod = 0.01f;
    
    self.isTouchEnabled = YES;
    CGSize size = [CCDirector sharedDirector].winSize;
    
    CCSpriteBatchNode* bnode = [CCSpriteBatchNode batchNodeWithFile: @"game_item.png"];
    bnode.anchorPoint = ccp(0,0);
    [self addChild: bnode z: 1];
    NSArray* settings = @[
    @{@"type":@(typeTouchableSprite),
    @"texture":@"button_pause.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(1,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width,size.height)],
    @"name":@"pauseBtn",
    @"selectorname":@"pauseClicked"},
    
    @{@"type":@(typeSprite),
    @"texture":@"pause_overlay.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,size.height+236)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width/2,size.height+566)],
    @"name":@"bgOverlay"},
    
    @{@"type":@(typeTouchableSprite),
    @"texture":@"button_resume.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(120,185)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(280,450)],
    @"name":@"resumeBtn",
    @"depth":@1,
    @"parentname":@"bgOverlay",
    @"selectorname":@"resumeClicked"},
    
    @{@"type":@(typeTouchableSprite),
    @"texture":@"button_restart.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(120,185-46)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(280,450-110)],
    @"name":@"restartBtn",
    @"depth":@1,
    @"parentname":@"bgOverlay",
    @"selectorname":@"restartClicked"},
    
    @{@"type":@(typeTouchableSprite),
    @"texture":@"button_exit.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(120,185-46-46)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(280,450-220)],
    @"name":@"stageBtn",
    @"depth":@1,
    @"parentname":@"bgOverlay",
    @"selectorname":@"toStageClicked"},
    
    @{@"type":@(typeTouchableSprite),
    @"texture":@"button_menu.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(120,185-46-46-46)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(280,460-330)],
    @"name":@"menuBtn",
    @"depth":@1,
    @"parentname":@"bgOverlay",
    @"selectorname":@"toMenuClicked"},
    
    @{@"type":@(typeTouchableSprite),
    @"texture":@"button_sound.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(50,185-42)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(130,450-110)],
    @"name":@"soundBtn",
    @"depth":@1,
    @"parentname":@"bgOverlay",
    @"selectorname":@"toggleSound"},
    
    @{@"type":@(typeTouchableSprite),
    @"texture":@"button_music.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(50,185-42-48)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(130,450-220)],
    @"name":@"musicBtn",
    @"depth":@1,
    @"parentname":@"bgOverlay",
    @"selectorname":@"toggleMusic"},
    
    ];
    
    [[SpriteCreator sharedCreator] addSprites: settings toNode: bnode inObject: self];
    
    _pointLabel = [[CCLabelBMFont alloc] initWithString: @"00000000" fntFile: @"font.fnt"];
    [self addChild: _pointLabel];
    
    _pointLabelScale = 0.5f;
    _pointLabel.scale = _pointLabelScale;
    _pointLabel.anchorPoint = ccp(0.5,1);
    _pointLabel.position = ccp(size.width/2, size.height);
    
    _coinLabel = [[CCLabelBMFont alloc] initWithString: @"0" fntFile: @"font.fnt"];
    [self addChild: _coinLabel];
    _coinLabel.scale = 0.3f;
    _coinLabel.anchorPoint = ccp(0,1);
    _coinLabel.position = ccp(30, size.height-2);
    
    if(!_comboLabel)
    {
        _comboLabel = [[CCLabelBMFont alloc] initWithString: @"combo x " fntFile: @"font.fnt"];
        [self addChild: _comboLabel z: kDepthUI];
        _comboLabel.visible = NO;
        if([DeviceInfoHelper getScreenSizeType] == kscreenSizeIpad)
        {
        
        }
        else
        {
            _comboLabel.scale = 0.5f;
        }
        _comboLabel.position = ccp(size.width/2, size.height*0.7);
    }
}

- (void) displayScore
{
    CGSize size = [CCDirector sharedDirector].winSize;
    self.scoreMode = YES;
    if(!_showMenuAction)
    {
        _showMenuAction = [[CCEaseIn actionWithAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2, size.height)] rate: 0.4f] retain];
        _hideMenuAction = [[CCSequence actions:
                            [CCEaseIn actionWithAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2, size.height+self.bgOverlay.contentSize.height)] rate: 0.6f],
                            [CCCallBlock actionWithBlock: ^{ [self setOpacity: 0]; self.pausedMode = NO; [self.flowDelegate resume]; }],
                            nil] retain];
    }
    [self setOpacity: 100];
    [self.bgOverlay stopAllActions];
    [self.bgOverlay runAction: _showMenuAction];
    
    if(!_finalPointLabel)
    {
        _finalPointLabel = [[CCLabelBMFont alloc] initWithString: @"Final Score" fntFile: @"font.fnt"];
        [self addChild: _finalPointLabel z: kDepthUI];
    }
    if([DeviceInfoHelper getScreenSizeType] == kscreenSizeIpad)
    {
        
    }
    else
    {
        _finalPointLabel.scale = 0.5f;
    }
    
    _finalPointLabel.visible = YES;
    _finalPointLabel.string = [NSString stringWithFormat: @"Final Score %08i", _point];
    _finalPointLabel.position = ccp(size.width/2, size.height/2);
}

- (void) toggleSound
{
    [DebugController debugger].debugOn = YES;
    [SoundSystem system].muteSound = ![SoundSystem system].muteSound;
}

- (void) toggleMusic
{
    [DebugController debugger].debugOn = NO;
    [SoundSystem system].muteMusic = ![SoundSystem system].muteMusic;
}

- (void) resumeClicked
{
    if(self.scoreMode)
        return;
    
    [self.bgOverlay stopAllActions];
    [self.bgOverlay runAction: _hideMenuAction];
}

- (void) restartClicked
{
    _finalPointLabel.visible = NO;
    self.scoreMode = NO;
    _doubleInSection = NO;
    _comboStarted = NO;
    _point = 0;
    _pointLabel.scale = _pointLabelScale;
    [self displayPointsAnimated: NO];
    
    _coin = [GamePersistantData sharedData].playerCoins;
    
    _pausedMode = NO;
    [self setOpacity: 0];
    CGSize size = [CCDirector sharedDirector].winSize;
    self.bgOverlay.position = ccp(size.width/2, size.height+self.bgOverlay.contentSize.height);
    [self.flowDelegate restart];
}

- (void) toMenuClicked
{
    self.scoreMode = NO;
    [self.flowDelegate toMenu];
}

- (void) toStageClicked
{
    self.scoreMode = NO;
    [self.flowDelegate toStageSelection];
}


- (void) pauseClicked
{
    if(self.scoreMode)
    {
        // should not be able to activate pause from score mode
        return;
    }
    
    if(!_showMenuAction)
    {
        CGSize size = [CCDirector sharedDirector].winSize;
        
        _showMenuAction = [[CCEaseIn actionWithAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2, size.height)] rate: 0.4f] retain];
        _hideMenuAction = [[CCSequence actions:
                            [CCEaseIn actionWithAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2, size.height+self.bgOverlay.contentSize.height)] rate: 0.6f],
                            [CCCallBlock actionWithBlock: ^{ [self setOpacity: 0]; self.pausedMode = NO; [self.flowDelegate resume]; }],
                            nil] retain];
    }
    
    BOOL mode = !self.pausedMode;
    
    if(mode)
    {
        if([self.flowDelegate pause])
        {
            [self setOpacity: 100];
            [self.bgOverlay stopAllActions];
            [self.bgOverlay runAction: _showMenuAction];
        }
    }
    else
    {
        [self.bgOverlay stopAllActions];
        [self.bgOverlay runAction: _hideMenuAction];
    }
}

- (void) update: (ccTime) dt
{
    [self displayItem];
}

- (void) displayPointsAnimated: (BOOL) animated
{
    NSString* pointText = [NSString stringWithFormat: @"%08i", _point];
    [_pointLabel stopAllActions];
    if(animated)
    {
        [_pointLabel runAction: [CCSequence actions:[CCScaleTo actionWithDuration: 0.2f scale: (_pointLabelScale+0.1f)], [CCCallBlock actionWithBlock: ^{ _pointLabel.string = pointText; }], [CCScaleTo actionWithDuration: 0.1f scale: _pointLabelScale], nil]];
    }
    else
    {
        _pointLabel.string = pointText;
    }
}

- (void) displayCombo
{
    _comboLabel.string = [NSString stringWithFormat: @"combo x %i", _comboCount];
}

- (void) displayCoin
{
    _coinLabel.string = [@(_coin) stringValue];
}

- (void) displayItem
{
    
}

// music status + action delegate

- (void) placeNote: (BOOL) right onTime: (BOOL) perfect skippedNotes: (int) count atLocation:(CGPoint)pos
{
    NSString* name = @"reaction_good.png";
    if(right)
    {
        float bonus = (_comboStarted ? ((_comboCount - 25) * _comboPointBonusMod + _comboPointBonus) : 1);
        _point += (perfect ? 100 : 10) * bonus;
        [self displayPointsAnimated: YES];
        if(perfect)
            name = @"reaction_perfect.png";
    }
    else
    {
        name = @"reaction_oops.png";
    }
    
    //CGSize size = [CCDirector sharedDirector].winSize;
    CCSprite* sprite = [CCSprite spriteWithSpriteFrameName: name];
    [self addChild: sprite z: kDepthFront];
    [sprite runAction: [CCSequence actions:[CCDelayTime actionWithDuration: 0.5f],[CCCallBlock actionWithBlock: ^{ [self removeChild: sprite cleanup: YES]; } ], nil]];
    //sprite.position = ccp(size.width/2, size.height*0.9);
    sprite.position = ccpAdd(pos, ccp(0,50));
}

- (void) noteMissed
{
    
}

- (void) comboStarted
{
    _comboStarted = YES;
    _comboLabel.visible = YES;
}

- (void) comboEnded
{
    _comboStarted = NO;
    _comboCount = 0;
    _comboLabel.visible = NO;
}

- (void) noteCorrectPercentage: (float) percentage
{
    
}

- (void) comboFillPercentage: (float) percentage
{
    [self displayCombo];
}

- (void) comboAt: (int) comboCount
{
    _comboCount = comboCount;
    [self displayCombo];
}

// item trigger delegate

- (void) activateItem: (SpecialItemType) itemType
{
    if(itemType == kItemDoubleCoin)
        _doubleInSection = YES;
}

- (void) deactivateItem: (SpecialItemType) itemType
{
    if(itemType == kItemDoubleCoin)
        _doubleInSection = NO;
}

- (void) obtainCoin
{
    _coin += 1 * (_doubleInSection ? 2 : 1);
    [self displayCoin];
}

@end
