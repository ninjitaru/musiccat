//
//  CCFileUtils+DocumentSupport.m
//  MusicCat
//
//  Created by Jason Chang on 12/18/12.
//
//
//
//#import "CCFileUtils+DocumentSupport.h"
//
//@implementation CCFileUtils (custom)
//
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"
//
//
//-(NSString*) pathForResource:(NSString*)resource ofType:(NSString *)ext inDirectory:(NSString *)subpath
//{
//    NSString* path = [bundle_ pathForResource:resource
//                                       ofType:ext
//                                  inDirectory:subpath];
//    if(!path)
//    {
//        NSString *imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent: resource];
//        if([fileManager_ fileExistsAtPath: imagePath])
//            return imagePath;
//    }
//    return path;
//}
//
//-(BOOL) fileExistsAtPath:(NSString*)relPath withSuffix:(NSString*)suffix
//{
//	NSString *fullpath = nil;
//    
//	// only if it is not an absolute path
//	if( ! [relPath isAbsolutePath] ) {
//		// pathForResource also searches in .lproj directories. issue #1230
//		NSString *file = [relPath lastPathComponent];
//		NSString *imageDirectory = [relPath stringByDeletingLastPathComponent];
//        
//		fullpath = [bundle_ pathForResource:file
//                                     ofType:nil
//                                inDirectory:imageDirectory];
//        
//	}
//    
//	if (fullpath == nil)
//		fullpath = relPath;
//    
//	NSString *path = [self getPath:fullpath forSuffix:suffix];
//    
//	return ( path != nil );
//}
//
//-(NSString*) getPath:(NSString*)path forSuffix:(NSString*)suffix
//{
//	NSString *newName = path;
//	
//	// only recreate filename if suffix is valid
//	if( suffix && [suffix length] > 0)
//	{
//		NSString *pathWithoutExtension = [path stringByDeletingPathExtension];
//		NSString *name = [pathWithoutExtension lastPathComponent];
//        
//		// check if path already has the suffix.
//		if( [name rangeOfString:suffix].location == NSNotFound ) {
//			
//            
//			NSString *extension = [path pathExtension];
//            
//			if( [extension isEqualToString:@"ccz"] || [extension isEqualToString:@"gz"] )
//			{
//				// All ccz / gz files should be in the format filename.xxx.ccz
//				// so we need to pull off the .xxx part of the extension as well
//				extension = [NSString stringWithFormat:@"%@.%@", [pathWithoutExtension pathExtension], extension];
//				pathWithoutExtension = [pathWithoutExtension stringByDeletingPathExtension];
//			}
//            
//            
//			newName = [pathWithoutExtension stringByAppendingString:suffix];
//			newName = [newName stringByAppendingPathExtension:extension];
//		} else
//			CCLOGWARN(@"cocos2d: WARNING Filename(%@) already has the suffix %@. Using it.", name, suffix);
//	}
//    
//	NSString *ret = nil;
//	// only if it is not an absolute path
//	if( ! [path isAbsolutePath] ) {
//		
//		// pathForResource also searches in .lproj directories. issue #1230
//		NSString *imageDirectory = [path stringByDeletingLastPathComponent];
//		
//		// If the file does not exist it will return nil.
//		ret = [self pathForResource:[newName lastPathComponent]
//                             ofType:nil
//                        inDirectory:imageDirectory];
//	}
//	else if( [fileManager_ fileExistsAtPath:newName] )
//		ret = newName;
//    
//	if( ! ret )
//		CCLOGINFO(@"cocos2d: CCFileUtils: file not found: %@", [newName lastPathComponent] );
//    
//	return ret;
//}
//
//
//// do your override
//
//#pragma clang diagnostic pop
//
//@end
