//
//  DebugController.h
//  MusicCat
//
//  Created by Jason Chang on 12/21/12.
//
//

#import <Foundation/Foundation.h>

@protocol DebugDelegate <NSObject>

- (void) setDebug: (BOOL) state;

@end

@interface DebugController : NSObject
{
    NSMutableArray* _delegates;
}

+ (DebugController*) debugger;

@property (nonatomic, assign) BOOL debugOn;

- (void) registerDebugDelegate: (id<DebugDelegate>) delegate;
- (void) removeDebugDelegate: (id<DebugDelegate>) delegate;
- (void) removeAllDebugDelegate;

@end
