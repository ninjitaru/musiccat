//
//  GameConstants.h
//  MusicCat
//
//  Created by Jason Chang on 12/21/12.
//
//

#import <Foundation/Foundation.h>

@interface GameConstants : NSObject

+ (GameConstants*) constants;

@property (assign) BOOL colorMode;
@property (assign) BOOL catBandMode;

@property (assign) int comboTriggerValue;
@property (assign) float randomMusicNotePositionChance;
@property (assign) float gridXOffset;
@property (assign) float gridYDelta;
@property (assign) float perfectDragDelay;

@property (assign) int fullCatBellyCount;

@end
