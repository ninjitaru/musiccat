//
//  SystemFlowDelegate.h
//  MusicCat
//
//  Created by Jason Chang on 11/19/12.
//
//

#ifndef MusicCat_SystemFlowDelegate_h
#define MusicCat_SystemFlowDelegate_h

@protocol SystemFlowDelegate <NSObject>

- (void) resume;
- (BOOL) pause;
- (void) restart;
- (void) toMenu;
- (void) toStageSelection;

@end


#endif
