//
//  CatManager.h
//  MusicCat
//
//  Created by Jason Chang on 12/11/12.
//
//

#import "LayerManager.h"
#import "MusicNoteStatusDelegate.h"

@interface CatManager : LayerManager <MusicNoteActionDelegate>
{
    CCSprite* _cat;
}

@property (nonatomic, readonly) CCSprite* cat;

- (void) update: (ccTime) dt;
- (void) restart;

@end
