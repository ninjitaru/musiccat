//
//  SettingDataFactory.m
//  MusicCat
//
//  Created by Jason Chang on 12/11/12.
//
//

#import "SettingDataFactory.h"
#import "StageInfo.h"
#import "Song.h"
#import "SpriteCreator.h"
#import "GameDepthEnum.h"
#import "GRTextureMaskSprite.h"

@implementation SettingDataFactory

+ (SettingDataFactory*) sharedFactory
{
    static SettingDataFactory *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] init];
    });
    return _sharedClient;
}

- (void) dealloc
{
    [_stageDetailDataMap release];
    [_stages release];
    [_songs release];
    [super dealloc];
}

- (id) init
{
    self = [super init];
    if(self)
    {
        _stageDetailDataMap = [@{
                               @"halloween.png":@"populateHalloweenStage:",
                               @"pirate.png":@"populatePirateStage:",
                               @"sea.png":@"populateSeaStage:",
                               @"zeus_temple.png":@"populateTempleStage:",
                               @"fairy.png":@"populateFairylandStage:",
                               } retain];
    }
    return self;
}

- (void) loadData
{
    [self createSongDataArray];
    [self createStageDataArray];
}

- (void) createSongDataArray
{
    // name, timing, second per beat, beat per section, song length in second
    // track 1
    NSArray* songData = @[
    
    @[@"halloween.mp3",@[
    @[@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12, @16, @17,@18,@19 , @22,@23 ,@25,@26,@27,@28, @37,@38,@41,@42,@43,@44,@49,@50,@51,@52,@66,@69,@72,@77,@78,@79,@80,@90,@92,@100, @100,@101,@102,@103,@142,@144,@146,@148,@150,@159,@160,@161,@162,@163,@168,@191,@192,@193,@194,@195,@196,@197,@198,@199],
    @[@1,@2,@5,@6,@13,@14,@15,@20,@21,@24,@53,@54,@55,@56,@57,@59,@62,@63,@67,@70,@73,@81,@82,@83,@84,@94,@95,@104,@105,@106,@107,@132,@134,@136,@138,@140,@155,@156,@157,@158,@164,@165,@170,@171,@172,@173,@174,@175,@176,@177,@178,@179,@180,@200],
    @[@1,@2,@7,@8,@9,@10,@12,@13,@15,@16,@19,@20,@21,@25,@26,@30,@31,@32,@35,@36,@40,@41,@44,@45,@47,@48,@50,@52,@54,@58,@60,@61,@64,@65,@68,@71,@74,@85,@86,@87,@88,@97,@99,@108,@109,@110,@111,@112,@113,@114,@115,@116,@117,@118,@119,@120,@122,@124,@126,@128,@130,@151,@152,@153,@154,@166,@167,@169,@181,@182,@183,@184,@185,@186,@187,@188,@189,@190]
    ],@(60/132.0f), @4],
    
    @[@"piratedeck.mp3",@[
    @[@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12, @16, @17,@18,@19 , @22,@23 ,@25,@26,@27,@28, @37,@38,@41,@42,@43,@44,@49,@50,@51,@52,@66,@69,@72,@77,@78,@79,@80,@90,@92,@100, @100,@101,@102,@103,@142,@144,@146,@148,@150,@159,@160,@161,@162,@163,@168,@191,@192,@193,@194,@195,@196,@197,@198,@199],
    @[@1,@2,@5,@6,@13,@14,@15,@20,@21,@24,@53,@54,@55,@56,@57,@59,@62,@63,@67,@70,@73,@81,@82,@83,@84,@94,@95,@104,@105,@106,@107,@132,@134,@136,@138,@140,@155,@156,@157,@158,@164,@165,@170,@171,@172,@173,@174,@175,@176,@177,@178,@179,@180,@200],
    @[@1,@2,@7,@8,@9,@10,@12,@13,@15,@16,@19,@20,@21,@25,@26,@30,@31,@32,@35,@36,@40,@41,@44,@45,@47,@48,@50,@52,@54,@58,@60,@61,@64,@65,@68,@71,@74,@85,@86,@87,@88,@97,@99,@108,@109,@110,@111,@112,@113,@114,@115,@116,@117,@118,@119,@120,@122,@124,@126,@128,@130,@151,@152,@153,@154,@166,@167,@169,@181,@182,@183,@184,@185,@186,@187,@188,@189,@190]
    ],@(60/114.0f), @4],
    
    @[@"sea.mp3",@[
    @[@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12, @16, @17,@18,@19 , @22,@23 ,@25,@26,@27,@28, @37,@38,@41,@42,@43,@44,@49,@50,@51,@52,@66,@69,@72,@77,@78,@79,@80,@90,@92,@100, @100,@101,@102,@103,@142,@144,@146,@148,@150,@159,@160,@161,@162,@163,@168,@191,@192,@193,@194,@195,@196,@197,@198,@199],
    @[@1,@2,@5,@6,@13,@14,@15,@20,@21,@24,@53,@54,@55,@56,@57,@59,@62,@63,@67,@70,@73,@81,@82,@83,@84,@94,@95,@104,@105,@106,@107,@132,@134,@136,@138,@140,@155,@156,@157,@158,@164,@165,@170,@171,@172,@173,@174,@175,@176,@177,@178,@179,@180,@200],
    @[@1,@2,@7,@8,@9,@10,@12,@13,@15,@16,@19,@20,@21,@25,@26,@30,@31,@32,@35,@36,@40,@41,@44,@45,@47,@48,@50,@52,@54,@58,@60,@61,@64,@65,@68,@71,@74,@85,@86,@87,@88,@97,@99,@108,@109,@110,@111,@112,@113,@114,@115,@116,@117,@118,@119,@120,@122,@124,@126,@128,@130,@151,@152,@153,@154,@166,@167,@169,@181,@182,@183,@184,@185,@186,@187,@188,@189,@190]
    ],@(60/58.0f), @4],
    
    @[@"zeus_temple.mp3",@[
    @[@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12, @16, @17,@18,@19 , @22,@23 ,@25,@26,@27,@28, @37,@38,@41,@42,@43,@44,@49,@50,@51,@52,@66,@69,@72,@77,@78,@79,@80,@90,@92,@100, @100,@101,@102,@103,@142,@144,@146,@148,@150,@159,@160,@161,@162,@163,@168,@191,@192,@193,@194,@195,@196,@197,@198,@199],
    @[@1,@2,@5,@6,@13,@14,@15,@20,@21,@24,@53,@54,@55,@56,@57,@59,@62,@63,@67,@70,@73,@81,@82,@83,@84,@94,@95,@104,@105,@106,@107,@132,@134,@136,@138,@140,@155,@156,@157,@158,@164,@165,@170,@171,@172,@173,@174,@175,@176,@177,@178,@179,@180,@200],
    @[@1,@2,@7,@8,@9,@10,@12,@13,@15,@16,@19,@20,@21,@25,@26,@30,@31,@32,@35,@36,@40,@41,@44,@45,@47,@48,@50,@52,@54,@58,@60,@61,@64,@65,@68,@71,@74,@85,@86,@87,@88,@97,@99,@108,@109,@110,@111,@112,@113,@114,@115,@116,@117,@118,@119,@120,@122,@124,@126,@128,@130,@151,@152,@153,@154,@166,@167,@169,@181,@182,@183,@184,@185,@186,@187,@188,@189,@190]
    ],@(60/108.0f), @4],
    
    @[@"fairyland.mp3",@[
    @[@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12, @16, @17,@18,@19 , @22,@23 ,@25,@26,@27,@28, @37,@38,@41,@42,@43,@44,@49,@50,@51,@52,@66,@69,@72,@77,@78,@79,@80,@90,@92,@100, @100,@101,@102,@103,@142,@144,@146,@148,@150,@159,@160,@161,@162,@163,@168,@191,@192,@193,@194,@195,@196,@197,@198,@199],
    @[@1,@2,@5,@6,@13,@14,@15,@20,@21,@24,@53,@54,@55,@56,@57,@59,@62,@63,@67,@70,@73,@81,@82,@83,@84,@94,@95,@104,@105,@106,@107,@132,@134,@136,@138,@140,@155,@156,@157,@158,@164,@165,@170,@171,@172,@173,@174,@175,@176,@177,@178,@179,@180,@200],
    @[@1,@2,@7,@8,@9,@10,@12,@13,@15,@16,@19,@20,@21,@25,@26,@30,@31,@32,@35,@36,@40,@41,@44,@45,@47,@48,@50,@52,@54,@58,@60,@61,@64,@65,@68,@71,@74,@85,@86,@87,@88,@97,@99,@108,@109,@110,@111,@112,@113,@114,@115,@116,@117,@118,@119,@120,@122,@124,@126,@128,@130,@151,@152,@153,@154,@166,@167,@169,@181,@182,@183,@184,@185,@186,@187,@188,@189,@190]
    ],@(60/120.0f), @4],
    
    ];
    
    NSMutableArray* array = [NSMutableArray array];
    for(NSArray* data in songData)
    {
        Song* asong = [self createSongFrom: data];
        [array addObject: asong];
    }
    _songs = [array retain];
}

- (void) createStageDataArray
{
    // preview, label, plist, plist image, song index
    NSArray* stageData = @[
    @[@"halloween.png",@"word_halloween.png",@"halloween.plist",@"halloween.png",@0],
    @[@"pirate.png", @"word_piratedeck.png",@"pirate.plist",@"pirate.png",@1],
    @[@"sea.png", @"word_sea.png",@"sea.plist",@"sea.png",@2],
    @[@"zeus_temple.png", @"word_temple.png",@"zeus_temple.plist",@"zeus_temple.png",@3],
    @[@"fairy.png", @"word_fairyland.png",@"fairyland.plist",@"fairyland.png",@4]
    ];
    NSMutableArray* temp = [NSMutableArray array];
    for(NSArray* data in stageData)
    {
        StageInfo* stage = [self createStageFrom: data];
        [temp addObject: stage];
        SEL sel = NSSelectorFromString(_stageDetailDataMap[data[0]]);
        [self performSelector: sel withObject: stage];
    }
    _stages = [temp retain];
}

- (void) populateHalloweenStage: (StageInfo*) stage
{
    //CGSize size = [CCDirector sharedDirector].winSize;
    NSArray* settings = @[
//    @{@"type":@(typeBatch),
//    @"texture":@"halloween.png",
//    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"depth":@(kDepthBackground),
//    @"name":@"batch"},
    
    @{@"type":@(typeSprite),
    @"purepng":@YES,
    @"texture":@"halloween.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"iphoneposition": [NSValue valueWithCGPoint: CGPointMake(-45,0)], // 480x320
//    @"iphone5position": [NSValue valueWithCGPoint: CGPointMake(0,0)],  // 568x320
//    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(-169,0)],  // 1024x768
    @"scale": @1,
//    @"parentname":@"batch",
    @"name":@"bg"
    },
    ];
    stage.spriteData = settings;
    
}

- (void) populatePirateStage: (StageInfo*) stage
{
    // CGSize size = [CCDirector sharedDirector].winSize;
    NSArray* settings = @[
//    @{@"type":@(typeBatch),
//    @"texture":@"pirate.png",
//    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"depth":@(kDepthBackground),
//    @"name":@"batch"},
    
    @{@"type":@(typeSprite),
    @"purepng":@YES,
    @"texture":@"pirate.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"iphoneposition": [NSValue valueWithCGPoint: CGPointMake(-45,0)], // 480x320
//    @"iphone5position": [NSValue valueWithCGPoint: CGPointMake(0,0)],  // 568x320
//    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(-169,0)],  // 1024x768
    @"scale": @1,
//    @"parentname":@"batch",
    @"name":@"bg"
    },
    
    ];
    
    stage.spriteData = settings;
}


- (void) populateSeaStage: (StageInfo*) stage
{
    NSArray* settings = @[
//    @{@"type":@(typeBatch),
//    @"texture":@"sea.png",
//    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"depth":@(kDepthBackground),
//    @"name":@"batch"},
    
    @{@"type":@(typeSprite),
    @"purepng":@YES,
    @"texture":@"sea.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"iphoneposition": [NSValue valueWithCGPoint: CGPointMake(-45,0)], // 480x320
//    @"iphone5position": [NSValue valueWithCGPoint: CGPointMake(0,0)],  // 568x320
//    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(-169,0)],  // 1024x768
    @"scale": @1,
//    @"parentname":@"batch",
    @"name":@"bg"},
    ];
    
    stage.spriteData = settings;
}



- (void) populateTempleStage: (StageInfo*) stage
{
    // CGSize size = [CCDirector sharedDirector].winSize;
    NSArray* settings = @[
//    @{@"type":@(typeBatch),
//    @"texture":@"zeus_temple.png",
//    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"depth":@(kDepthBackground),
//    @"name":@"batch"},
    
    @{@"type":@(typeSprite),
    @"purepng":@YES,
    @"texture":@"zeus_temple.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"iphoneposition": [NSValue valueWithCGPoint: CGPointMake(-45,0)], // 480x320
//    @"iphone5position": [NSValue valueWithCGPoint: CGPointMake(0,0)],  // 568x320
//    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(-169,0)],  // 1024x768
    @"scale": @1,
//    @"parentname":@"batch",
    @"name":@"bg"
    },
    
    ];
    
    stage.spriteData = settings;
}

- (void) populateFairylandStage: (StageInfo*) stage
{
    // CGSize size = [CCDirector sharedDirector].winSize;
    NSArray* settings = @[
//    @{@"type":@(typeBatch),
//    @"texture":@"fairyland.png",
//    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"depth":@(kDepthBackground),
//    @"name":@"batch"},
    
    @{@"type":@(typeSprite),
    @"purepng":@YES,
    @"texture":@"fairy.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(0,0)],
//    @"iphoneposition": [NSValue valueWithCGPoint: CGPointMake(-45,0)], // 480x320
//    @"iphone5position": [NSValue valueWithCGPoint: CGPointMake(0,0)],  // 568x320
//    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(-169,0)],  // 1024x768
    @"scale": @1,
//    @"parentname":@"batch",
    @"name":@"bg"
    },
    
    ];
    
    stage.spriteData = settings;
}


- (StageInfo*) createStageFrom: (NSArray*) data
{
    StageInfo* stage = [[[StageInfo alloc] init] autorelease];
    stage.previewImage = data[0];
    stage.labelImage = data[1];
    stage.plistFile = data[2];
    stage.imageFile = data[3];
    stage.song = _songs[[data[4] intValue]];
    return stage;
}

- (Song*) createSongFrom: (NSArray*) data
{
    Song* song = [[[Song alloc] init] autorelease];
    song.soundFileName = data[0];
    song.trackTimings = data[1];
    song.secondPerBeat = [data[2] floatValue];
    song.beatPerSection = [data[3] intValue];
    return song;
}

@end
