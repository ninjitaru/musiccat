//
//  SpriteCreator.m
//  MusicCat
//
//  Created by Jason Chang on 11/19/12.
//
//

#import "SpriteCreator.h"
#import "GRTouchableCCSprite.h"
#import "GRTextureMaskSprite.h"

@implementation SpriteCreator

+ (SpriteCreator*) sharedCreator
{
    static SpriteCreator *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] init];
    });
    return _sharedClient;
}

- (void) dealloc
{
    [spriteProcessorMap release];
    [super dealloc];
}

- (id) init
{
    self = [super init];
    if(self)
    {
        spriteProcessorMap = [@{@(typeSprite):@"addCCSprite:",
                              @(typeTouchableSprite):@"addTouchableSprite:",
                              @(typeMaskSprite):@"addMaskSprite:",
                              @(typeBatch):@"addBatchSprite:"}
                              retain];
        _screenSizeType = [DeviceInfoHelper getScreenSizeType];
        _screenTypePositionString = @"iphoneposition";
        if(_screenSizeType == kscreenSizeIphone5)
            _screenTypePositionString = @"iphone5position";
        else if(_screenSizeType == kscreenSizeIpad)
        {
            _screenTypePositionString = @"ipadposition";
        }
    }
    return self;
}

- (void) addAnimations: (NSArray*) datas inObject: (id) object
{
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    NSMutableArray* array = [NSMutableArray array];
    for(NSDictionary* data in datas)
    {
        for(NSString* framename in data[@"framename"])
        {
            [array addObject: [cache spriteFrameByName: framename]];
        }
        id action = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames: array delay:0.5f]];
        [object setValue: action forKey: data[@"name"]];
        [array removeAllObjects];
    }
}

// purepng = yes,no
// class type = type
// sprite texture name = texture
// mask sprite = masktexture
// sprite highlight = highlighttexture
// sprite selected = selecttexture
// sprite disable = disabletexture
// anchor = anchor
// position = position , iphoneposition, iphone5position, ipadposition
// depth = depth
// tag = tag
// object property name = name
// parent = parentname
// selector function = selectorname
// batch capacity = capacity
// scale (float) = scale 

- (void) addSprites: (NSArray*) settings toNode: (CCNode*) node inObject: (id) object
{
    if(!node)
        return;
    _currentNode = node;
    _currentObject = object;
    for(NSDictionary* data in settings)
    {
        //kSpriteType tt = [data[@"type"] intValue];
        SEL test = NSSelectorFromString(spriteProcessorMap[@([data[@"type"] intValue])]);
        [self performSelector: test withObject: data];
    }
    _currentNode = nil;
    _currentObject = nil;
}

- (void) addCCSprite: (NSDictionary*) data
{
    CCSprite* sprite = nil;
    if(data[@"purepng"])
        sprite = [[CCSprite alloc] initWithFile: data[@"texture"]];
    else
        sprite = [[CCSprite alloc] initWithSpriteFrameName: data[@"texture"]];
    
    if(data[@"anchor"])
        sprite.anchorPoint = [data[@"anchor"] CGPointValue];
    [self setPosition: sprite with: data];
    if(data[@"scale"])
        sprite.scale = [data[@"scale"] floatValue];
    CCNode* parent = _currentNode;
    if(data[@"parentname"])
    {
        parent = [_currentObject valueForKey: data[@"parentname"]];
    }
    int depth = [data[@"depth"] integerValue];
    int tag = [data[@"tag"] integerValue];
    [parent addChild: sprite z: depth tag: tag];
    if(_currentObject && data[@"name"])
    {
        [_currentObject setValue: sprite forKey: data[@"name"]];
    }
    [sprite release];
}

- (void) addTouchableSprite: (NSDictionary*) data
{
    GRTouchableCCSprite* sprite = [[GRTouchableCCSprite alloc] initWithSpriteFrameName: data[@"texture"]
                                                                       highlightedName: data[@"highlighttexture"]
                                                                          selectedName: data[@"selecttexture"]
                                                                          disabledName: data[@"disabletexture"]];
    if(data[@"anchor"])
        sprite.anchorPoint = [data[@"anchor"] CGPointValue];
    [self setPosition: sprite with: data];
    if(data[@"scale"])
        sprite.scale = [data[@"scale"] floatValue];
    CCNode* parent = _currentNode;
    if(data[@"parentname"])
    {
        parent = [_currentObject valueForKey: data[@"parentname"]];
    }
    int depth = [data[@"depth"] integerValue];
    int tag = [data[@"tag"] integerValue];
    [parent addChild: sprite z: depth tag: tag];
    if(_currentObject)
    {
        if(data[@"name"])
        {
            [_currentObject setValue: sprite forKey: data[@"name"]];
        }
        if(data[@"selectorname"])
        {
            [sprite setTouchUpInsideTarget: _currentObject action: NSSelectorFromString(data[@"selectorname"])];
        }
    }
    [sprite release];
}

- (void) addMaskSprite: (NSDictionary*) data
{
    GRTextureMaskSprite* sprite = [[GRTextureMaskSprite alloc] initWithSpriteFrameName: data[@"texture"] maskSpriteFrameName: data[@"masktexture"]];
    if(data[@"anchor"])
        sprite.anchorPoint = [data[@"anchor"] CGPointValue];
    [self setPosition: sprite with: data];
    if(data[@"scale"])
        sprite.scale = [data[@"scale"] floatValue];
    CCNode* parent = _currentNode;
    if(data[@"parentname"])
    {
        parent = [_currentObject valueForKey: data[@"parentname"]];
    }
    int depth = [data[@"depth"] integerValue];
    int tag = [data[@"tag"] integerValue];
    [parent addChild: sprite z: depth tag: tag];
    if(_currentObject)
    {
        if(data[@"name"])
        {
            [_currentObject setValue: sprite forKey: data[@"name"]];
        }
    }
    [sprite release];
}

- (void) addBatchSprite: (NSDictionary*) data
{
    int capacity = [data[@"capacity"] intValue];
    if(capacity == 0)
        capacity = 10;
    CCSpriteBatchNode* batch = [[CCSpriteBatchNode alloc] initWithFile: data[@"texture"] capacity: capacity];
    if(data[@"ahchor"])
        batch.anchorPoint = [data[@"anchor"] CGPointValue];
    [self setPosition: batch with: data];
    if(data[@"scale"])
        batch.scale = [data[@"scale"] floatValue];
    CCNode* parent = _currentNode;
    if(data[@"parentname"])
    {
        parent = [_currentObject valueForKey: data[@"parentname"]];
    }
    int depth = [data[@"depth"] integerValue];
    int tag = [data[@"tag"] integerValue];
    [parent addChild: batch z: depth tag: tag];
    if(_currentObject)
    {
        if(data[@"name"])
        {
            [_currentObject setValue: batch forKey: data[@"name"]];
        }
    }

    [batch release];
}

- (void) setPosition: (CCNode*) node with: (NSDictionary*) data
{
    if(data[_screenTypePositionString])
    {
        node.position = [data[_screenTypePositionString] CGPointValue];
        return;
    }
    if(data[@"position"])
        node.position = [data[@"position"] CGPointValue];
}

@end
