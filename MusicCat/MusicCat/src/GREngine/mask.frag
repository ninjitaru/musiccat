#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
uniform sampler2D u_texture;
uniform sampler2D u_mask;

void main()
{
    vec4 texColor = texture2D(u_texture, v_texCoord);
    vec4 maskColor = texture2D(u_mask, v_texCoord);
    float visible = texColor.a * maskColor.a;
    vec4 finalColor = vec4(texColor.r*visible, texColor.g*visible, texColor.b*visible, visible);
    gl_FragColor = v_fragmentColor * finalColor;
}