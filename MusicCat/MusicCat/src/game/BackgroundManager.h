//
//  BackgroundManager.h
//  MusicCat
//
//  Created by Jason Chang on 11/5/12.
//
//

#import "LayerManager.h"
#import "StageInfo.h"
#import "GRTextureMaskSprite.h"

@interface BackgroundManager : LayerManager
{
    StageInfo* _stage;
    
    NSMutableDictionary* _spriteMap;
    NSMutableDictionary* _animationMap;
}

@property (readonly) StageInfo* currentStage;

- (id) initWithParentNode:(CCNode *)node andStage: (StageInfo*) stage;
- (void) updateMusic: (float) musicDelta;
- (void) update: (ccTime) dt;

@end
