//
//  MusicNoteManager.m
//  MusicCat
//
//  Created by Jason Chang on 12/11/12.
//
//

#import "MusicNoteManager.h"
#import "GameDepthEnum.h"
#import "TouchTracker.h"
#import "MusicManager.h"
#import "GameConstants.h"

#define MAXNOTES 24

@implementation MusicNoteManager

- (void) dealloc
{
    [_catBandBar release];
    [_temp release];
    [_actionDelegations release];
    [_statusDelegations release];
    [_blades release];
    [_gridManager release];
    [_notePool release];
    [_recyclePool release];
    [_musicBar release];
    [_activePool release];
    [_song release];
    [super dealloc];
}

- (void) initLayerManager
{
    _trackManager = [[TrackPatternManager alloc] init];
    _comboCount = 0;
    _comboTriggerValue = [GameConstants constants].comboTriggerValue;
    _randomNotePositionChance = [GameConstants constants].randomMusicNotePositionChance;
    _perfectDelay = [GameConstants constants].perfectDragDelay;
    _winSize = [CCDirector sharedDirector].winSize;
    _colorMode = [GameConstants constants].colorMode;
    _catBandMode = [GameConstants constants].catBandMode;
    _temp = [[NSMutableArray alloc] init];
}

- (id) initWithParentNode:(CCNode *)node andSong:(Song *)song andTouchTracker:(TouchTracker *)tracker
{
    self = [super initWithParentNode: node];
    if(self)
    {
        _tracker = tracker;
        _song = [song retain];
        _internalIndex = 0;
        
        [self initWithSong];

        _gridOffset = ccp([GameConstants constants].gridXOffset ,_bandHeight + _winSize.height* [GameConstants constants].gridYDelta);
        _gridManager = [[GridManager alloc] initWithOffset: _gridOffset];
        [self setupNotes];
        [self initBlade];
    }
    return self;
}

- (void) initBlade
{
    _bladeTexture = [[CCTextureCache sharedTextureCache] addImage:@"streak1.png"];
    
    _blades = [[NSMutableArray array] retain];
    for(int i = 0; i < [TouchTracker maxTouchCount] ; i++)
    {
        CCBlade* blade = [CCBlade bladeWithMaximumPoint: 40];
        blade.texture = _bladeTexture;
        blade.autoDim = NO;
        blade.width = ((MusicBall*)[_notePool lastObject]).node.contentSize.width*0.45;
        [_parentNode addChild: blade z: kDepthStreak];
        [_blades addObject: blade];
    }
}

- (void) initWithSong
{
    if(_catBandMode)
    {
        _catBandBar = [[CatBandBar alloc] initWithParentNode: _parentNode];
        _bandHeight = _winSize.height * [GameConstants constants].gridYDelta + _catBandBar.height;
    }
    else
    {
        _musicBar = [[MusicBar alloc] initWithParent: _parentNode];
        _musicBar.node.anchorPoint = ccp(0,0);
        _musicBar.node.zOrder = kDepthBand;
        _musicBar.timePerSection = _song.timePerSection;
        _musicBar.song = _song;
        [_musicBar createNotePreview];
        _bandHeight = _winSize.height * [GameConstants constants].gridYDelta + _musicBar.height;
    }
}

- (void) setupNotes
{
    _activePool = [[NSMutableArray alloc] initWithCapacity: MAXNOTES];
    _notePool = [[NSMutableArray alloc] initWithCapacity: MAXNOTES];
    _recyclePool = [[NSMutableArray alloc] initWithCapacity: MAXNOTES/2];
    _noteBatch = [CCSpriteBatchNode batchNodeWithFile: @"game_item.png"];
    [_parentNode addChild: _noteBatch z: kDepthNote];
    for(int i = 0; i < MAXNOTES; i++)
    {
        MusicBall* ball = [[MusicBall alloc] initWithParent: _noteBatch];
        ball.touchTracker = _tracker;
        ball.gridManager = _gridManager;
        [ball reset];
        [_notePool addObject: ball];
        [ball release];
    }
}

- (void) pause
{
    for(MusicBall* ball in _activePool)
    {
        [ball.node pauseSchedulerAndActions];
    }
}

- (void) resume
{
    for(MusicBall* ball in _activePool)
    {
        [ball.node resumeSchedulerAndActions];
    }
}

- (void) restart
{
    [_temp removeAllObjects];
    _lastCreated = nil;
    _pullNextNote = NO;
    _effectActivated = NO;
    _nextCorrectNote = nil;
    
    _noteSpawned = 0;
    _noteCorrect = 0;
    _noteMissed = 0;
    _noteWrong = 0;
    
    _comboCount = 0;
    _comboTriggerValue = 6;
    _internalIndex = 0;
    [self resetNotePool];
    [_gridManager reset];
    [_musicBar reset];
    [_catBandBar reset];
}

- (void) resetNotePool
{
    for(MusicBall* ball in _activePool)
    {
        [ball reset];
    }
    [_notePool addObjectsFromArray: _activePool];
    [_activePool removeAllObjects];
}

- (void) stop
{
    //[_musicBar reset];
}

- (void) updateMusic: (float) musicDelta
{
    if(!_catBandMode)
    {
        [_musicBar update: musicDelta];
    }
}

- (void) update: (ccTime) dt
{
    if(!_catBandBar.start)
        _catBandBar.start = YES;
    [_catBandBar update: dt];

    for(MusicBall* ball in _activePool)
    {
        if(ball.isDead && ball.noteIndex == _nextCorrectNote.noteIndex)
        {
            _nextCorrectNote = nil;
        }
        
        if(ball.isDead && ball.shouldDelete)
        {
            [_musicBar missNote: ball];
            for(id<MusicNoteActionDelegate> delegate in _actionDelegations)
            {
                [delegate noteMissed];
            }
            [ball reset];
            [_recyclePool addObject: ball];
        }
        
        [ball update: dt];
        
        if(ball.node.position.y <= _bandHeight  && !ball.showWing  && ball.autoPilot)
        {
            [_temp addObject: ball];
        }
    }
    
    for(MusicBall* ball in _temp)
    {
        [self placeNoteWithRule: ball];
    }
    [_temp removeAllObjects];
    
    [self playAutoMove];
    if(!_nextCorrectNote)
    {
        [self showSpiritNode];
    }
    
    if(_noteSpawned > 0)
    {
        for(id<MusicNoteStatusDelegate> delegate in _statusDelegations)
        {
            [delegate noteCorrectPercentage: (_noteCorrect/(_noteSpawned))];
        }
    }
    
    [_activePool removeObjectsInArray: _recyclePool];
    [_notePool addObjectsFromArray: _recyclePool];
    [_recyclePool removeAllObjects];
}

- (void) createBubble: (int) index atTime: (float) gameTime forTrack: (int) trackNum
{
    _internalIndex = (_internalIndex + 1) % _song.beatPerSection;
    
    if(_internalIndex == 0)
    {
        if(CCRANDOM_0_1() <= _randomNotePositionChance)
        {
            _currentTrackType = kTrackTypeRandom;
        }
        else
        {
            _currentTrackType = ((int)(CCRANDOM_0_1() * 100)) % (kTrackTypeReverseHalfCircle+1);
            
            // determine possible max index
            int leftIndex = _gridManager.gridXCount - _song.beatPerSection;
            if(leftIndex < 0)
                leftIndex = 0;
            int rightIndex = _gridManager.gridXCount - (leftIndex + 1);
            int xIndex = 0;
            int yIndex = 0;
            
            switch(_currentTrackType)
            {
                case kTrackTypeHalfCircle:
                case kTrackTypeArchLeft:
                    // arch must always leave a height of 1 gridSize ...  TODO define this better
                    xIndex = (((int)(CCRANDOM_0_1()*100)) % (leftIndex+1));
                    yIndex = (((int)(CCRANDOM_0_1()*100)) % 2);
                    break;
                case kTrackTypeReverseHalfCircle:
                case kTrackTypeArchRight:
                    xIndex = (((int)(CCRANDOM_0_1()*100)) % (_gridManager.gridXCount - rightIndex) + rightIndex);
                    yIndex = (((int)(CCRANDOM_0_1()*100)) % 2);
                    break;
                case kTrackTypeHorizontalLine:
                    xIndex = 0;
                    yIndex = (((int)(CCRANDOM_0_1()*100)) % _gridManager.gridYCount);
                    break;
                case kTrackTypeZipHorizontal:
                    // zip line must not use the first row and last row because it will cross the boundary
                    xIndex = 0;
                    yIndex = (((int)(CCRANDOM_0_1()*100)) % (_gridManager.gridYCount-2) + 1);
                    break;
                case kTrackTypeZipVertical:
                    // zip line must not use the first row and last row because it will cross the boundary
                    xIndex = (((int)(CCRANDOM_0_1()*100)) % (_gridManager.gridXCount-2) + 1);
                    yIndex = 0;
                    break;
                case kTrackTypeVerticalLine:
                    xIndex = (((int)(CCRANDOM_0_1()*100)) % _gridManager.gridXCount);
                    yIndex = 0;
                    break;
                default:
                    break;
            }
            NSLog(@"trail index (%i,%i)", xIndex, yIndex);
            _trailOffset = ccp(xIndex*_gridManager.gridSize,
                               yIndex*_gridManager.gridSize);
        }
    }
    [self createBubble: _internalIndex withTag: index withTime: gameTime forTrack: trackNum];
}

- (int) findGridIndexFromSoundIndex: (int) soundIndex
{
    int gindex = -1;
    if(_currentTrackType != kTrackTypeRandom)
    {
        CGPoint trailPos = [_trackManager generatePath: _currentTrackType withIndex: soundIndex outOf: _song.beatPerSection range: _gridManager.range cellSize: _gridManager.gridSize];
        trailPos = ccpAdd(ccpAdd(trailPos, _trailOffset),_gridOffset);
        //NSLog(@"%f %f",pos.x, pos.y);
        gindex = [_gridManager pointToGridIndex: trailPos];
        if(gindex != -1 && ![_gridManager avaliableGridIndex: gindex])
        {
            gindex = [_gridManager findEmptyIndexFrom: gindex searchDepth: 3];
        }
    }
    
    // random or the returned position is out of bound
    if(gindex == -1)
    {
        gindex = [_gridManager randomAvaliableGridIndex];
    }
    return gindex;
}

- (void) createBubble: (int) soundIndex withTag:(int) index withTime: (float) gameTime forTrack: (int) trackNum
{    
    MusicBall* ball = [_notePool lastObject];
    ball.node.visible = YES;
    int gindex = [self findGridIndexFromSoundIndex: soundIndex];
    if(gindex == -1)
    {
        // totoally out of space
        ball.node.visible = NO;
        return;
    }
    
    CGPoint pos = [_gridManager gridIndexToPoint: gindex];
    [_gridManager setIndex: gindex withValue: YES];
    ball.gridIndex = gindex;
    
    ball.node.position = ccp(_winSize.width/2, _winSize.height/2); //ccpAdd(self.cat.position,ccp(0,50));
    //NSLog(@"track %i with Index %i", trackNum, index);
    [ball typeByTrack: trackNum];
    ball.node.zOrder = kDepthNote;
    
    float travelTime = _song.timePerSection/_song.beatPerSection/2;
    
    ((CCSprite*)ball.node).opacity = 0;
    ((CCSprite*)ball.node).scale = 0.1f;

    id action1 = [CCEaseExponentialInOut actionWithAction: [CCMoveTo actionWithDuration: travelTime position: pos]];
    id action3 = [CCScaleTo actionWithDuration: travelTime scale: 1.0f];
    id action2 = [CCSequence actions:[CCFadeIn actionWithDuration: travelTime],[CCCallBlock actionWithBlock: ^{  ball.shouldFloat = YES; }],nil];
    
    // TODO might need to tweak
    ball.birthTime = gameTime + travelTime;
    ball.timeToDeath = (_song.timePerSection*2 - _song.secondPerBeat);
    ball.noteIndex = index;
    //ball.trackIndex = trackNum;
    [ball randomType];
    ball.trackIndex = ball.type;
    if(ball.type == kMusicBallStar)
    {
        ball.trackIndex = 0;
    }
    else if(ball.type == kMusicBallHeart)
    {
        ball.trackIndex = 1;
    }
    else
    {
        ball.trackIndex = 2;
    }
    
    [ball.node runAction: [CCSpawn actions:action1,action2,action3,nil]];
    
    if(_lastCreated == nil)
    {
        ball.previous = nil;
        ball.next = nil;
        _lastCreated = ball;
    }
    else
    {
        ball.previous = _lastCreated;
        _lastCreated.next = ball;
        ball.next = nil;
        _lastCreated = ball;
    }
    
    [_activePool addObject: ball];
    [_notePool removeLastObject];
    _noteSpawned++;
}

- (void) playSound: (NSString*) name atTime: (float) time
{
    if(time == 0)
    {
        [[SimpleAudioEngine sharedEngine] playEffect: name];
    }
    else
    {
        [self.musicManager.noteSoundTiming addObject: @(time)];
        [self.musicManager.noteSound addObject: name];
    }
}

- (BOOL) touchBegin: (CGPoint) pos withTrackNum: (int) trackNumber
{
    for(MusicBall* ball in _activePool)
    {
        if(!ball.isDead && ball.node.opacity == 255 && !ball.showWing && ball.touchTrackNumber == -1 && ccpDistance(ball.node.position, pos) < ball.node.contentSize.width)
        {
            [ball.node stopAllActions];
            ball.touchTrackNumber = trackNumber;
            //NSLog(@"trackNumber");
            CCBlade* blade = _blades[trackNumber];
            ball.blade = blade;
            [blade clear];
            blade.visible = YES;
            [blade push: pos];
            
            if(_pullNextNote)
            {
                ball.showMagnet = YES;
            }
            ball.start = ball.node.position;
            
            return YES;
        }
    }
    [_tracker releaseTouchByID: trackNumber];
    return NO;
}

- (void) touchMoved: (CGPoint) pos withTrackNum: (int) trackNumber
{
    for(MusicBall* ball in _activePool)
    {
        if(ball.touchTrackNumber == trackNumber)
        {
            [((CCBlade*)_blades[trackNumber]) push: pos];
            ball.node.position = pos;
            
            if(_pullNextNote && ball.next && ball.next.touchTrackNumber == -1 && !ball.next.isDead && ball.next.noteIndex != 0 && ball.next.node.opacity == 255)
            {
                [ball.next.node stopAllActions];
                ball.next.node.position = ccpAdd(ball.next.node.position, ccpMult(ccpSub(ball.node.position, ball.next.node.position),0.1));
            }
        }
    }
}

- (void) touchEnded: (CGPoint) pos withTrackNum: (int) trackNumber time: (float) gameTime
{
    [_tracker releaseTouchByID: trackNumber];
    BOOL loopToKill = NO;
    int counter = 0;
    BOOL displayComboLost = NO;
    
    for(MusicBall* ball in _activePool)
    {
        if(!ball.isDead && ball.touchTrackNumber == trackNumber && !ball.showWing)
        {
            if(ball.node.position.y <= _bandHeight)
            {
                // turn off blade visual
                ((CCBlade*)_blades[trackNumber]).visible = NO;
                
                // handle note display
                BOOL shouldIgnoreRule = (_effectActivated && _effectType == kItemSuper) ? YES : NO;
                [ball.node stopAllActions];
                BOOL success = NO;
                if(!_catBandMode)
                {
                    success = [_musicBar addNote: ball ignoreRule: shouldIgnoreRule];
                }
                else
                {
                    success = [_catBandBar addNote: ball];
                }
                
                if(!success)
                {

                    if(_comboCount > _comboTriggerValue)
                        displayComboLost = YES;
                    _comboCount = -1;

                    if(_colorMode)
                    {
                        [self playSound: @"piano_1.mp3" atTime: 0];
                    }
                    else
                    {
                        loopToKill = YES;
                        counter = _musicBar.latestNoteValueSeem;
                        float time = (ball.noteIndex-1)*_song.secondPerBeat+_song.timePerSection*2;
                        [self playSound: @"piano_1.mp3" atTime: time];
                        _noteWrong += _musicBar.skippedNoteCount;
                        NSLog(@"wrong note count %i",_noteWrong);
                    }
                    
                }
                else
                {
                    if(shouldIgnoreRule)
                    {
                        int trueIndex = _musicBar.latestNoteValueSeem;
                        for(MusicBall* swapball in _activePool)
                        {
                            if(swapball.noteIndex == trueIndex)
                            {
                                swapball.noteIndex = ball.noteIndex;
                                swapball.internalTime = ball.internalTime;
                            }
                        }
                    }

                    NSString* name = @"note_sound1.mp3";
//                    if(CCRANDOM_0_1() > 0.5)
//                        name = @"note_sound2.mp3";
                    if(_colorMode)
                    {
                        [self playSound: name atTime: 0];
                    }
                    else
                    {
                        float time = (ball.noteIndex-1)*_song.secondPerBeat+_song.timePerSection*2;
                        [self playSound: name atTime: time];
                    }
                    _noteCorrect++;
                }
                
                _nextCorrectNote = nil;
                //NSLog(@"birth time %f", ball.birthTime);
                //NSLog(@"game time %f", gameTime);
                
                _comboCount++;
                for(id<MusicNoteActionDelegate> delegate in _actionDelegations)
                {
                    [delegate placeNote: success onTime: (gameTime - ball.birthTime <= _perfectDelay) skippedNotes: _musicBar.skippedNoteCount atLocation: ball.node.position];
                }
                
                //[self displayReaction: ball gameTime: gameTime isCorrect: success];
                
                if(_pullNextNote && ball.next && ball.next.touchTrackNumber == -1 && !ball.next.isDead && ball.next.noteIndex != 0 && ball.next.node.opacity == 255)
                {
                    MusicBall* nextOne = ball.next;
                    [nextOne.node runAction: [ CCSequence actions: [CCMoveTo actionWithDuration: _song.secondPerBeat/2 position:ccp(nextOne.node.position.x, 50)], [CCCallBlock actionWithBlock: ^{ [self placeNote: nextOne]; }], nil ]];
                }
                
                [ball reset];
                [_recyclePool addObject: ball];
                
            }
            else
            {
                if(_colorMode)
                {
                    // dim the blade it
                    [((CCBlade*)_blades[trackNumber]) dim: YES];
                    ball.end = ball.node.position;
                    
                    float y = -ball.node.contentSize.height;
                    float ydist = ball.start.y-ball.node.contentSize.height;
                    CGPoint sub = ccpSub(ball.start, ball.end);
                    if(sub.y > 0)
                    {
                        float x = ball.start.x - (ydist/sub.y * sub.x);
                        NSLog(@"startt %f %f", ball.start.x, ball.start.y);
                        NSLog(@"end %f %f", ball.end.x, ball.end.y);
                        NSLog(@"%f %f", x,y);
                        
                        [ball.node stopAllActions];
                        ball.autoPilot = YES;
                        [ball.node runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(x,y)]];
                    }
                }
                else
                {
                    // dim the blade it
                    [((CCBlade*)_blades[trackNumber]) dim: YES];
                    // adjust ball to its correct position
                    [self setNewIndexFor: ball];
                    ball.showMagnet = NO;
                }
            }
            ball.touchTrackNumber = -1;
        }
    }
    
    // TODO redo this
    if(loopToKill)
    {
        for(MusicBall* ball in _activePool)
        {
            if(ball.noteIndex <= counter && !ball.isDead)// ball.node.visible && !ball.isDead)
            {
                ball.isDead = YES;
                [ball.node stopAllActions];
                [ball.node runAction: [CCSequence actions: [CCMoveTo actionWithDuration: 0.2f position: ccp(ball.node.position.x, 50)],[CCCallBlock actionWithBlock: ^{ [ball reset]; [_activePool removeObject: ball]; [_notePool addObject: ball]; }],nil]];
                [_musicBar skipNote: ball];
            }
        }
        
        [_activePool removeObjectsInArray: _recyclePool];
        [_notePool addObjectsFromArray: _recyclePool];
        [_recyclePool removeAllObjects];
    }
    
    [self processCombo: displayComboLost];
}

- (void) setNewIndexFor: (MusicBall*) ball
{
    int newindex = [_gridManager pointToGridIndex: ball.node.position];
    if([_gridManager avaliableGridIndex: newindex])
    {
        [_gridManager setIndex: ball.gridIndex withValue: NO];
        ball.gridIndex = newindex;
        [_gridManager setIndex: newindex withValue: YES];
    } 
    else
    {
        // actually... just force swap
        
        // find the ball with the index
        MusicBall* original = nil;
        for(MusicBall* oball in _activePool)
        {
            if(oball.gridIndex == newindex)
            {
                original = oball;
                break;
            } // end if
        } // end for
        
        if(original == nil)
        {
            // impossible??
            [_gridManager setIndex: ball.gridIndex withValue: NO];
            ball.gridIndex = newindex;
            [_gridManager setIndex: newindex withValue: YES];
        }
        else
        {
            int oldindex = ball.gridIndex;
            [original.node stopAllActions];
            [original.node runAction: [CCMoveTo actionWithDuration: 0.2f position: [_gridManager gridIndexToPoint: oldindex]]];
            original.gridIndex = oldindex;
            [_gridManager setIndex: oldindex withValue: YES];
            
            [ball.node stopAllActions];
            [ball.node runAction: [CCMoveTo actionWithDuration: 0.2f position: [_gridManager gridIndexToPoint: newindex]]];
            ball.gridIndex = newindex;
            [_gridManager setIndex: newindex withValue: YES];
        }
    }
}

- (void) processCombo: (BOOL) displayLost
{
    // TODO dont do this here
    if(_comboCount >= _comboTriggerValue)
    {
        if(_comboCount == _comboTriggerValue)
        {
            for(id<MusicNoteActionDelegate> delegate in _actionDelegations)
            {
                [delegate comboStarted];
            }
        }
        for(id<MusicNoteStatusDelegate> delegate in _statusDelegations)
        {
            [delegate comboAt: _comboCount];
        }
        //[self.delegate comboAt: _comboCount];
        //[self displayCombo: NO];
    }
    else
    {
        for(id<MusicNoteStatusDelegate> delegate in _statusDelegations)
        {
            [delegate comboFillPercentage: (_comboCount/_comboTriggerValue)];
        }
    }
    
    if(displayLost)
    {
        for(id<MusicNoteActionDelegate> delegate in _actionDelegations)
        {
            [delegate comboEnded];
        }
        //[self displayCombo: YES];
    }
}

// delegations

- (void) registerActionDelegatation: (id<MusicNoteActionDelegate>) delegate
{
    if(!_actionDelegations)
    {
        _actionDelegations = [[NSMutableArray alloc] init];
    }
    [_actionDelegations addObject: delegate];
}

- (void) removeActionDelegation: (id<MusicNoteActionDelegate>) delegate
{
    [_actionDelegations removeObject: delegate];
}

- (void) registerStatusDelegatation: (id<MusicNoteStatusDelegate>) delegate
{
    if(!_statusDelegations)
    {
        _statusDelegations = [[NSMutableArray alloc] init];
    }
    [_statusDelegations addObject: delegate];
}

- (void) removeStatusDelegation: (id<MusicNoteStatusDelegate>) delegate
{
    [_statusDelegations removeObject: delegate];
}

// item trigger delegate

- (void) activateItem: (SpecialItemType) itemType
{
    switch(itemType)
    {
        case kItemAutoWing:
            break;
        case kItemMagnet:
            _pullNextNote = YES;
            break;
        case kItemSpirit:
            _nextCorrectNote = nil;
            break;
        case kItemSuper:
            _musicBar.showSuperMode = YES;
            break;
        default:
            return;
    }
    
    _effectActivated = YES;
    _effectType = itemType;
}

- (void) deactivateItem: (SpecialItemType) itemType
{
    _effectActivated = NO;
    switch(itemType)
    {
        case kItemAutoWing:
            break;
        case kItemMagnet:
            _pullNextNote = NO;
            break;
        case kItemSpirit:
            _nextCorrectNote = nil;
            for(MusicBall* ball in _activePool)
            {
                if(ball.isDead)
                    continue;
                ball.showSpirit = NO;
            }
            break;
        case kItemSuper:
            _musicBar.showSuperMode = NO;
            break;
        default:
            return;
    }
}

- (void) obtainCoin
{
    
}

- (void) playAutoMove
{
    if(!(_effectActivated && _effectType == kItemAutoWing))
        return;
    
    if(_nextCorrectNote == nil)
    {
        for(MusicBall* ball in _activePool)
        {
            // TODO ... resolve these stat crap
            if(ball.isDead || ball.noteIndex == 0 || ball.node.opacity != 255)
                continue;
            if(_nextCorrectNote == nil)
                _nextCorrectNote = ball;
            else if(_nextCorrectNote.noteIndex > ball.noteIndex)
                _nextCorrectNote = ball;
        }
        
        if(_nextCorrectNote)
        {
            [_nextCorrectNote.node stopAllActions];
            _nextCorrectNote.showWing = YES;
            [_nextCorrectNote.node runAction: [ CCSequence actions: [CCMoveTo actionWithDuration: _song.secondPerBeat/2 position:ccp(_nextCorrectNote.node.position.x, 50)], [CCCallBlock actionWithBlock: ^{ [self placeNote:_nextCorrectNote]; }], nil ]];
        }
    }
}

- (void) placeNoteWithRule: (MusicBall*) ball
{
    BOOL displayComboLost = NO;
    // handle note display
    BOOL shouldIgnoreRule = (_effectActivated && _effectType == kItemSuper) ? YES : NO;
    
    [ball.node stopAllActions];
    BOOL success = NO;
    if(!_catBandMode)
    {
        success = [_musicBar addNote: ball ignoreRule: shouldIgnoreRule];
    }
    else
    {
        success = [_catBandBar addNote: ball];
    }
    
    NSString* name = @"note_sound1.mp3";
    if(success)
    {
        _noteCorrect++;
    }
    else
    {
        if(_comboCount > _comboTriggerValue)
            displayComboLost = YES;
        _comboCount = -1;
        name = @"piano_1.mp3";
        
    }
    
    _comboCount++;
    for(id<MusicNoteActionDelegate> delegate in _actionDelegations)
    {
        [delegate placeNote: success onTime: 0 skippedNotes: 0 atLocation: ball.node.position];
    }
    
    if(_pullNextNote && ball.next && ball.next.touchTrackNumber == -1 && !ball.next.isDead && ball.next.noteIndex != 0 && ball.next.node.opacity == 255)
    {
        MusicBall* nextOne = ball.next;
        [nextOne.node runAction: [ CCSequence actions: [CCMoveTo actionWithDuration: _song.secondPerBeat/2 position:ccp(nextOne.node.position.x, 50)], [CCCallBlock actionWithBlock: ^{ [self placeNote: nextOne]; }], nil ]];
    }
    
    [ball reset];
    [_recyclePool addObject: ball];
    _nextCorrectNote = nil;
}

- (void) placeNote: (MusicBall*) ball
{
    //((CCBlade*)_blades[trackNumber]).visible = NO;
    
    // handle note display
    [ball.node stopAllActions];
    if(!_catBandMode)
    {
        [_musicBar addNote: ball ignoreRule: NO];
    }
    else
    {
        [_catBandBar addNote: ball];
    }

    NSString* name = @"note_sound1.mp3";
//    if(CCRANDOM_0_1() > 0.5)
//        name = @"note_sound2.mp3";

    if(_colorMode)
    {
        [self playSound: name atTime: 0];
    }
    else
    {
        float time = (ball.noteIndex-1)*_song.secondPerBeat+_song.timePerSection*2;
        [self playSound: name atTime: time];
    }
    _noteCorrect++;
    
    _nextCorrectNote = nil;
    
    _comboCount++;
    for(id<MusicNoteActionDelegate> delegate in _actionDelegations)
    {
        [delegate placeNote: YES onTime: YES skippedNotes: 0 atLocation: ball.node.position];
    }
    
    //[self displayReaction: ball gameTime: ball.birthTime isCorrect: YES];
    [ball reset];
    [_notePool addObject: ball];
    [_activePool removeObject: ball];
}

- (void) showSpiritNode
{
    if(!(_effectActivated && _effectType == kItemSpirit))
        return;
    
    if(_nextCorrectNote == nil)
    {
        for(MusicBall* ball in _activePool)
        {
            // TODO ... resolve these stat crap
            if(ball.isDead || ball.noteIndex == 0 || ball.node.opacity != 255)
                continue;
            if(_nextCorrectNote == nil)
                _nextCorrectNote = ball;
            else if(_nextCorrectNote.noteIndex > ball.noteIndex)
                _nextCorrectNote = ball;
        }
    }
    
    if(_nextCorrectNote)
    {
        //NSLog(@"%i", _nextCorrectNote.noteIndex);
        _nextCorrectNote.showSpirit = YES;
    }
}

@end