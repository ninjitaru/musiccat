//
//  SpecialItem.m
//  MusicCat
//
//  Created by Jason Chang on 12/19/12.
//
//

#import "SpecialItem.h"
#import "GameDepthEnum.h"

@implementation SpecialItem

- (id) initWithParent:(CCNode *)parent
{
    self = [super init];
    if(self)
    {
        _sprite = [CCSprite spriteWithSpriteFrameName: @"tool_autowing.png"];
        [parent addChild: _sprite z: kDepthItem];
    }
    return self;
}

- (void) setType:(SpecialItemType)type
{
    _type = type;
    NSString* name = @"tool_autowing.png";
    switch(_type)
    {
        case kItemAutoWing:
            name = @"tool_autowing.png";
            break;
        case kItemDoubleCoin:
            name = @"tool_doublecoin.png";
            break;
        case kItemMagnet:
            name = @"tool_magnet.png";
            break;
        case kItemSpirit:
            name = @"tool_spirit.png";
            break;
        case kItemSuper:
            name = @"tool_super.png";
            break;
    }
    [_sprite setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: name]];
}

- (CCNode*) node
{
    return _sprite;
}

- (void) setRandomTypeExcludeDouble: (BOOL) shouldExclude;
{
    int index = CCRANDOM_0_1() * 100;
    if(shouldExclude)
        index = index % (kItemDoubleCoin);
    else
        index = index % (kItemDoubleCoin+1);
    self.type = index;
}

@end
