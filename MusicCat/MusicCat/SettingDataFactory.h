//
//  SettingDataFactory.h
//  MusicCat
//
//  Created by Jason Chang on 12/11/12.
//
//

#import <Foundation/Foundation.h>

@interface SettingDataFactory : NSObject
{
    NSArray* _stages;
    NSArray* _songs;
    NSDictionary* _stageDetailDataMap;
}

@property (nonatomic, retain, readonly) NSArray* stages;
@property (nonatomic, retain, readonly) NSArray* songs;

+ (SettingDataFactory*) sharedFactory;
- (void) loadData;

@end
