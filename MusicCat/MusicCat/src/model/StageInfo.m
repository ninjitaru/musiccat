//
//  StageInfo.m
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "StageInfo.h"

@implementation StageInfo

- (void) dealloc
{
    self.previewImage = nil;
    self.labelImage = nil;

    self.plistFile = nil;
    self.imageFile = nil;
    
    self.spriteData = nil;
    self.animationData = nil;

    self.fixedAnimationPair = nil;
    self.musicAnimationPair = nil;
    self.boostFixedAnimationPair = nil;
    self.boostMusicAnimationPair = nil;
    
    self.song = nil;
    self.updateBlock = nil;
    
    [super dealloc];
}

@end
