//
//  GRTouchableCCSprite.h
//  MusicCat
//
//  Created by Jason Chang on 11/19/12.
//
//

#import "CCSprite.h"

@interface GRTouchableCCSprite : CCSprite <CCTargetedTouchDelegate>
{
    SEL _action;
    id _target;
    //ccColor3B _disableColor;
}

- (id) initWithSpriteFrameName:(NSString *)spriteFrameName highlightedName: (NSString*) hname selectedName: (NSString*) sname disabledName: (NSString*) dname;

@property (retain, nonatomic) NSString* highlightedSpriteName;
@property (retain, nonatomic) NSString* selectedSpriteName;
@property (retain, nonatomic) NSString* normalSpriteName;
@property (retain, nonatomic) NSString* disabledSpriteName;

@property (assign, nonatomic) BOOL selected;
@property (assign, nonatomic) BOOL highlighted;
@property (assign, nonatomic) BOOL disabled;

- (void) setTouchUpInsideTarget: (id) target action: (SEL) action;

@end
