//
//  SingleStageMenuManager.m
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "SingleStageMenuManager.h"
#import "MenuDepthEnum.h"
#import "SpriteCreator.h"
#import "DeviceInfoHelper.h"

@implementation SingleStageMenuManager

- (int) difficulty
{
    return _difficutlyIndex;
}

- (void) dealloc
{
    [_notes release];
    [_auras release];
    [super dealloc];
}

- (void) prepareLayer
{
    [self showStageSelection];
}

- (void) changeStage: (StageInfo*) stage
{
    self.currentStage = stage;
    [self animateCurtainClose];
    [_node stopAllActions];
    [_node runAction: [CCSequence actions:
                       [CCDelayTime actionWithDuration: 0.6f],
                       [CCCallBlock actionWithBlock:
                        ^{
                            if(_preview)
                            {
                                [_node removeChild: _preview cleanup: YES];
                                _preview = nil;
                            }
                            _preview = [CCSprite spriteWithFile: stage.previewImage];
                            CGSize size = [CCDirector sharedDirector].winSize;
                            _preview.position = ccp(size.width/2,size.height/2);
                            [_node addChild: _preview z: kDepthMapPreview];
                            [_stageNamelabel setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: stage.labelImage]];
                            
                            [[CCTextureCache sharedTextureCache] removeUnusedTextures];
                            
                            [self animateCurtainOpen];
                        }],
                       nil]];
    
    
}

- (BOOL) isRollingControl:( CGPoint) pos
{
    CCSprite* roll = (CCSprite*)[_node getChildByTag: 18];
    return CGRectContainsPoint(roll.boundingBox, pos);
}

- (BOOL) isBackButton: (CGPoint) pos
{
    return CGRectContainsPoint(_backButton.boundingBox, pos);
}

- (void) showStageSelection
{
    CGSize size = [CCDirector sharedDirector].winSize;
    
    _preview = [CCSprite spriteWithFile: self.currentStage.previewImage];
    _preview.position = ccp(size.width/2,size.height/2);
    [_node addChild: _preview z: kDepthMapPreview];
    
    NSArray* setup = @[
    
    @{@"type":@(typeSprite),
    @"texture":@"item_topcurtain.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,size.height)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width/2,size.height)],
    @"depth":@(kDepthCurtain),
    @"tag":@15
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_back.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(2,size.height-2)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(2,size.height-2)],
    @"depth":@(kDepthUI),
    @"tag":@16
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"label_coin.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(1,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width-2,size.height-4)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width-2,size.height-4)],
    @"depth":@(kDepthUI),
    @"tag":@17
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_roll.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(1,0.5)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width+150,0)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width+200,0)],
    @"depth":@(kDepthUI),
    @"tag":@18
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_roll_body.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(1,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width,0)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width,0)],
    @"depth":@(kDepthUI),
    @"tag":@19
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_roll_head.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(1,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width+20,1)],
    @"ipadposition": [NSValue valueWithCGPoint: CGPointMake(size.width+60,1)],
    @"depth":@(kDepthUI),
    @"tag":@20
    },
    
    ];
    
    _isRunningEndLayer = NO;
    [[SpriteCreator sharedCreator] addSprites: setup toNode: _node inObject: self];
    
    _backButton = (CCSprite*)[_node getChildByTag: 16];
    
    _stageNamelabel = [CCSprite spriteWithSpriteFrameName: self.currentStage.labelImage];
    _stageNamelabel.anchorPoint = ccp(1,0);
    _stageNamelabel.position = ccp(size.width, 0);
    [_node addChild: _stageNamelabel z: kDepthUI];
    
    [self animateCurtainOpen];
    [self animateControlToStage];
}

- (void) backToStage
{
    [self animateControlToStage];
    for(int i = 21; i < 34; i++)
    {
        [_node removeChildByTag: i cleanup: YES];
    }
}

- (void) backToMenu:(void (^)(void))block;
{
    if(_isRunningEndLayer)
        return;
    _isRunningEndLayer = YES;
    
    [self hideRollingControl];
    CGSize size = [CCDirector sharedDirector].winSize;
    CCSprite* lcurtain = (CCSprite*)[_node getChildByTag: 1];
    CCSprite* rcurtain = (CCSprite*)[_node getChildByTag: 2];
    [lcurtain stopAllActions];
    [rcurtain stopAllActions];
    
    float widthDiff = 30;
    if([DeviceInfoHelper getScreenSizeType] == kscreenSizeIpad)
    {
        widthDiff = 60;
    }
    
    [lcurtain runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2+widthDiff, size.height)]];
    [rcurtain runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2-widthDiff*2, size.height)]];
    CCSprite* topc = (CCSprite*)[_node getChildByTag: 15];
    [topc stopAllActions];
    [topc runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2, size.height)]];
    
    [_node runAction: [CCSequence actions:
                       [CCDelayTime actionWithDuration: 0.6f],
                       [CCCallBlock actionWithBlock:
                        ^ {
                            _isRunningEndLayer = NO;
                            for(int i = 15; i < 34; i++)
                            {
                                [_node removeChildByTag: i cleanup: YES];
                            }
                            
                            [_node removeChild: _preview cleanup: YES];
                            [_node removeChild: _stageNamelabel cleanup: YES];
                            _stageNamelabel = nil;
                            _preview = nil;
                            
                            block();
                            [self animateCurtainOpen];
                        }],
                       nil]];

}

- (void) animateControlToStage
{
    [_node getChildByTag: 19].visible = YES;
    [_node getChildByTag: 20].visible = YES;
    _stageNamelabel.visible = YES;
    CGSize size = [CCDirector sharedDirector].winSize;
    CCSprite* top = (CCSprite*)[_node getChildByTag: 15];
    CCSprite* roll = (CCSprite*)[_node getChildByTag: 18];
    
    [top runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2, size.height-top.contentSize.height)]];
    [roll runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width, 0)]];
}

- (void) animateCurtainOpen
{
    CGSize size = [CCDirector sharedDirector].winSize;
    CCSprite* lcurtain = (CCSprite*)[_node getChildByTag: 1];
    CCSprite* rcurtain = (CCSprite*)[_node getChildByTag: 2];
    [lcurtain stopAllActions];
    [rcurtain stopAllActions];
    
    float widthDiff = 110;
    if([DeviceInfoHelper getScreenSizeType] == kscreenSizeIpad)
    {
        widthDiff = 220;
    }
    
    [lcurtain runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(widthDiff, size.height)]];
    [rcurtain runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width-widthDiff, size.height)]];
}

- (void) animateCurtainClose
{
    CGSize size = [CCDirector sharedDirector].winSize;
    CCSprite* lcurtain = (CCSprite*)[_node getChildByTag: 1];
    CCSprite* rcurtain = (CCSprite*)[_node getChildByTag: 2];
    [lcurtain stopAllActions];
    [rcurtain stopAllActions];
    
    float widthDiff = 30;
    if([DeviceInfoHelper getScreenSizeType] == kscreenSizeIpad)
    {
        widthDiff = 60;
    }
    
    [lcurtain runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2+widthDiff, size.height)]];
    [rcurtain runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width/2-widthDiff*2, size.height)]];
}

- (void) hideRollingControl
{
    CGSize size = [CCDirector sharedDirector].winSize;
    CCSprite* roll = (CCSprite*)[_node getChildByTag: 18];
    [_node getChildByTag: 19].visible = NO;
    [_node getChildByTag: 20].visible = NO;
    _stageNamelabel.visible = NO;
    [roll runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(size.width+roll.contentSize.width, 0)]];
    
    
}

- (void) showDifficulty
{
    // animate roller away (keep name tag label)
    CGSize size = [CCDirector sharedDirector].winSize;
    
    NSArray* setup = @[
    
    @{@"type":@(typeSprite),
    @"texture":@"menu_cat.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,size.height*0.18)],
    @"depth":@(kDepthFrontItem),
    @"tag":@21
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_step.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,20)],
    @"depth":@(kDepthFrontItem),
    @"tag":@22
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_stand.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,2)],
    @"depth":@(kDepthFrontGameButton),
    @"tag":@23
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_start.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,0)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,size.height*0.68)],
    @"depth":@(kDepthUI),
    @"tag":@24
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_spotlight.png",
    @"anchor": [NSValue valueWithCGPoint: CGPointMake(0.5,1)],
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width/2,size.height+30)],
    @"depth":@(kDepthSpotLight),
    @"tag":@25
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_overlay1.png",
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.25,size.height*0.68)],
    @"depth":@(kDepthFrontGameButton),
    @"tag":@26
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_overlay2.png",
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.25,size.height*0.31)],
    @"depth":@(kDepthFrontGameButton),
    @"tag":@27
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_overlay3.png",
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.75,size.height*0.68)],
    @"depth":@(kDepthFrontGameButton),
    @"tag":@28
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"button_overlay4.png",
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.75,size.height*0.31)],
    @"depth":@(kDepthFrontGameButton),
    @"tag":@29
    },

    @{@"type":@(typeSprite),
    @"texture":@"item_level1.png",
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.25,size.height*0.68)],
    @"depth":@(kDepthFrontGameButton),
    @"tag":@30
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_level2.png",
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.25,size.height*0.31)],
    @"depth":@(kDepthFrontGameButton),
    @"tag":@31
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_level3.png",
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.75,size.height*0.68)],
    @"depth":@(kDepthFrontGameButton),
    @"tag":@32
    },
    
    @{@"type":@(typeSprite),
    @"texture":@"item_level4.png",
    @"position": [NSValue valueWithCGPoint: CGPointMake(size.width*0.75,size.height*0.31)],
    @"depth":@(kDepthFrontGameButton),
    @"tag":@33
    },

    ];
    
    [[SpriteCreator sharedCreator] addSprites: setup toNode: _node inObject: self];
    [self hideRollingControl];
    [self showUnselected];
    
    [self setDiffulty: 1];
    
}

- (void) setDiffulty: (int) index
{
    if(_difficutlyIndex != index)
    {
        CCSprite* note = _notes[_difficutlyIndex];
        CCSprite* aura = _auras[_difficutlyIndex];
        [note stopAllActions];
        [aura stopAllActions];
        [note runAction: [CCSpawn actions:
                          [CCScaleTo actionWithDuration: 0.2f scale: 0.8f],
                          nil]];
        [aura runAction: [CCSpawn actions:
                          [CCScaleTo actionWithDuration: 0.2f scale: 0.8f],
                          [CCFadeTo actionWithDuration: 0.2f opacity: 150],
                          nil]];
    }
    
    switch(index)
    {
        case 0:
            self.currentStage.song.beatPerSection = 2;
            break;
        case 1:
            self.currentStage.song.beatPerSection = 4;
            break;
        case 2:
            self.currentStage.song.beatPerSection = 6;
            break;
        case 3:
            self.currentStage.song.beatPerSection = 8;
            break;
    }
    self.currentStage.difficulty = index;
    _difficutlyIndex = index;
    CCSprite* note = _notes[_difficutlyIndex];
    CCSprite* aura = _auras[_difficutlyIndex];
    [note stopAllActions];
    [aura stopAllActions];
    [note runAction: [CCSpawn actions:
                      [CCScaleTo actionWithDuration: 0.2f scale: 1.0f],
                      nil]];
    [aura runAction: [CCSpawn actions:
                      [CCScaleTo actionWithDuration: 0.2f scale: 1.0f],
                      [CCFadeTo actionWithDuration: 0.2f opacity: 255],
                      nil]];
    
}

- (void) showUnselected
{
    _notes = [@[[_node getChildByTag: 30],[_node getChildByTag: 31],[_node getChildByTag: 32],[_node getChildByTag: 33]] retain];
    _auras = [@[[_node getChildByTag: 26],[_node getChildByTag: 27],[_node getChildByTag: 28],[_node getChildByTag: 29]] retain];
    
    for(CCSprite* sprite in _notes)
    {
        sprite.scale = 0.8f;
    }
    for(CCSprite* sprite in _auras)
    {
        sprite.scale = 0.8f;
        sprite.opacity = 150;
    }
}

- (BOOL) isStartButton: (CGPoint) pos
{
    return CGRectContainsPoint([_node getChildByTag: 24].boundingBox, pos);
}

- (BOOL) alterDifficulty: (CGPoint) pos
{
    int index = 0;
    for(CCSprite* aura in _auras)
    {
        if(CGRectContainsPoint(aura.boundingBox,  pos))
        {
            [self setDiffulty: index];
            return YES;
        }
        index++;
    }
    return NO;
}



@end
