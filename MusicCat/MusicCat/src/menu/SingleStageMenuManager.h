//
//  SingleStageMenuManager.h
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "MenuManager.h"
#import "StageInfo.h"

@interface SingleStageMenuManager : MenuManager
{
    CCSprite* _preview;
    CCSprite* _stageNamelabel;
    CCSprite* _backButton;
    NSArray* _notes;
    NSArray* _auras;
    
    int _difficutlyIndex;
}

@property (nonatomic, assign) StageInfo* currentStage;
@property (readonly) int difficulty;

- (void) showStageSelection;
- (void) showDifficulty;

- (void) backToStage;
- (void) backToMenu:(void (^)(void))block;

- (BOOL) isBackButton: (CGPoint) pos;
- (BOOL) isRollingControl: (CGPoint) pos;
- (void) changeStage: (StageInfo*) stage;
- (BOOL) isStartButton: (CGPoint) pos;
- (BOOL) alterDifficulty: (CGPoint) pos;


@end
