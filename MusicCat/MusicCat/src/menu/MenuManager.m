//
//  MenuManager.m
//  MusicCat
//
//  Created by Jason Chang on 11/1/12.
//
//

#import "MenuManager.h"

@implementation MenuManager

- (id) initWithLayer: (CCNode*) node
{
    self = [super init];
    if(self)
    {
        _node = node;
    }
    return self;
}

- (void) prepareLayer
{
    
}

- (void) endLayer:(void (^)(void))block
{
    
}

@end
