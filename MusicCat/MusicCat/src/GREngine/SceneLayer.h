//
//  SceneLayer.h
//  MusicCat
//
//  Created by Jason Chang on 10/29/12.
//
//

#import "cocos2d.h"
#import "CCLayer.h"

@interface SceneLayer : CCLayer

+(CCScene *) scene;

- (void) initLayer;

@end
