//
//  MusicNoteStatusDelegate.h
//  MusicCat
//
//  Created by Jason Chang on 12/18/12.
//
//

#ifndef MusicCat_MusicNoteStatusDelegate_h
#define MusicCat_MusicNoteStatusDelegate_h

@protocol MusicNoteActionDelegate <NSObject>

- (void) placeNote: (BOOL) right onTime: (BOOL) perfect skippedNotes: (int) count atLocation: (CGPoint) pos;
- (void) noteMissed;
- (void) comboStarted;
- (void) comboEnded;

@end

@protocol MusicNoteStatusDelegate <NSObject>

- (void) noteCorrectPercentage: (float) percentage;
- (void) comboFillPercentage: (float) percentage;
- (void) comboAt: (int) comboCount;

@end


#endif
