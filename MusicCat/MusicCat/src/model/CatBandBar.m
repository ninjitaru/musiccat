//
//  CatBandBar.m
//  MusicCat
//
//  Created by Jason Chang on 12/24/12.
//
//

#import "CatBandBar.h"
#import "MusicBall.h"
#import "BandCat.h"

@implementation CatBandBar

- (void) dealloc
{
    [_catsPool release];
    [_activeCats release];
    [super dealloc];
}

- (id) initWithParentNode: (CCNode*) parent
{
    self = [super init];
    if(self)
    {
        _parentNode = parent;

        _initSpeed = 50;
        _currentSpeed = _initSpeed;
        _timeAccum = 0;
        _totalTime = 0;
        _speedIncrementCount = 2;

        [self createBandCats];
    }
    return self;
}

- (float) height
{
    return _bandHeight;
}

- (void) createBandCats
{
    _catsPool = [[NSMutableArray alloc] init];
    _activeCats = [[NSMutableArray alloc] init];
    for(int i = 0; i < 10; i++)
    {
        BandCat* cat = [[BandCat alloc] initWithParentNode: _parentNode];
        cat.active = NO;
        [_catsPool addObject: cat];
    }
    
    // find the heighest cat
    BandCat* cat = [[BandCat alloc] initWithParentNode: _parentNode];
    cat.catType = kCatGreen;
    _bandHeight = cat.node.contentSize.height;
    cat.catType = kCatOrange;
    if(cat.node.contentSize.height > _bandHeight)
        _bandHeight = cat.node.contentSize.height;
    cat.catType = kCatPink;
    if(cat.node.contentSize.height > _bandHeight)
        _bandHeight = cat.node.contentSize.height;
    [cat release];
    
    _firstCat = nil;
    _lastCat = nil;
}

- (void) update: (ccTime) dt
{
    _totalTime += dt;
    if(self.start)
    {
        _timeAccum += dt;
        if(_timeAccum >= _speedIncrementCount)
        {
            _timeAccum = 0;
            _currentSpeed *= 1.1f;
            for(BandCat* cat in _activeCats)
            {
                cat.speed = _currentSpeed;
            }
        }
        
        [self updateBandCats: dt];
    }
}

- (void) updateBandCats: (ccTime) dt
{
    CGSize size = [CCDirector sharedDirector].winSize;
    if(_lastCat)
    {
        // spawn next cat when last cat is 50% out
        if(_lastCat.node.position.x <=  (size.width - (_lastCat.node.contentSize.width*0.5)))
        {
            if([_catsPool lastObject])
            {
                BandCat* next = [_catsPool lastObject];
                _lastCat.next = next;
                [next setRandomCat];
                [_activeCats addObject: next];
                [_catsPool removeLastObject];
                next.active = YES;
                next.speed = _currentSpeed;
                next.node.position = ccp(_lastCat.node.position.x + _lastCat.node.contentSize.width + 5, 1);
                _lastCat = next;
            }
        }
    }
    else
    {
        _lastCat = [_catsPool lastObject];
        [_lastCat setRandomCat];
        _firstCat = _lastCat;
        [_activeCats addObject: _lastCat];
        [_catsPool removeLastObject];
        _lastCat.active = YES;
        _lastCat.speed = _currentSpeed;
        _lastCat.node.position = ccp(size.width + 5, 1);
    }
    
    if(_firstCat && _firstCat.node.position.x <= -_firstCat.node.contentSize.width)
    {
        BandCat* next = _firstCat.next;
        _firstCat.active = NO;
        [_catsPool addObject: _firstCat];
        [_activeCats removeObject: _firstCat];
        _firstCat = next;
    }
    
    for(BandCat* cat in _activeCats)
    {
        [cat update: dt];
    }
}

- (void) reset
{
    _currentSpeed = _initSpeed;
    _timeAccum = 0;
    _totalTime = 0;
    
    [_catsPool addObjectsFromArray: _activeCats];
    for(BandCat* cat in _catsPool)
    {
        cat.active = NO;
    }
    _lastCat = nil;
    _firstCat = nil;
}

- (BOOL) addNote: (MusicBall*) ball
{
    for(BandCat* cat in _activeCats)
    {
        if(CGRectContainsPoint(cat.node.boundingBox, ball.node.position))
        {
            return [cat addNote: ball];
        }
    }
    return NO;
}

@end
