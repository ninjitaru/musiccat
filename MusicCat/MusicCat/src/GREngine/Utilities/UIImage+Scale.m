//
//  UIImage+Scale.m
//  MusicCat
//
//  Created by Jason Chang on 11/26/12.
//
//

#import "UIImage+Scale.h"

@implementation UIImage (custom)

+ (UIImage*) imageFromFile: (NSString*) name
{
    if(name)
    {
        NSString *imagePath = [[NSBundle mainBundle] pathForResource: name ofType:nil];
        return [UIImage imageWithContentsOfFile: imagePath];
    }
    return nil;
}


-(UIImage*)scaleToSize:(CGSize)size
{
    return [self scaleToSize: size forcePOT: NO];
}

-(UIImage*)scaleToSize:(CGSize)size forcePOT: (BOOL) enablePOT
{
    CGSize potSize = size;
    if(enablePOT)
    {
        float x = powf(2, ceilf(logf(size.width)/logf(2)));
        float y = powf(2, ceilf(logf(size.height)/logf(2)));
        potSize.width = x;
        potSize.height = y;
    }
    
    //int bt = CGImageGetBitsPerComponent(self.CGImage);
    
    CGContextRef bitmap = CGBitmapContextCreate(NULL,
                                                potSize.width,
                                                potSize.height,
                                                CGImageGetBitsPerComponent(self.CGImage),
                                                0,
                                                CGImageGetColorSpace(self.CGImage),
                                                kCGImageAlphaPremultipliedLast);
    
    // Rotate and/or flip the image if required by its orientation
    //CGContextConcatCTM(bitmap, transform);
    //CGInterpolationQuality
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(bitmap, kCGInterpolationHigh);
    
    // Draw into the context; this scales the image
    CGContextDrawImage(bitmap, CGRectMake(0, 0, potSize.width, potSize.height), self.CGImage);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(bitmap);
    UIImage *scaledImage = [UIImage imageWithCGImage:newImageRef];
    
    // Clean up
    CGContextRelease(bitmap);
    CGImageRelease(newImageRef);
    
//    UIGraphicsBeginImageContextWithOptions(potSize, NO, [[UIScreen mainScreen] scale]);
//    
//    // Draw the scaled image in the current context
//    //[self drawInRect:CGRectMake((potSize.width-size.width)/2, (potSize.height-size.height)/2, size.width, size.height)];
//    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
//    
//    // Create a new image from current context
//    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    
//    // Pop the current context from the stack
//    UIGraphicsEndImageContext();
    
    // Return our new scaled image
    return scaledImage;
    
}

- (UIImage*) cropToRect: (CGRect) rect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return img;
}

- (void) savetoDiskWithName: (NSString*) name
{
    NSData* data = UIImagePNGRepresentation(self);
    NSString *imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent: name];
    [data writeToFile:imagePath atomically:YES];
}

@end
