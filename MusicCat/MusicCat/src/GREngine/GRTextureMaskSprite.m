//
//  GRTextureMaskSprite.m
//  MusicCat
//
//  Created by Jason Chang on 12/4/12.
//
//

#import "GRTextureMaskSprite.h"

@implementation GRTextureMaskSprite

- (void) dealloc
{
    [_renderSurface release];
    self.original = nil;
    self.mask = nil;
    [super dealloc];
}

+ (id) spriteWithSpriteFrameName:(NSString *)spriteFrameName maskSpriteFrameName:(NSString*) maskSpriteFrameName
{
    return [[[self alloc] initWithSpriteFrameName: spriteFrameName maskSpriteFrameName: maskSpriteFrameName] autorelease];
}

+ (id) spriteWithFile:(NSString *)filename maskFile: (NSString*) maskname
{
    return [[[self alloc] initWithFile: filename maskFile: maskname] autorelease];
}


- (id) initWithSpriteFrameName:(NSString *)spriteFrameName maskSpriteFrameName:(NSString*) maskSpriteFrameName
{
    self = [super init];
    if(self)
    {
        self.original = [CCSprite spriteWithSpriteFrameName: spriteFrameName];
        self.mask = [CCSprite spriteWithSpriteFrameName: maskSpriteFrameName];
        [self createTexture];
    }
    return self;
}

- (id) initWithFile:(NSString *)filename maskFile: (NSString*) maskname
{
    self = [super init];
    if(self)
    {
        self.original = [CCSprite spriteWithFile: filename];
        self.mask = [CCSprite spriteWithFile: maskname];
        [self createTexture];
    }
    return self;

}

- (void) createTexture
{
    CGSize textureSize = self.original.contentSize;
    _renderSurface = [[CCRenderTexture renderTextureWithWidth: textureSize.width height: textureSize.height] retain];
    
    self.original.position = ccp(textureSize.width/2, textureSize.height/2);
    self.mask.position = self.original.position;
    
    [self.original setBlendFunc:(ccBlendFunc){GL_DST_ALPHA, GL_ZERO}];
    [self.mask setBlendFunc:(ccBlendFunc){GL_ONE, GL_ZERO}];
    
    //[self update];
    
    [self setTexture: _renderSurface.sprite.texture];
    [self setTextureRect: _renderSurface.sprite.textureRect];
    self.flipY = YES;

}

- (void) visit
{
    if (!visible_)
        return;
    [self update];
    [super visit];
}
//
//- (void) draw
//{
//    //[self update: 0];
//    //[self updateTransform];
//    [super draw];
//}

- (void) update
{
    [_renderSurface clear: 0 g: 0 b: 0 a: 0];
    [_renderSurface begin];
    [self.mask visit];
    [self.original visit];
    [_renderSurface end];
}


@end
