//
//  MenuDepthEnum.h
//  MusicCat
//
//  Created by Jason Chang on 10/29/12.
//
//

#ifndef MusicCat_MenuDepthEnum_h
#define MusicCat_MenuDepthEnum_h

typedef enum
{
    kDepthBackground,
    kDepthMapPreview,
    kDepthForeground,
    kDepthBackGameButton,
    kDepthBackItemShadow,
    kDepthBackItem,
    kDepthCurtain,
    kDepthFrontItemShadow,
    kDepthFrontItem,
    kDepthSpotLight,
    kDepthFrontGameButton,
    kDepthUI,
    
} MenuDepthEnum;

#endif
