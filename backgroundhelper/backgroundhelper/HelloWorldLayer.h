//
//  HelloWorldLayer.h
//  backgroundhelper
//
//  Created by Jason Chang on 12/7/12.
//  Copyright Greamer 2012. All rights reserved.
//


#import <GameKit/GameKit.h>
#import "BackgroundManager.h"
#import "MusicManager.h"

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer <GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>
{
    BackgroundManager* bkManager;
    NSArray* _stages;
    MusicManager* musicManager;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
