//
//  GameImageFileManager.h
//  MusicCat
//
//  Created by Jason Chang on 11/26/12.
//
//

#import <Foundation/Foundation.h>
#import "DeviceInfoHelper.h"

@protocol ImageStatusDelegate <NSObject>

- (void) imagesReady: (NSString*) source;
- (void) completeProcessImages;
- (void) completeProcessPlist;
- (void) loadingProgressAt: (float) percentage;

@end

@interface GameImageFileManager : NSObject
{
    CCFileUtils* _fileUtil;
    enumiOSDeviceType _deviceSize;
}

@property (assign) id<ImageStatusDelegate> delegate;

// loading bg is different than loading composite textures
// for texture iphone4,5,ipad = ipad3 / 2 , iphon3 = ipad3/4
// bg images only
- (void) setupImages: (NSArray*) images;
// composite texture image
- (void) setupPlists: (NSArray*) lists;

@end
