//
//  MusicNoteManager.h
//  MusicCat
//
//  Created by Jason Chang on 12/11/12.
//
//

#import "LayerManager.h"
#import "TrackPatternManager.h"
#import "MusicBar.h"
#import "Song.h"
#import "CCBlade.h"
#import "MusicNoteStatusDelegate.h"
#import "ItemTriggerDelegate.h"
#import "GridManager.h"
#import "CatBandBar.h"

@class MusicManager;
@class TouchTracker;

@interface MusicNoteManager : LayerManager <ItemTriggerDelegate>
{
    NSMutableArray* _temp;
    
    BOOL _colorMode;
    BOOL _catBandMode;
    GridManager* _gridManager;
    
    NSMutableArray* _notePool;
    NSMutableArray* _activePool;
    NSMutableArray* _recyclePool;
    
    float _timeAccum;
    MusicBar* _musicBar;
    CCSpriteBatchNode* _noteBatch;
    Song* _song;
    TrackPatternType _currentTrackType;
    TrackPatternManager* _trackManager;
    TouchTracker* _tracker;
    
    int _internalIndex;
    NSMutableArray* _blades;
    CCTexture2D* _bladeTexture;
    
    int _comboCount;
    int _comboTriggerValue;  // setting
    float _bandHeight;
    
    
    NSMutableArray* _actionDelegations;
    NSMutableArray* _statusDelegations;
    
    int _noteSpawned;
    int _noteCorrect;
    int _noteMissed;
    int _noteWrong;
    
    BOOL _effectActivated;
    SpecialItemType _effectType;
    BOOL _pullNextNote;
    
    MusicBall* _nextCorrectNote;
    MusicBall* _lastCreated;
    
    CGPoint _trailOffset;
    float _randomNotePositionChance;
    CGSize _winSize; // use too freq. save a copy myself
    CGPoint _gridOffset;
    float _perfectDelay;
    
    CatBandBar* _catBandBar;

}

@property (assign) CCSprite* cat;
@property (assign) MusicManager* musicManager;
@property (readonly) int comboCount;

- (id) initWithParentNode:(CCNode *)node andSong: (Song*) song andTouchTracker: (TouchTracker*) tracker;

- (void) pause;
- (void) resume;
- (void) restart;
- (void) stop;
- (void) updateMusic: (float) musicDelta;
- (void) update: (ccTime) dt;

- (void) createBubble: (int) index atTime: (float) gameTime forTrack: (int) trackNum;

- (BOOL) touchBegin: (CGPoint) pos withTrackNum: (int) trackNumber;
- (void) touchMoved: (CGPoint) pos withTrackNum: (int) trackNumber;
- (void) touchEnded: (CGPoint) pos withTrackNum: (int) trackNumber time: (float) gameTime;

- (void) registerActionDelegatation: (id<MusicNoteActionDelegate>) delegate;
- (void) removeActionDelegation: (id<MusicNoteActionDelegate>) delegate;
- (void) registerStatusDelegatation: (id<MusicNoteStatusDelegate>) delegate;
- (void) removeStatusDelegation: (id<MusicNoteStatusDelegate>) delegate;


@end
