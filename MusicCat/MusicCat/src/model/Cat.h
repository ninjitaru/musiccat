//
//  Cat.h
//  MusicCatInvasion
//
//  Created by Jason Chang on 8/28/12.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Cat : NSObject
{
    CCNode* _parent;
    BOOL _isJumping;
    CGPoint _initPos;
    id _handAnimation;
}

@property (nonatomic, assign) CCSprite* body;
@property (nonatomic, assign) CCSprite* head;
@property (nonatomic, assign) CCSprite* foot;
@property (nonatomic, assign) CCSprite* hand;
@property (nonatomic, assign) CCSprite* ear;
@property (nonatomic, assign) CCSprite* eye;
@property (nonatomic, assign) ccColor3B mainColor;
@property (nonatomic, assign) ccColor3B earColor;
@property (nonatomic, assign) ccColor3B eyeColor;
@property (nonatomic, assign) CCNode* node;
@property (nonatomic, retain) NSString* soundName;

@property (nonatomic, assign) CGPoint position;

- (id) initWithParent: (CCNode*) parent;
- (BOOL) containsPoint: (CGPoint) pos;
- (void) playSound;

- (BOOL) shakeHead: (BOOL) start;
- (void) jump;

- (void) update: (ccTime) dt;

@end
