////
////  GameLayer.m
////  MusicCatInvasion
////
////  Created by Jason Chang on 8/24/12.
////
////
//
//#import "GameLayer2.h"
//#import "MusicBall.h"
//
//#define kCATCOUNT 7
//#define kSTARTDELAY 1.0f
//#define kGlobalFontScale 0.5f
//
//@interface GameLayer2 ()
//
//@property (nonatomic, assign) BOOL begin;
//@property (nonatomic, assign) BOOL musicBegin;
//@property (nonatomic, assign) BOOL displayListen;
//@property (nonatomic, assign) BOOL displayPlay;
//@property (nonatomic, assign) int score;
//@property (nonatomic, assign) BOOL paused;
//
//@end
//
//@implementation GameLayer2
//
//- (void) setPaused:(BOOL)paused
//{
//    if(_paused == paused)
//        return;
//    
//    _paused = paused;
//    
//    if(_paused)
//    {
//        [[CCDirector sharedDirector] pause];
//        [_musicManager pauseBGSound];
//    }
//    else
//    {
//        [[CCDirector sharedDirector] resume];
//        [_musicManager resumeBGSound];
//    }
//}
//
//- (void) setDisplayListen:(BOOL)displayListen
//{
//    if(_displayListen == displayListen)
//    {
//        return;
//    }
//    
//    _displayListen = displayListen;
//    if(displayListen)
//    {
//        float center = [CCDirector sharedDirector].winSize.width/2;
//        _hint1.visible = YES;
//        _hint1Cat.visible = YES;
//        _hint1.position = ccp(500, _hint1.position.y);
//        _hint1Cat.position = ccp(500, _hint1Cat.position.y);
//        [_hint1 runAction: [CCRepeatForever actionWithAction: [CCBlink actionWithDuration: 0.5f blinks: 1]]];
//        [_hint1 runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(center,_hint1.position.y)]];
//        [_hint1Cat runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(center, _hint1Cat.position.y)]];
//        
//    }
//    else
//    {
//        [_hint1 stopAllActions];
//        
//        [_hint1 runAction: [CCSequence actionOne: [CCMoveTo actionWithDuration: 0.5f position: ccp(-200,_hint1.position.y)]two: [CCCallBlock actionWithBlock: ^{
//            _hint1.opacity = 255;
//            _hint1.visible = NO;
//            _hint1Cat.visible = NO;
//        }]]];
//        [_hint1Cat runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(-200, _hint1Cat.position.y)]];
//    }
//}
//
//- (void) setDisplayPlay:(BOOL)displayPlay
//{
//    if(_displayPlay == displayPlay)
//    {
//        return;
//    }
//    
//    _displayPlay = displayPlay;
//    if(displayPlay)
//    {
//        float center = [CCDirector sharedDirector].winSize.width/2;
//        _hint2.visible = YES;
//        _hint2Cat.visible = YES;
//        
//        _hint2.position = ccp(500, _hint2.position.y);
//        _hint2Cat.position = ccp(500, _hint2Cat.position.y);
//        [_hint2 runAction: [CCRepeatForever actionWithAction: [CCBlink actionWithDuration: 0.5f blinks: 1]]];
//        [_hint2 runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(center,_hint2.position.y)]];
//        [_hint2Cat runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(center, _hint2Cat.position.y)]];
//    }
//    else
//    {
//        [_hint2 stopAllActions];
//        
//        [_hint2 runAction: [CCSequence actionOne: [CCMoveTo actionWithDuration: 0.5f position: ccp(-200,_hint2.position.y)]two: [CCCallBlock actionWithBlock: ^{
//            _hint2.opacity = 255;
//            _hint2.visible = NO;
//            _hint2Cat.visible = NO;
//        }]]];
//        [_hint2Cat runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(-200, _hint2Cat.position.y)]];
//    }
//}
//
//- (void) setBegin:(BOOL)begin
//{
//    _begin = begin;
//    if(_begin)
//    {
//        _bubbleCounter = 0;
//        _trackCounter = 0;
//        _gameScoreLabel.string = @"s 0";
//        [self resetEnegyBar];
//        [self resetProgressBar];
//        
//        _allowInput = NO;
//        _gameTimeAccum = 0;
//        self.score = 0;
//        _comboCount = 0;
//        [_label runAction: [CCMoveTo actionWithDuration: 0.5f position: ccp(_label.position.x, [CCDirector sharedDirector].winSize.height+ 100)]];
//    }
//    else
//    {
//        _allowInput = YES;
//        self.musicBegin = NO;
//        _label.position = ccp([CCDirector sharedDirector].winSize.width/2, [CCDirector sharedDirector].winSize.height/2);
//        
//        for(MusicBall* ball in _activeBallArray)
//        {
//            [ball.node stopAllActions];
//            [ball.node removeFromParentAndCleanup: YES];
//        }
//        [_activeBallArray removeAllObjects];
//        [_trashCan removeAllObjects];
//    }
//}
//
//- (void) setMusicBegin:(BOOL)musicBegin
//{
//    if(_musicBegin == musicBegin)
//        return;
//    
//    _musicBegin = musicBegin;
//    if(_musicBegin)
//    {
//        _initedCurrentGroup = NO;
//        _groupIndex = 0;
//        _activeModeGroupValue = 0;
//        _musicMode = sMusicSetup;
//        [_musicManager playBGSound: @"play_loop.mp3"];
//        
//        for(Cat* cat in _cats)
//        {
//            [cat shakeHead: YES];
//        }
//    }
//    else
//    {
//        [self resetProgressBar];
//        [_musicManager smoothStopBGSound];
//        
//        for(Cat* cat in _cats)
//        {
//            [cat shakeHead: NO];
//        }
//    }
//}
//
//#pragma mark - life cycle
//
//+(CCScene *) scene
//{
//	CCScene *scene = [CCScene node];
//	GameLayer2 *layer = [GameLayer2 node];
//	[scene addChild: layer];
//	return scene;
//}
//
//- (id) init
//{
//    if((self = [super init]))
//    {
//        _testScale = 1.0f;
//        if(![[CCDirector sharedDirector] enableRetinaDisplay: YES])
//            _testScale = 0.5f;
//        _tracker = [[TouchTracker alloc] init];
//        CCSprite* bg = [CCSprite spriteWithFile: @"bg_usa.png"];
//        bg.anchorPoint = ccp(0,0);
//        [self addChild: bg];
//        
//        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"cat.plist"];
//        
//        [self setupCat];
//        [self setupUI];
//        [self setupMusicStructure];
//        
//        self.isTouchEnabled = YES;
//        [self scheduleUpdate];
//        
//        CGSize size = [CCDirector sharedDirector].winSize;
//        
//        _pauseButton = [CCLabelBMFont labelWithString: @"P" fntFile: @"font.fnt"];
//        _pauseButton.anchorPoint = ccp(1,1);
//        _pauseButton.scale = kGlobalFontScale;
//        _pauseButton.position = ccp(size.width, size.height);
//        [self addChild: _pauseButton z: sDepthUI];
//        
//        _restartButton = [CCLabelBMFont labelWithString: @"R" fntFile: @"font.fnt"];
//        _restartButton.anchorPoint = ccp(1,1);
//        _restartButton.scale = kGlobalFontScale;
//        _restartButton.position = ccp(size.width, size.height - _pauseButton.contentSize.height);
//        [self addChild: _restartButton z: sDepthUI];
//        
//        _comboLabel = [CCLabelBMFont labelWithString: @"" fntFile: @"font.fnt"];
//        _comboLabel.visible = NO;
//        _comboLabel.scale = kGlobalFontScale;
//        _comboLabel.position = ccp(size.width/2, size.height*0.7f);
//        [self addChild: _comboLabel z: sDepthUI];
//        
//        _gameScoreLabel = [CCLabelBMFont labelWithString: @"S 0" fntFile: @"font.fnt"];
//        _gameScoreLabel.anchorPoint = ccp(0,1);
//        _gameScoreLabel.scale = kGlobalFontScale;
//        _gameScoreLabel.position = ccp(0, size.height);
//        [self addChild: _gameScoreLabel z: sDepthUI];
//        
//        _loopLabel = [CCLabelBMFont labelWithString: @"0" fntFile: @"font.fnt"];
//        _loopLabel.anchorPoint = ccp(0,1);
//        _loopLabel.scale = 0.25f;
//        _loopLabel.position = ccp(0, size.height - _gameScoreLabel.contentSize.height);
//        [self addChild: _loopLabel z: sDepthUI];
//        
//        _energyBar = [CCSprite spriteWithFile: @"image_blood.png"];
//        _energyBar.anchorPoint = ccp(0, 1);
//        _energyBar.position = ccp(20, size.height - 10);
//        CGRect rect = _energyBar.textureRect;
//        _energyWidth = rect.size.width;
//        [self addChild: _energyBar];
//        [self resetEnegyBar];
//        
//        CCSprite* sprite = [CCSprite spriteWithFile: @"tickbar_bg.png"];
//        sprite.anchorPoint = ccp(0,1);
//        sprite.position = ccp(0, size.height);
//        [self addChild: sprite];
//        
//        _groupProgressBar = [CCSprite spriteWithFile: @"tickbar_front.png"];
//        _groupProgressBar.anchorPoint = ccp(0,1);
//        _groupProgressBar.position = ccp(0, size.height);
//        rect = _groupProgressBar.textureRect;
//        _progressWidth = rect.size.width;
//        [self resetProgressBar];
//        
//        [self addChild: _groupProgressBar];
//        _allowInput = YES;
//        
//    }
//    return self;
//}
//
//- (void) dealloc
//{
//    [_activeBallArray release];
//    [_trashCan release];
//    free(_pianoButtonRect);
//    [_pianoButtons release];
//    
//    [_scoreTimingArray release];
//    [_scorePitchArray release];
//    [_musicManager release];
//    free(_trackNumberArray);
//    [_cats release];
//    [_tracker release];
//    [super dealloc];
//}
//
//#pragma mark - inits
//
//- (void) setupUI
//{
//    CGSize size = [CCDirector sharedDirector].winSize;
//    
//    _hint1 = [CCSprite spriteWithFile: @"listen.png"];
//    _hint1Cat = [CCSprite spriteWithFile: @"listencat.png"];
//    
//    _hint2 = [CCSprite spriteWithFile: @"yourturn.png"];
//    _hint2Cat = [CCSprite spriteWithFile: @"yourturncat.png"];
//    
//    _hint1.position = ccp(size.width/2, size.height * 0.6);
//    _hint1Cat.position = ccp(size.width/2, size.height * 0.85);
//    _hint2.position = ccp(size.width/2, size.height * 0.6);
//    _hint2Cat.position = ccp(size.width/2, size.height * 0.85);
//    
//    _hint1.scale = kGlobalFontScale;
//    _hint1Cat.scale = kGlobalFontScale;
//    _hint2.scale = kGlobalFontScale;
//    _hint2Cat.scale = kGlobalFontScale;
//    
//    [self addChild: _hint1];
//    [self addChild: _hint1Cat];
//    [self addChild: _hint2];
//    [self addChild: _hint2Cat];
//    
//    self.displayListen = YES;
//    self.displayPlay = YES;
//    self.displayListen = NO;
//    self.displayPlay = NO;
//    
//    _label = [CCLabelBMFont labelWithString: @"Start" fntFile: @"font.fnt"];
//    _label.position = ccp(size.width/2, size.height/2);
//    [self addChild: _label];
//}
//
//- (void) setupCat
//{
//    _soundNames = [@[@"sound_1.mp3",
//    @"sound_2.mp3",
//    @"sound_3.mp3",
//    @"sound_4.mp3",
//    @"sound_5.mp3",
//    @"sound_6.mp3",
//    @"sound_7.mp3"] retain];
//    
//    _trackNumberArray = (int*)malloc(sizeof(int)*kCATCOUNT);
//    NSMutableArray* array = [NSMutableArray arrayWithCapacity: kCATCOUNT];
//    CGSize size = [CCDirector sharedDirector].winSize;
//    for(int i = 0; i < 1; i++)
//    {
//        _trackNumberArray[i] = -1;
//        Cat* cat = [[Cat alloc] initWithParent: self];
//        cat.position = ccp(size.width*0.5f, size.height*0.4f);
//        [array addObject: cat];
//        [cat release];
//    }
//    
//    _cats = [array retain];
//}
//
//- (void) setupMusicStructure
//{
//    _musicManager = [[MusicManager2 alloc] init];
//    [self setupSongRule];
//}
//
//- (void) setupSongRule
//{
//    _scoreTimingArray = [@[
//                         @[@0.0f, @0.5f, @1.0f],@[@0.0f, @0.5f, @1.0f], @[@0.0f, @0.5f, @1.0f, @1.5f], @[@0.0f, @0.5f, @1.0f],
//                         @[@0.0f, @0.5f, @1.0f],@[@0.0f, @0.5f, @1.0f], @[@0.0f, @0.5f, @1.0f, @1.5f], @[@0.0f],
//                         @[@0.0f, @0.5f, @1.0f,@1.5f],@[@0.0f, @0.5f, @1.0f], @[@0.0f, @0.5f, @1.0f, @1.5f], @[@0.0f, @0.5f, @1.0f],
//                         @[@0.0f, @0.5f, @1.0f],@[@0.0f, @0.5f, @1.0f], @[@0.0f, @0.5f, @1.0f, @1.5f], @[@0.0f]] retain];
//    
//    _scorePitchArray = [@[
//                        @[@4, @2, @2],@[@3, @1, @1], @[@0, @1, @2, @3], @[@4, @4, @4],
//                        @[@4, @2, @2],@[@3, @1, @1], @[@0, @2, @4, @4], @[@2],
//                        @[@1, @1, @1,@1],@[@1, @2, @3], @[@2, @2, @2, @2], @[@2, @3, @4],
//                        @[@4, @2, @2],@[@3, @1, @1], @[@0, @2, @4, @4], @[@0]] retain];
//}
//
//#pragma mark - game cycle
//
//- (void) update: (ccTime) dt
//{
//    if(self.paused)
//        return;
//    
//    for(Cat* cat in _cats)
//    {
//        [cat update: dt];
//    }
//    
//    if(!self.begin)
//        return;
//    
//    _gameTimeAccum += dt;
//    if(_gameTimeAccum > kSTARTDELAY && !self.musicBegin)
//    {
//        self.musicBegin = YES;
//        return;
//    }
//    [self testMusic];
//    
//}
//
//- (BOOL) setupMusic
//{
//    if(_initedCurrentGroup)
//        return NO;
//    
//    _initedCurrentGroup = YES;
//    if(_groupIndex >= _scoreTimingArray.count)
//        return NO;
//    
//    _timings = [_scoreTimingArray objectAtIndex: _groupIndex];
//    _pitchs = [_scorePitchArray objectAtIndex: _groupIndex];
//    
//    _groupIndex++;
//    _noteIndex = 0;
//    return YES;
//}
//
//- (void) updateProgressBar
//{
//    CGRect rect = _groupProgressBar.textureRect;
//    float p = 0.25f + _musicManager.soundPositionPercentage;
//    if(p > 1.0f)
//        p = p - 1.0f;
//    rect.size.width = p * _progressWidth;
//    _groupProgressBar.textureRect =rect;
//}
//
//- (void) resetProgressBar
//{
//    CGRect rect = _groupProgressBar.textureRect;
//    rect.size.width = _progressWidth*0.25f;
//    _groupProgressBar.textureRect =rect;
//}
//
//- (void) createBubble: (int) soundIndex
//{
//    MusicBall* ball = [[MusicBall alloc] initWithParent: self];
//    ball.trackNumber = -1;
//    ball.counter = _bubbleCounter++;
//    CCSprite* sprite = (CCSprite*)ball.node;
//    sprite.scale = _testScale;
//    sprite.position = ((Cat*)_cats[0]).position;
//    
//    float y = CCRANDOM_0_1()*220+100;
//    float x = CCRANDOM_0_1()*440 + 20;
//    if(y <= 260)
//    {
//        x = CCRANDOM_0_1() * 100 + 20;
//        if(CCRANDOM_0_1() > 0.5f)
//        {
//            x += 340;
//        }
//    }
//    
//    [sprite runAction: [CCSequence actions: [CCMoveTo actionWithDuration: 1.0f position: ccp(x, y)],[CCDelayTime actionWithDuration: 3.0f], [CCCallBlock actionWithBlock: ^{
//        [sprite removeFromParentAndCleanup: YES];
//        if(ball.trackNumber != -1)
//        {
//            [_tracker releaseTouchByID: ball.trackNumber];
//        }
//        _trackCounter++;
//        [_activeBallArray removeObject: ball];
//    }], nil]];
//    
//    if(!_activeBallArray)
//    {
//        _activeBallArray = [[NSMutableArray alloc] initWithCapacity: 12];
//    }
//    [_activeBallArray addObject: ball];
//}
//
//#define kTurnSecion 4
//
//- (void) testMusic
//{
//    if(self.musicBegin)
//    {
//        [self updateProgressBar];
//        
//        _loopLabel.string = [@(_musicManager.groupPosition) stringValue];
//        
//        switch(_musicMode)
//        {
//            case sMusicSetup:
//                if(_activeModeGroupValue == _musicManager.groupPosition)
//                {
//                    if([self setupMusic])
//                    {
//                        self.displayListen = YES;
//                        self.displayPlay = NO;
//                        _activeModeGroupValue = _musicManager.groupPosition+1;
//                        _musicMode = sMusicComputer;
//                        _allowInput = NO;
//                    }
//                    else
//                    {
//                        self.begin = NO;
//                        self.displayListen = NO;
//                        self.displayPlay = NO;
//                    }
//                }
//                break;
//            case sMusicComputer:
//                if(_activeModeGroupValue == _musicManager.groupPosition)
//                {
//                    if(_noteIndex < _timings.count && _musicManager.soundPosition > [[_timings objectAtIndex: _noteIndex] floatValue])
//                    {
//                        // display bubble
//                        int soundIndex = [[_pitchs objectAtIndex: _noteIndex] intValue];
//                        [self createBubble: soundIndex];
//                        
//                        [[SimpleAudioEngine sharedEngine] playEffect: _soundNames[soundIndex]];
//                        _noteIndex++;
//                    }
//                    
//                    if(_noteIndex >= _timings.count)
//                    {
////                        self.displayListen = NO;
////                        self.displayPlay = YES;
////                        _allowInput = YES;
//                        _playerInputNoteIndex = 0;
////                        _activeModeGroupValue = _musicManager.groupPosition+1;
//                        _initedCurrentGroup = NO;
//                        _musicMode = sMusicSetup;
//                    }
//                }
//                break;
//            case sMusicPlayer:
//                if(_noteIndex <= 0)
//                {
//                    _musicMode = sMusicSetup;
//                    _allowInput = NO;
//                    _initedCurrentGroup = NO;
//                    
//                    if([self setupMusic])
//                    {
//                        self.displayListen = YES;
//                        self.displayPlay = NO;
//                        _activeModeGroupValue = _musicManager.groupPosition+1;
//                        _musicMode = sMusicComputer;
//                        _allowInput = NO;
//                    }
//                    else
//                    {
//                        self.begin = NO;
//                        self.displayListen = NO;
//                        self.displayPlay = NO;
//                    }
//                    
//                }
//                break;
//        }
//    }
//}
//
//- (void) showReaction: (BOOL) correct position: (CGPoint) pos isOnTime: (BOOL) correctTime
//{
//    NSString* display = @"NO";
//    
//    if(correct)
//    {
//        _comboCount++;
//        display = @"YES!!";
//        float scoreMult = 1;
//        
//        if(_comboCount >= 15)
//        {
//            scoreMult = 2;
//        }
//        
//        float timingReward = 0;
//        if(correctTime)
//            timingReward = 10;
//        
//        self.score += (10 + _comboCount*0.1f + timingReward) * scoreMult;
//        [_gameScoreLabel stopAllActions];
//        _gameScoreLabel.string = [NSString stringWithFormat: @"S %i", self.score];
//        [_gameScoreLabel runAction: [CCSequence actionOne: [CCScaleTo actionWithDuration: 0.2f scale: 1.2f*kGlobalFontScale] two: [CCScaleTo actionWithDuration: 0.2f scale: 1.0f*kGlobalFontScale]]];
//        
//        CGRect rect = _energyBar.textureRect;
//        rect.size.width =  (_comboCount/15.0f) * _energyWidth;
//        _energyBar.textureRect = rect;
//        
//        if(_comboCount >= 3)
//        {
//            _comboLabel.string = [NSString stringWithFormat: @"x%i", _comboCount];
//            _comboLabel.visible = YES;
//            [_comboLabel stopAllActions];
//            id a1 = [CCSequence actionOne: [CCScaleTo actionWithDuration: 0.2f scale: 1.3f*kGlobalFontScale] two: [CCScaleTo actionWithDuration: 0.4f scale: 1.0f*kGlobalFontScale]];
//            id a2 = [CCSequence actions: [CCFadeIn actionWithDuration: 0.1f], [CCDelayTime actionWithDuration: 0.5f], [CCFadeOut actionWithDuration: 0.4f], nil];
//            [_comboLabel runAction: [CCSpawn actionOne: a1 two: a2]];
//        }
//    }
//    else
//    {
//        if(correctTime)
//        {
//            self.score += 2;
//        }
//        
//        _comboCount = 0;
//        [self resetEnegyBar];
//    }
//    
//    if(correctTime)
//    {
//        CCLabelBMFont* label = [CCLabelBMFont labelWithString: @"right beat" fntFile: @"font.fnt"];
//        label.scale = kGlobalFontScale;
//        label.position = ccpAdd(pos, ccp(0,100));
//        label.opacity = 0;
//        [self addChild: label];
//        [label runAction: [CCSequence actions: [CCFadeIn actionWithDuration: 0.1f], [CCDelayTime actionWithDuration: 0.5f], [CCFadeOut actionWithDuration: 0.4f], [CCCallBlock actionWithBlock: ^{ [label removeFromParentAndCleanup: YES]; }], nil]];
//    }
//    
//    CCLabelBMFont* _reactionLabel = [CCLabelBMFont labelWithString: display fntFile: @"font.fnt"];
//    _reactionLabel.scale = kGlobalFontScale;
//    _reactionLabel.position = pos;
//    _reactionLabel.opacity = 0;
//    [self addChild: _reactionLabel];
//    [_reactionLabel runAction: [CCSequence actions: [CCFadeIn actionWithDuration: 0.1f], [CCDelayTime actionWithDuration: 0.5f], [CCFadeOut actionWithDuration: 0.4f], [CCCallBlock actionWithBlock: ^{ [_reactionLabel removeFromParentAndCleanup: YES]; }], nil]];
//}
//
//- (void) restartGame
//{
//    [_musicManager stopBGSound];
//    self.paused = NO;
//    self.begin = NO;
//    self.displayListen = NO;
//    self.displayPlay = NO;
//}
//
//- (void) updateEnergyBar
//{
//    
//}
//
//- (void) resetEnegyBar
//{
//    CGRect rect = _energyBar.textureRect;
//    rect.size.width = 0;
//    _energyBar.textureRect = rect;
//}
//
//#pragma mark - inputs
//
//- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    for(UITouch* touch in touches)
//    {
//        CGPoint pos = [self convertTouchToNodeSpace: touch];
//        if(CGRectContainsPoint(_label.boundingBox, pos))
//        {
//            if(self.paused)
//                return;
//            self.begin = YES;
//            return;
//        }
//        else if(CGRectContainsPoint(_pauseButton.boundingBox, pos))
//        {
//            self.paused = !self.paused;
//            return;
//        }
//        else if(CGRectContainsPoint(_restartButton.boundingBox, pos))
//        {
//            [self restartGame];
//            return;
//        }
//        
//        int num = [_tracker trackTouch: touch];
//        if(num == -1)
//            continue;
//        
//        BOOL releaseTouch = YES;
//        
//        /*
//         if pos on music and not tracked
//            if object can move
//                move it and decrement lock counter
//            track it
//         else if pos not music and not tracked
//            if object can move
//                move it and decrement lock counter
//            else
//                track it
//         
//         */
//        for(MusicBall* ball in _activeBallArray)
//        {
//            if(ball.trackNumber == -1 && ccpDistance(ball.node.position, pos) < ball.node.contentSize.width)
//            {
//                [ball.node stopAllActions];
//                releaseTouch = NO;
//                ball.trackNumber = num;
//                break;
//            }
//        }
//        
//        if(releaseTouch)
//        {
//            [_tracker releaseTouchByID: num];
//        }
//    }
//}
//
//- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    for(UITouch* touch in touches)
//    {
//        int num = [_tracker getTouchID: touch];
//        if(num == -1)
//            continue;
//        
//        /*
//         if tracked object is music
//            if music is movable
//                move it
//         */
//        
//        for(MusicBall* ball in _activeBallArray)
//        {
//            if(ball.trackNumber == num)
//            {
//                ball.node.position = [self convertTouchToNodeSpace: touch];
//            }
//        }
//
//    }
//}
//
//- (void) ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self ccTouchesEnded: touches withEvent: event];
//}
//
//- (void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    if(!_trashCan)
//    {
//        _trashCan = [[NSMutableArray alloc] init];
//    }
//    
//    for(UITouch* touch in touches)
//    {
//        int num = [_tracker getTouchID: touch];
//        if(num == -1)
//            continue;
//        
//        /*
//         if tracked object is music
//            if object needs activate
//                decrement hint active counter
//                display active visual
//            else if object is in music band
//                if object is in the right music band
//                    gain point and lock it
//                else
//                    lose point and lock it but with wrong visual and wrong response
//         else if tracked object is not music
//            if object needs activate
//                decrement hint active counter
//                display active visual
//            if object is unlocked
//                eat it, gain item
//         */
//        
//        for(MusicBall* ball in _activeBallArray)
//        {
//            if(ball.trackNumber == num)
//            {
//                if(ball.node.position.y <= 100)
//                {
//                    if(ball.counter == _trackCounter)
//                        [self showReaction: YES position: ccp(240,300) isOnTime: NO];
//                    else
//                        [self showReaction: NO position: ccp(240,300) isOnTime: NO];
//                    _trackCounter++;
//                    [ball.node stopAllActions];
//                    [ball.node removeFromParentAndCleanup: YES];
//                    [_trashCan addObject: ball];
//                }
//                ball.trackNumber = -1;
//            }
//        }
//
//        [_tracker releaseTouchByID: num];
//    }
//    
//    [_activeBallArray removeObjectsInArray: _trashCan];
//    [_trashCan removeAllObjects];
//}
//
//@end
